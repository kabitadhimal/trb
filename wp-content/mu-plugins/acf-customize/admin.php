<?php
/* ================================================================================
  CREATE THE OPTIONS PAGE
  ================================================================================== */
/*if (function_exists('acf_add_options_page')) {

}*/


add_action('acf/init', 'my_acf_init');
//https://wpml.org/forums/topic/options-page-fields-not-being-translated/
function my_acf_init()
{
    if (function_exists('acf_add_options_page')) {
        // your acf_add_options_page settings
        acf_add_options_page(array(
            'page_title' => 'Footer Options',
            'menu_title' => 'Footer Options'
        ));

        acf_add_options_page(array(
            'page_title' => 'Header Options',
            'menu_title' => 'Header Options'
        ));
    }
}


/* ================================================================================
  CREATE THE OPTIONS PAGE
  ================================================================================== */
//if (function_exists('acf_add_options_page')) {
//    acf_add_options_page(array(
//        'page_title' => 'Options',
//        'menu_title' => 'Options'
//    ));
//}



/* ================================================================================
        FILTERING CUSTOM TAXONOMY `MAP CATEGORY` AND CONTINENT
  ================================================================================== */

add_action('restrict_manage_posts', 'filter_map_by_taxonomies', 10, 2);
function filter_map_by_taxonomies($post_type, $which)
{

    // Apply this only on a specific post type
    if ('map' !== $post_type)
        return;

    // A list of taxonomy slugs to filter by
    $taxonomies = array('map_cat', 'continent_cat');

    foreach ($taxonomies as $taxonomy_slug) {

        // Retrieve taxonomy data
        $taxonomy_obj = get_taxonomy($taxonomy_slug);
        $taxonomy_name = $taxonomy_obj->labels->name;

        // Retrieve taxonomy terms
        $terms = get_terms($taxonomy_slug);

        // Display filter HTML
        echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
        echo '<option value="">' . sprintf(esc_html__('Show All %s', 'text_domain'), $taxonomy_name) . '</option>';
        foreach ($terms as $term) {
            printf(
                '<option value="%1$s" %2$s>%3$s (%4$s)</option>',
                $term->slug,
                ((isset($_GET[$taxonomy_slug]) && ($_GET[$taxonomy_slug] == $term->slug)) ? ' selected="selected"' : ''),
                $term->name,
                $term->count
            );
        }
        echo '</select>';
    }

}

/* ================================================================================
       END FILTERING CUSTOM TAXONOMY `MAP CATEGORY` AND CONTINENT
  ================================================================================== */



/* ================================================================================
        FILTERING COUNTRY
  ================================================================================== */

add_action('restrict_manage_posts', 'custom_restrict_manage_posts');

function custom_restrict_manage_posts()
{

    global $wpdb;
    global $post_type;

    $post_ids = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_type = %s", $post_type));

    if (empty($post_ids)) {

        return false;

    }

    $query = 'SELECT DISTINCT meta_value ';
    $query .= "FROM $wpdb->postmeta ";
    $query .= "WHERE meta_key = 'country' ";
    $query .= 'AND post_id IN( ' . implode(',', $post_ids) . ') ';
    $query .= 'ORDER BY meta_value+0 ASC ';

    $custom_fields = $wpdb->get_results($query);

    if (empty($custom_fields)) {

        return false;

    }

    ?>

    <label for="filter-by-price" class="screen-reader-text"><?php _e('country'); ?></label>
    <select name="meta_country" id="filter-by-country">

        <option value=""><?php _e('All Countries'); ?></option>

        <?php foreach ($custom_fields as $custom_field) : ?>

            <option
                value="<?php echo esc_attr($custom_field->meta_value); ?>"><?php echo esc_attr($custom_field->meta_value); ?></option>

        <?php endforeach; ?>

    </select>

    <?php

}


add_filter('request', 'custom_request_query');

function custom_request_query($vars)
{

    global $pagenow;
    global $post_type;

    $possible_post_types = array('map');

    if (!empty($pagenow) && $pagenow == 'edit.php' && in_array($post_type, $possible_post_types)) {

        if (!empty($_GET['meta_country'])) {

            $meta_country = $_GET['meta_country'];

            $vars['meta_key'] = 'country';
            $vars['meta_value'] = $meta_country;

        }

    }

    return $vars;

}

/* ================================================================================
        END FILTERING COUNTRY
  ================================================================================== */

/* ================================================================================
     SHOW COUTNRY HEADER
  ================================================================================== */


add_filter('manage_map_posts_columns', 'bs_event_table_head');
function bs_event_table_head($defaults)
{
    $defaults['country'] = 'Country';
    return $defaults;
}

add_action('manage_map_posts_custom_column', 'bs_map_table_content', 10, 2);

function bs_map_table_content($column_name, $post_id)
{
    if ($column_name == 'country') {
        $country = get_post_meta($post_id, 'country', true);
        echo $country;
    }
}


/* ================================================================================
    END COUTNRY HEADER
  ================================================================================== */