<?php
namespace App\Cpt;

function add_post_type($name, $args = array())
{
    add_action('init', function () use ($name, $args) {
        $upper = ucwords($name);
        $name = strtolower(str_replace(' ', '_', $name));

        $args = array_merge(
            array(
                'menu_icon' => 'dashicons-admin-home',
                'public' => true,
                'label' => "$upper",
                'labels' => ['add_new_item' => "Add new $upper"],
                'supports' => ['title', 'editor', 'thumbnail'],
                'exclude_from_search' => false,
                'capability_type' => 'page',
            ), $args);

        register_post_type($name, $args);
    });
}


/*
 * Taxonomy
 */

function add_taxonomy($name, $post_type, $args = array())
{
    $name = strtolower($name);
    add_action('init', function () use ($name, $post_type, $args) {
        $args = array_merge(
            array(
                'label' => ucwords($name),
                'show_ui' => true,
                'query_var' => true,
                'show_admin_column' => true,
            ), $args);

        register_taxonomy($name, $post_type, $args);
    });
}


/* =========================================================
  Add custom post type
  ======================================================== */
/* Property Post */
add_post_type('map', array(
    'public' => true,
    'menu_icon' => 'dashicons-building',
    'label' => "All Map pointer",
    'labels' => array(
        'add_new_item' => "Add New Map pointer"
    ),
    'supports' => array(
        'title',
        'thumbnail'

    )
));

add_taxonomy('map_cat', array('map'), array('label' => 'Map Category', 'hierarchical' => true));
//add_taxonomy('country', array('map'), array('label' => 'Country', 'hierarchical' => false));
add_taxonomy('continent_cat', array('map'), array('label' => 'Continent', 'hierarchical' => true));
//
///* Service Post */
//add_post_type('service', array(
//    'public' => true,
//    'menu_icon' => 'dashicons-businessman',
//    'label' => "All Services",
//    'labels' => array(
//        'add_new_item' => "Add New Service"
//    ),
//    'rewrite' => array(
//        'slug' => 'decouvrir/service'
//    ),
//    'supports' => array(
//        'title',
//        'editor',
//        'thumbnail'
//
//    )
//));

/* Contact Post */
//add_post_type('contact', array(
//    'public' => true,
//    'menu_icon' => 'dashicons-email',
//    'label' => "All Contacts",
//    'labels' => array(
//        'add_new_item' => "Add New Contact"
//    ),
//
//    'supports' => array(
//        'title',
//        'editor',
//        'thumbnail'
//
//    )
//));

/* Contact Post */
//add_post_type('project', array(
//    'public' => true,
//    'menu_icon' => 'dashicons-building',
//    'label' => "All Projects",
//    'labels' => array(
//        'add_new_item' => "Add New Projects"
//    ),
//
//    'supports' => array(
//        'title',
//        'editor',
//        'thumbnail'
//
//    )
//));
//add_taxonomy('category_cat', array('project'), array('label' => 'Category', 'hierarchical' => true));
/* Connection Post */
//add_post_type('connection', array(
//    'public' => true,
//    'menu_icon' => 'dashicons-phone',
//    'label' => "All Connection",
//    'labels' => array(
//        'add_new_item' => "Add New Connection"
//    ),
//
//    'supports' => array(
//        'title',
//
//
//    )
//));
//


