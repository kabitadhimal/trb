<?php
/*
Plugin Name: MU_Plugin Loader
Description: This is a loader for mu_plugins.
Author: Procab
Version: 1.0
Author URI: http://procab.ch
*/
require WPMU_PLUGIN_DIR.'/advanced-custom-fields-pro/acf.php';
require WPMU_PLUGIN_DIR.'/app/vendor/autoload.php';
require WPMU_PLUGIN_DIR.'/acf-customize/admin.php';
//require WPMU_PLUGIN_DIR.'/bory-import/bory-import.php';
require WPMU_PLUGIN_DIR.'/custom-post-types/cpt.php';
require WPMU_PLUGIN_DIR.'/advanced-custom-fields-table-field/acf-table.php';
//require WPMU_PLUGIN_DIR.'/regie-import/regie-import.php';


