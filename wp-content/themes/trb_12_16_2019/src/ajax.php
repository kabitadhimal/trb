<?php

use DrewM\MailChimp\MailChimp;

add_action("wp_ajax_newsletter_subscribe", "subscribedToMailchimp");
add_action("wp_ajax_nopriv_newsletter_subscribe", "subscribedToMailchimp");

function subscribedToMailchimp()
{


    check_ajax_referer('newsletter-subscribe', 'security');


    $email = (isset($_POST["email"]) && $_POST["email"] != '') ? filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) : NULL;

    // $apikey = '4f2cdc2ca4377f701766c4acd51dd82d-us12';
    // $listId = 'e4a78d541a';
    $mailchimp = get_field('mailchimp', 'option');

    if (!empty($mailchimp['api_key']) && !empty($mailchimp['list_id'])) {

        $apikey = $mailchimp['api_key'];
        $listId = $mailchimp['list_id'];

        $MailChimp = new \DrewM\MailChimp\MailChimp($apikey);
        $result = $MailChimp->post("lists/$listId/members", [
            'email_address' => $email,
            'status' => 'subscribed',
        ]);

        if ($MailChimp->success()) {

            echo json_encode(array('status' => true, 'msg' => 'You\'ve just been sent an email to confirm your email address. Please click on the link in this email to confirm your subscription.'));

        } else {
            // app\debug($result);
            if ($result['status'] == '400')
                echo json_encode(array('status' => false, 'msg' => 'The email is already exists.'));
            else
                echo json_encode(array('status' => false, 'msg' => $MailChimp->getLastError()));

        }


    } else {
        echo json_encode(array('status' => false, 'msg' => 'Something went wrong. Please contact administrator.'));
    }
    exit();
}


add_action("wp_ajax_map_data", "map_datas");
add_action("wp_ajax_nopriv_map_data", "map_datas");

function map_datas()
{

    $continents = [];
    $term_id = $_GET['term_id'];

    ?>
    <?php if (!empty($term_id)):

    ?>
    <div id="tab-map-<?php echo $term_id ?>"
         class="tab-content list-style current">

        <?php $args = array(
            'post_type' => 'map',
            'posts_per_page'=>'-1',
            'tax_query' => array(
                array(
                    'taxonomy' => 'map_cat',
                    'field' => 'term_id',
                    'terms' => $term_id
                )
            )
        );
        $query = new WP_Query($args);

       // App\debug($query);
        ?>
        <div class="hideonmobile position-relative">
            <img
                src="<?php echo get_template_directory_uri() . '/assets/images/location-maps-transparent.png'; ?>" alt="locations-maps-transparent">

            <?php


            while ($query->have_posts()):$query->the_post();
                $args = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'all');
                $terms = wp_get_post_terms(get_the_ID(), 'continent_cat', $args);
                if (!empty($terms)) {
                    $continents[] = [
                        'name' => $terms[0]->name,
                        'id' => $terms[0]->term_id,
                    ];
                }

                ?>


                <a id="marker-<?php echo get_the_ID() ?>"
                   data-content-id="popup-<?php echo get_the_ID() ?>"
                   class="js-marker map-pointer-small not-link"
                   href="javascript:;"
                   style="left: <?php echo get_field('x_point') ?>%; top: <?php echo get_field('y_point') ?>%;"></a>
                <div id="popup-<?php echo get_the_ID() ?>" class="d-none">
                    <span class="pull-right"><i class="icon icon-close"></i></span>

                    <p><?php the_title(); ?></p>
                    <?php $add_more_company = get_field('add_more_company');
                            if($add_more_company):
                                foreach($add_more_company as $company):
                                    ?>

                                    
                                                      <p><?php echo $company['address'] ?><br/><br/>

                                                            <?php if ($company['phone_number']): ?>
                                                                <span
                                                                    class="icon icon-phone"></span> <?php echo $company['phone_number'] ?>
                                                                <br/>
                                                            <?php endif; ?>
                                                            <?php if ($company['email']): ?>
                                                                <span class="icon icon-newsletter"></span><?php echo $company['email'] ?>
                                                                <br/>
                                                            <?php endif; ?>
                                                            <?php if ($company['website']): ?>
                                                                <span class="icon icon-search"></span><a href="<?php echo $company['website'] ?>"
                                                                                                         class="text-white"
                                                                                                         target="_blank"><?php echo App\remove_http($company['website']) ?></a>
                                                            <?php endif; ?>
                                                            </p>


                                <?php endforeach;
                            endif;
                    ?>
                    
                </div>
            <?php endwhile;
            wp_reset_postdata(); ?>


        </div>


        <div class="gap-p-eq pt-0 c-country hideonmobilex">
            <div class="row">
                <?php




                if (!empty($continents)):
                    $results = array_unique($continents, SORT_REGULAR);

                    $final_values = array_values($results);
                    array_multisort (array_column($final_values, 'name'), SORT_ASC, $final_values);

                    foreach ($final_values as $continent):
                        ?>
                        <div class="col-sm-4">

                            <h3><?php echo $continent['name'] ?></h3>
                            <ul class="lists-editor lists-editor--unordered">
                                <?php

                                $args1 = array(
                                    'post_type' => 'map',
                                    'posts_per_page'=>'-1',
                                    'meta_key'			=> 'map_title',
                                    'orderby'			=> 'meta_value',
                                    'order'				=> 'ASC',
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'continent_cat',
                                            'field' => 'term_id',
                                            'terms' => $continent['id']
                                        ),
                                        array(
                                            'taxonomy' => 'map_cat',
                                            'field' => 'term_id',
                                            'terms' => $term_id
                                        )
                                    )
                                );
                                $query = new WP_Query($args1);
                                // App\debug($query);

                                while ($query->have_posts()):$query->the_post();
                                    $id = get_the_ID();
                                    $map_title = get_field('map_title', $id);
                                    $terms = get_the_terms($id, 'map_cat');
                                    //App\debug($terms);
                                    if (!empty($map_title)):
                                        ?>
                                        <li data-id="<?php echo get_the_ID() ?>"
                                            data-term_id="<?php echo !empty($terms[0]->term_id) ? $terms[0]->term_id : '' ?>">
                                            <a data-marker-id="marker-<?php echo $id ?>"
                                               class="js-proxy-marker"><?php echo $map_title ?></a>

                                            <div data-id="<?php echo get_the_ID() ?>" data-popup_id="<?php echo get_the_ID() ?>"
                                                 data-popup_term_id="<?php echo !empty($terms[0]->term_id) ? $terms[0]->term_id : '' ?>"
                                                 class="mobile-map">




                                        <?php $add_more_company = get_field('add_more_company');
                                            if($add_more_company):
                                                foreach($add_more_company as $company):
                                                    ?>

                                                      <p><?php echo $company['address'] ?><br/><br/>

                                                            <?php if ($company['phone_number']): ?>
                                                                <span
                                                                    class="icon icon-phone"></span> <?php echo $company['phone_number'] ?>
                                                                <br/>
                                                            <?php endif; ?>
                                                            <?php if ($company['email']): ?>
                                                                <span class="icon icon-newsletter"></span><?php echo $company['email'] ?>
                                                                <br/>
                                                            <?php endif; ?>
                                                            <?php if ($company['website']): ?>
                                                                <span class="icon icon-search"></span><a href="<?php echo $company['website'] ?>"
                                                                                                         class="text-white"
                                                                                                         target="_blank"><?php echo $company['website'] ?></a>
                                                            <?php endif; ?>
                                                            </p>
                                                              <?php endforeach;
                                            endif; ?>


                                               
                                            </div>
                                        </li>

                                    <?php endif;
                                endwhile; ?>

                            </ul>
                        </div>
                    <?php endforeach;
                endif; ?>


            </div>


        </div>

    </div>


    <?php
endif;
    exit(); ?>


    <?php
}




