(function() {
    tinymce.create('tinymce.plugins.wp_procab', {
        init : function(ed, url) {
            
             ed.addButton('download-button',{
                title : 'Download Button',
                cmd : 'download-button',
                text : 'Download Button'
             } );

            ed.addButton('bg-white-download-button',{
                title : 'BG White Button',
                cmd : 'bg-white-download-button',
                text : 'BG White Button'
             } );

            ed.addCommand('download-button', function() {
                ed.execCommand('mceInsertContent', 0, '<br/>[download-button src="" ]');
            });

             ed.addCommand('bg-white-download-button', function() {
                ed.execCommand('mceInsertContent', 0, '<br/>[bg-white-pdf src="" ]');
            });

        },
 
        createControl : function(n, cm) {
            return null;
        },
 
        getInfo : function() {
            return {
                longname : 'Wp Procab Buttons'
            };
        }
    });
 
    tinymce.PluginManager.add( 'wp_procab', tinymce.plugins.wp_procab );
})();