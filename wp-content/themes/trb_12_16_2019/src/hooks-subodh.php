<?php


function rosset_after_setup_theme()
{
    register_nav_menu('header_menu', __('Header Menu', 'rosset'));
    register_nav_menu('footer_sitemap', __('Footer Sitemap', 'rosset'));
}

add_action('after_setup_theme', 'rosset_after_setup_theme');


/**
 * Enqueue scripts and styles.
 *
 * @since procab 1.0
 */
function procab_scripts()
{

    if(is_page_template('page-templates/template-contact.php')){

    }
    /*news detial*/
     wp_enqueue_style('slicknav', get_template_directory_uri() . '/assets/js/modules/adminimal_admin_menu/js/slicknav/slicknav.css', array(), '2.0');


    wp_enqueue_style('systembase', get_template_directory_uri() . '/assets/css/systembase.css', array(), '2.0');

    wp_enqueue_style('style', get_template_directory_uri() . '/assets/css/style.css', array(), '2.0');
    wp_enqueue_style('custom-style', get_template_directory_uri() . '/assets/css/custom.css', array(), '2.0');
    wp_enqueue_style('addtohomescreen', get_template_directory_uri() . '/assets/css/addtohomescreen.css', array(), '2.0');
    // wp_enqueue_style('service-style', get_template_directory_uri() . '/assets/css/service-style.css', array(), '1.0');

    wp_register_script('picturefill', get_template_directory_uri() . '/assets/js/others/picturefill.min.js', array(), '1.0', true);
    wp_register_script('picture', get_template_directory_uri() . '/assets/js/others/picture.min.js', array(), '1.0', true);
    wp_register_script('jquery-once', get_template_directory_uri() . '/assets/js/others/jquery.once.js', array(), '1.0', true);
    wp_register_script('drupal', get_template_directory_uri() . '/assets/js/others/main.js', array(), '1.0', true);

    wp_register_script('lazysizes', get_template_directory_uri() . '/assets/js/others/lazysizes.min.js', array(), '1.0', true);
    wp_register_script('aspectratio', get_template_directory_uri() . '/assets/js/others/ls.aspectratio.min.js', array(), '1.0', true);
    wp_register_script('cookie', get_template_directory_uri() . '/assets/js/others/jquery.cookie.js', array(), '1.0', true);

    //wp_register_script('form', get_template_directory_uri() . '/assets/js/others/jquery.form.min.js', array(), '1.0', true);
    wp_register_script('ajax', get_template_directory_uri() . '/assets/js/others/ajax.js', array(), '1.0', true);
    wp_register_script('jquery_update', get_template_directory_uri() . '/assets/js/others/jquery_update.js', array(), '1.0', true);
    wp_register_script('cookie', get_template_directory_uri() . '/assets/js/others/jquery.cookie.js', array(), '1.0', true);
    wp_register_script('fr_ciqikf4IhNmagPtrxTvp5CqUvHDO4IUgE', get_template_directory_uri() . '/assets/js/others/fr_ciqikf4IhNmagPtrxTvp5CqUvHDO4IUgE-PHL794hx0.js', array(), '1.0', true);


    wp_register_script('google', 'https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyDZBAC5mhbnah32373H7lPZK55gphIuv6c', array(), '1.0', true);
    wp_register_script('m3_properties', get_template_directory_uri() . '/assets/js/others/m3_properties.sticky.js', array(), '1.0', true);
    wp_register_script('captcha', get_template_directory_uri() . '/assets/js/others/captcha.js', array(), '1.0', true);

    wp_register_script('textarea', get_template_directory_uri() . '/assets/js/others/textarea.js', array(), '1.0', true);
    wp_register_script('webform', get_template_directory_uri() . '/assets/js/others/webform.js', array(), '1.0', true);
    wp_register_script('progress', get_template_directory_uri() . '/assets/js/others/progress.js', array(), '1.0', true);


/*news detial*/
  //  wp_enqueue_script( 'jquery-no-conflict.slicknav.js',get_template_directory_uri() . '/assets/js/modules/adminimal_admin_menu/js/slicknav/jquery-no-conflict.slicknav.js', array(), '1.0', true);
    //wp_enqueue_script( 'adminimal_admin_menu',get_template_directory_uri() . '/assets/js/modules/adminimal_admin_menu/js/adminimal_admin_menu.js', array(), '1.0', true);




    wp_register_script('console', get_template_directory_uri() . '/assets/js/lib/console.js', array(), '1.0', true);
    wp_register_script('loadcss', get_template_directory_uri() . '/assets/js/lib/loadcss.js', array(), '1.0', true);
    wp_register_script('fast-click', get_template_directory_uri() . '/assets/js/lib/fast-click.js', array(), '1.0', true);
    wp_register_script('hoverintent', get_template_directory_uri() . '/assets/js/lib/hoverintent.js', array(), '1.0', true);
    wp_register_script('superfish', get_template_directory_uri() . '/assets/js/lib/superfish.js', array(), '1.0', true);
    wp_register_script('jquery-placeholder', get_template_directory_uri() . '/assets/js/lib/jquery.placeholder.js', array(), '1.0', true);
    wp_register_script('fonts-css-async', get_template_directory_uri() . '/assets/js/src/fonts-css-async.js', array(), '1.0', true);
    wp_register_script('background-parallax', get_template_directory_uri() . '/assets/js/src/background-parallax.js', array(), '1.0', true);
    wp_register_script('sticky', get_template_directory_uri() . '/assets/js/src/sticky.js', array(), '1.0', true);
    wp_register_script('menu', get_template_directory_uri() . '/assets/js/src/menu.js', array(), '1.0', true);
    wp_register_script('placeholder', get_template_directory_uri() . '/assets/js/src/placeholder.js', array(), '1.0', true);


    wp_register_script('wnumb', get_template_directory_uri() . '/assets/js/lib/wnumb.js', array(), '1.0', true);
    wp_register_script('form', get_template_directory_uri() . '/assets/js/src/form.js', array(), '1.0', true);

    wp_register_script('slick', get_template_directory_uri() . '/assets/js/lib/slick.js', array(), '1.0', true);
    wp_register_script('slider', get_template_directory_uri() . '/assets/js/src/slider.js', array(), '1.0', true);
    wp_register_script('maps', get_template_directory_uri() . '/assets/js/src/maps.js', array(), '1.0', true);
    wp_register_script('magnific-popup', get_template_directory_uri() . '/assets/js/lib/jquery.magnific-popup.js', array(), '1.0', true);
    wp_register_script('lightbox', get_template_directory_uri() . '/assets/js/src/lightbox.js', array(), '1.0', true);
    wp_register_script('wishlist', get_template_directory_uri() . '/assets/js/src/wishlist.js', array(), '1.0', true);

    wp_register_script('script', get_template_directory_uri() . '/assets/js/src/script.js', array(), '1.0', true);
    wp_register_script('footer', get_template_directory_uri() . '/assets/js/src/footer.js', array(), '1.0', true);
    wp_register_script('scroll-with-anchor', get_template_directory_uri() . '/assets/js/src/scroll-with-anchor.js', array(), '1.0', true);
    wp_register_script('responsive-tables', get_template_directory_uri() . '/assets/js/src/responsive-tables.js', array(), '1.0', true);

    wp_register_script('addtohomescreen', get_template_directory_uri() . '/assets/js/lib/addtohomescreen.js', array(), '1.0', true);




    wp_enqueue_script(array( 'propertyListing',
        'picturefill',
        'picture','jquery-once','drupal','lazysizes','aspectratio','cookie','ajax','jquery_update','cookie','fr_ciqikf4IhNmagPtrxTvp5CqUvHDO4IUgE',
        'google','m3_properties','captcha','textarea','webform','progress','console','loadcss','fast-click','hoverintent','superfish','jquery-placeholder',
        'fonts-css-async','background-parallax','sticky','menu','placeholder','wnumb','form','slick','slider','maps','magnific-popup','lightbox','wishlist',
        'script','footer','scroll-with-anchor'.'responsive-tables','addtohomescreen'
    ));


//    if(is_page_template('page-templates/template-property-listing.php')) {
//        //wp_enqueue_script( 'm3_properties.search',get_template_directory_uri() . '/assets/js/modules/m3_properties/js/m3_properties.search.js',array(),1.0,true);
//
//
//
//    }

    /*prop detail page*/
    wp_enqueue_script( 'scroll-with-anchor',get_template_directory_uri() . '/assets/js/src/scroll-with-anchor.js',array(),1.0,true);
    wp_enqueue_script( 'responsive-tables',get_template_directory_uri() . '/assets/js/src/responsive-tables.js',array(),1.0,true);


    $propertlisting = array(
        'admin_url' => admin_url('admin-ajax.php'),
        'get_template_directory_uri' => get_template_directory_uri(),
//            'property_type' => $property_type,
        'site_url' => site_url(),


    );

    if(is_page_template('page-templates/template-property-listing.php')) {


        /*listing pages*/
       // wp_enqueue_script( 'm3_properties.search',get_template_directory_uri() . '/assets/js/m3_properties.search.js',array(),1.0,true);


        wp_enqueue_style('jquery-ui-core', get_template_directory_uri() . '/assets/js/modules/jquery_update/replace/ui/themes/base/minified/jquery.ui.core.min.css', array(), '2.0');
        wp_enqueue_style('jquery-ui-theme', get_template_directory_uri() . '/assets/js/modules/jquery_update/replace/ui/themes/base/minified/jquery.ui.theme.min.css', array(), '2.0');
        wp_enqueue_style('jquery-ui-slider', get_template_directory_uri() . '/assets/js/modules/jquery_update/replace/ui/themes/base/minified/jquery.ui.slider.min.css', array(), '2.0');


        wp_enqueue_script( 'jquery.min.js',get_template_directory_uri() . '/assets/js/modules/jquery_update/replace/jquery/1.10/jquery.min.js' );
        wp_enqueue_script( 'jquery-ui.min.js','http://code.jquery.com/ui/1.10.2/jquery-ui.min.js' );
        wp_enqueue_script( 'new-jquery-ui.min.js',get_template_directory_uri() . '/assets/js/modules/jquery_update/replace/ui/ui/minified/jquery-ui.min.js');

        wp_enqueue_script('propertyListing', get_template_directory_uri() . '/assets/js/property.js', array(), '1.0', true);

        //wp_enqueue_script('google-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCwHYuL2hs0CzlpOWXlime6VyGlomHmeDw', 'jquery', '1.0', true);
//        die($property_type);


        wp_localize_script('propertyListing', 'propertyListing', $propertlisting);


        //wp_enqueue_script( 'property-listing',get_template_directory_uri() . '/assets/js/property.js');


    }

    wp_enqueue_script( 'favourite',get_template_directory_uri() . '/assets/js/addFav.js',array(),1.0,true);
    wp_localize_script('favourite', 'addFav', $propertlisting);


    if( is_page_template('page-templates/template-application-form-commerce.php') ||
        is_page_template('page-templates/template-application-form-housing.php') ||
        is_page_template('page-templates/template-application-form-parkinggaragewarehouse.php')) {

        wp_enqueue_script( 'detail_application_form',get_template_directory_uri() . '/assets/js/property_detail_applicationform.js',array(),1.0,true);
        wp_localize_script('detail_application_form', 'detail_application_form', array( 'ref_id' => $_GET['ref'],
                                                                                                            'admin_url' => admin_url('admin-ajax.php'),
                                                                                                            'prop_type' => 'location'));
    }




}

add_action('wp_enqueue_scripts', 'procab_scripts');



/*Body class*/

add_filter('body_class', 'rosset_body_class');

function rosset_body_class($classes) {


    $classes[] = 'html';
    $classes[] = 'not-logged-in';
    $classes[] = 'one-sidebar';
    $classes[] = 'sidebar-first';


     if(is_page_template('page-templates/template-property-listing.php')){
        $classes[] = 'not-front';
        $classes[] = 'page-properties';
        $classes[] = 'page-properties-rent';

        $classes[] = 'properties-scroll-active';

    }

    //if(!is_single())
    if(!is_singular())
        $classes[] = 'processed__menu-sticky';

    if(is_singular())
        $classes[] = 'page-node page-node- page-node-2560 node-type-news';

     //if(is_single())
     if(is_singular("property"))
         $classes[] = 'node-type-property';

/*remove in single page*/
    //$classes[] = 'processed__menu-sticky';


    $classes[] = 'processed__menu';
    $classes[] = 'processed__slider';
    $classes[] = 'processed__datalayer';

//html front not-logged-in one-sidebar sidebar-first page-node page-node- page-node-7 node-type-homepage i18n-fr



    return $classes;
}

/*Newsletter Form*/
add_action("wp_ajax_a_neswletter", "a_neswletter");
add_action("wp_ajax_nopriv_a_neswletter", "a_neswletter");

function a_neswletter() {

    global $wpdb;

    $email = $_REQUEST["email"];

    $results = $wpdb->get_results( $wpdb->prepare( "
        SELECT * FROM  newsletter
        WHERE email = %s",
        $email
    ) , OBJECT );


    if(empty($results)) {

        $wpdb->query( $wpdb->prepare( "
            INSERT `newsletter`
                (email, status, subscription_date)
            VALUES (%s, %s, %s)",
            array($email,1 , time())
        ) );

        $output = array('msg' => "<span class='success-msg'>Déjà enregistré.</span>");
    }
    else {
        $output = array('msg' => "<span class='success-msg'>Inscription réussie. Merci.</span>");

    }

    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($output);
        echo $result;
    }
    else {
        header("Location: ".$_SERVER["HTTP_REFERER"]);
    }

    die();

}

/*favourite priperties*/

add_action("wp_ajax_m3_favourite_property_action", "m3_favourite_property_action");
add_action("wp_ajax_nopriv_m3_favourite_property_action", "m3_favourite_property_action");

function m3_favourite_property_action()
{
    global $wpdb;

    $properties_code = $_POST['favourite_codes'];

    $args = array(
        'post_type' => 'property',
        'post_status' => 'any',
        'meta_query' => array(
            array(
                'key' => 'ref',
                'value' => $properties_code,
                'compare' => 'IN',
            ),
        ),
        'posts_per_page' => -1,
        'no_found_rows'          => true,
        'update_post_term_cache' => false,
        'update_post_meta_cache' => false,
        'cache_results'          => false

    );

    $query = new WP_Query($args);

    $locations = [];
    $ventes = [];
    $list_html = "";

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();

            $id = $query->post->ID;

            $term_list = wp_get_post_terms($id, 'property_type', array("fields" => "all"));
            $propertyType = $term_list[0]->slug;

            $ref = get_field('ref');
            if ($propertyType == "location") {
                $locations[] = $ref;
            }
            else if ($propertyType == "vente") {
                $ventes[] = $ref;
            }

            $list_html .= get_template_part( 'partials/content', 'property-block' );
            /*$list_html .= "<article class='col-4 news__item'>
                            
                            <button class='button__remove'><i class='demo-icon icon-cross'></i></button>
                            <div class='jsPointer'>
                            <a href=''<?php echo get_the_permalink(); ?>' class='full-link'></a>";
                        $list_html .="<header class='news__header'>
                            <p class='button button__small button__main'>".$proType."</p>";

                            if (!empty($images_arr)):
                                $picture_1_filename = $images_arr['picture_1_filename'];
                                if ($picture_1_filename != "" && !empty($picture_1_filename) && file_exists(ABSPATH . '/files/' . $sender_id . '/images/' . $picture_1_filename)):
                                    $img_path = site_url()."/files/".$sender_id."/property/".$propertyType."/s_prop_listing/".$picture_1_filename;
                                else:
                                    $img_path = site_url()."/files/default/default.jpg";
                                endif;

                                 $list_html .= "<picture>
                                                    <img class='lazyloaded' src='$img_path' alt='' title=''>
                                                </picture>";
                            endif;

                        $list_html .= "</header>";

                        $list_html .= "<div class='news__body'>
                                        <p class='icon icon-marker'>".get_field('object_zip').get_field('object_city')."</p>
                                        <h2 class='title'>
                                            <strong>
                                                <a href='".get_the_permalink()."'>".get_the_title()."</a>
                                            </strong>
                                        </h2>";

                             $surface_living = get_field('surface_living');
                             if (!empty($surface_living)):
                                 $list_html .= "<p class='icon icon-surface'>
                                                    <span class='first'>Surface (m²)</span>
                                                    <span
                                                        class='second'>".$surface_living."</span>
                                                </p>";
                             endif;

                            $number_of_rooms = get_field('number_of_rooms');
                            if (!empty($number_of_rooms)) :
                                $list_html .= "<p class='icon icon-room'>
                                                    <span class='first'>Nombre de pièces</span> <span
                                                        class='second'>".$number_of_rooms."</span>
                                                </p>";
                            endif;

                            $floor = get_field('floor');

                            if (!empty($floor)) :
                                 $list_html .="<p class='icon icon-step'>
                                                    <span class='first'>Etage</span>
                                                    <span
                                                        class='second'>".$floor."<sup>e</sup></span>
                                                </p>";
                            endif;

                        $list_html .= "</div>
                                <div class='news__footer'>";
                        $list_html .="<p class='price'>".\App\price_format($currency, $price, $price_unit)."</p>
                                </div>";

                        $list_html .= "<button class='button button__fav button__connection $favsClass is-active inFavourite favPage' code='$ref'></button>
                    </div>
                </article>";*/

        }
        wp_reset_postdata();
    }

    echo json_encode(array('list_html' => $list_html,'locations' => $locations,'ventes' => $ventes));
    die;
}

/*// Create post dropdown function for Contact Form 7
add_action( 'wpcf7_init', 'postselect' );
function postselect() {
    wpcf7_add_shortcode( 'postlist', 'custom_post_select', false ); //If the form-tag has a name part, set this to true.
}
function custom_post_select( $tag ) {
    $posttype = 'property';
    $args = array(
        'post_type' => $posttype,
        'posts_per_page' => 20
    );
    $posts = get_posts( $args );
    $output = "<select name='" . $posttype . "' id='" . $posttype . "' onchange='document.getElementById(\"" . $posttype . "\").value=this.value;'><option></option>";
    // if you've set the name part to true in wpcf7_add_shortcode use $tag['name'] instead of $posttype as the name of the select.
    foreach ( $posts as $post ) {
        $postid = $post->ID;
        $posttitle = get_the_title( $postid );
        $postslug = get_post_field( 'post_name', $postid );
        $output .= '<option value="' . $postslug . '">' . $posttitle . '</option>';
    } // close foreach
    $output .= "</select>";
    return $output;
} // close function
// To use this in your CF7 form you'd use [postlist]*/


add_action( 'wpcf7_init', 'property_detail_refrence_lists' );

function property_detail_refrence_lists() {
    wpcf7_add_form_tag( 'refrence_lists', 'custom_refrence_lists_tag_handler',array('form_id'=>'23') ); // "clock" is the type of the form-tag
}

function custom_refrence_lists_tag_handler( $tag )
{
    $form_id = $tag->options[0];
    //6584 Housing
//6571 commercial
    //6587 parking
    $prop_type= [];
    if($form_id == "6571"){
        $prop_type = ["INDUS"];
    } elseif($form_id == "6584"){
        $prop_type = ["HOUSE" , "APPT"];
    } elseif($form_id == "6587"){
        $prop_type = ["PARK"];
    }else {
        $prop_type = $prop_type;
    }

    $args = array(
        'post_type' => 'property',
        'posts_per_page' => -1,
        'meta_query' => array(
            array(
                'key'     => 'object_category',
                'value'   => $prop_type,
                'compare' => 'IN',
            ),
        ),
    );
    $query = new WP_Query($args);


    $html = '<select required="required" id="edit-submitted-property-property-ref" name="property_ref" class="form-select required"  onchange="getProperty(this.value)">';
    $props = [];
    if ($query->have_posts()) :

        while ($query->have_posts()) : $query->the_post();

            $type = get_field("object_category");
            $props[$type][] = array('ref' => get_field("ref"),
                                'title' => get_the_title());

        endwhile;


        /*Appt Housing*/
        if(isset($props["APPT"]) && !empty($props["APPT"])):
            $html .= '<optgroup label="Appartement">';

            foreach($props["APPT"] as $appt) {
                $html .= '<option value="'.$appt['ref'].'">'.$appt['title'].' ('.$appt['ref'].')</option>';
            }
            $html .= '</optgroup>';
        endif;

        if(isset($props["HOUSE"]) && !empty($props["HOUSE"])):
            $html .= '<optgroup label="Maison">';

            foreach($props["HOUSE"] as $house) {
                $html .= '<option value="'.$house['ref'].'">'.$house['title'].' ('.$house['ref'].')</option>';
            }
            $html .= '</optgroup>';
        endif;
        /*Appt Housing*/

        /*parking*/
        if(isset($props["PARK"]) && !empty($props["PARK"])):
            $html .= '<optgroup label="Parking / Dépôt">';

            foreach($props["PARK"] as $appt) {
                $html .= '<option value="'.$appt['ref'].'">'.$appt['title'].' ('.$appt['ref'].')</option>';
            }
            $html .= '</optgroup>';
        endif;
        /*parking*/


        /*commercial*/
        if(isset($props["INDUS"]) && !empty($props["INDUS"])):
            $html .= '<optgroup label="Commerce/Industrie">';

            foreach($props["INDUS"] as $appt) {
                $html .= '<option value="'.$appt['ref'].'">'.$appt['title'].' ('.$appt['ref'].')</option>';
            }
            $html .= '</optgroup>';
        endif;
        /*commercial*/


    endif;

    $html .= '</select>';

    return $html;

}

add_action("wp_ajax_get_property_from_ref", "get_property_from_ref");
add_action("wp_ajax_nopriv_get_property_from_ref", "get_property_from_ref");
function get_property_from_ref() {
    $ref_id = $_GET['ref'];

    $args = array(
        'post_type' => 'property',
        'posts_per_page' => '1',
        'meta_query' => array(
            array(
                'key' => 'ref',
                'value' => $ref_id,
                'compare' => '=',
            ),
        ),
    );
    $query = new WP_Query($args);

    if ($query->have_posts()) :

        ob_start();
        while ($query->have_posts()) : $query->the_post();
            get_template_part( 'partials/content', 'property-block' );
        endwhile;
        $output = ob_get_contents();

        ob_end_clean();
    endif;

    echo $output;
    die;


}

function insert_fb_in_head()
{
    global $post;
    if (!is_singular()) //if it is not a post or a page
        return;

    echo '<meta property="og:title" content="' . get_the_title() . '"/>';
    echo '<meta property="og:type" content="article"/>';
    echo '<meta property="og:url" content="' . get_permalink() . '"/>';
    echo '<meta property="og:description" content="' . \APP\get_limited_string_with_word(strip_tags(get_the_content()),300) . '"/>';
//    echo '<meta property="og:site_name" content="Your Site NAME Goes HERE"/>';



    if (!is_singular('property')) //if it is not a post or a page
    {
        echo '<meta property="og:image" content="' . the_post_thumbnail_url( 'medium' ) . '"/>';
        return;
    }


    $images = json_decode(get_field("images"));

    if (!empty($images->picture_1_filename))
        $img = $images->picture_1_filename;
    else
        $img = "s_prop_listing.jpg";

    $id = get_the_ID();
    $term_list = wp_get_post_terms($id, 'property_type', array("fields" => "all"));
    $propertyType = $term_list[0]->slug;

    $sender_id = get_field("sender_id");

    if ($sender_id == "m3 REAL ESTATE")
        $sender_id = "app-m3";

    $img =  site_url() . '/files/' . $sender_id . '/property/'.$propertyType.'/s_prop_listing/' . $img;

    echo '<meta property="og:image" content="' . $img . '"/>';
}

add_action('wp_head', 'insert_fb_in_head', 5);

