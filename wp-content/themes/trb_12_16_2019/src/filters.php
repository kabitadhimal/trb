<?php
/**
 * Created by PhpStorm.
 * User: laxman
 * Date: 7/8/2017
 * Time: 9:47 PM
 */

/**
 * add wrapper - template inheritance in wp
 */
add_filter('template_include', ['Procab\\Wp\\Wrapper', 'wrap'], 200);


// function procab_wp_title($title, $sep)
// {
//     global $paged, $page;

//     if (is_feed()) {
//         return $title;
//     }

//     // Add the site name.
//     $title .= get_bloginfo('name', 'display');

//     // Add the site description for the home/front page.
//     $site_description = get_bloginfo('description', 'display');
//     if ($site_description && (is_home() || is_front_page())) {
//         $title = "$title $sep $site_description";
//     }

//     // Add a page number if necessary.
//     if (($paged >= 2 || $page >= 2) && !is_404()) {
//         $title = "$title $sep " . sprintf(__('Page %s', 'RJ'), max($paged, $page));
//     }
// //die($title);
//     return $title;
// }

//add_filter( 'wp_title', 'procab_wp_title', 10, 2 );

//remove stress characters, stress characters

add_filter('wp_handle_upload_prefilter', 'procab_upload_filter', 1, 1);

function procab_upload_filter($file)
{

    $file['name'] = preg_replace('/[^a-zA-Z0-9-_\.]/', '-', $file['name']);

    return $file;

}

/*
 * Redirect to home page if author is 1
 */

add_action('template_redirect', 'procab_template_redirect');
function procab_template_redirect()
{
    if (is_author()) {
        wp_redirect(home_url());
        exit;
    }
}


remove_filter('the_content', 'wpautop');

add_filter('the_content', 'wpautop', 12);


add_filter('excerpt_more', function ($more) {
    return '...';
});

add_filter('excerpt_length', function ($length) {

    if (is_search()) {
        return 26;
    } else {
        return 12;
    }

});


/*
 * Modifying the exisitng Media Player
 */

add_filter('wp_audio_shortcode_override', 'short1_so_23875654', 10, 4);
function short1_so_23875654($return = '', $attr, $content, $instances)
{
    # If $return is not an empty array, the shorcode function will stop here printing OUR HTML
    // $return = '<html code here>';
    if (!empty($attr['mp3'])) :

        $audioContents = ' <div class="audio-wrap js-audio-wrap">
                            <audio controls="controls" preload="auto" class="js-audio__node">
                                <!-- <source src="audio/minions-clip.ogg" type="audio/ogg" /> -->
                                <source src="' . $attr['mp3'] . '" type="audio/mpeg" /></audio></div>';
    endif;

    return $audioContents;
}

;


/*
 * OPen external links in a new tab
 */


/* ================================================================================
        TO UPLOAD SVG IMAGES
  ================================================================================== */
function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');


function m3_logo()
{ ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png);
            height: 100px;
            width: 320px;
            background-size: 320px 65px;
            background-repeat: no-repeat;
            /*padding-bottom: 30px;*/

        }

        #loginform p.submit #wp-submit {
            background: #003767 !important;
            border-color: #003767 !important;

        }

        #wp-submit {
            background: #003767 !important;
            border-color: #003767 !important;
        }

        .login_error > a {
            color: #003767 !important;
        }
    </style>
<?php }

add_action('login_enqueue_scripts', 'm3_logo');

add_filter('login_errors', function ($error) {
    global $errors;
    $err_codes = $errors->get_error_codes();
    //App\debug($err_codes);
    // Invalid username.
    // Default: '<strong>ERROR</strong>: Invalid username. <a href="%s">Lost your password</a>?'
    if (in_array('invalid_username', $err_codes)) {
        $error = '<strong>ERROR</strong>: Invalid username/password.';
    }

    if (in_array('incorrect_password', $err_codes)) {
        $error = '<strong>ERROR</strong>: Invalid username/password.';
    }

    return $error;
});


add_filter('login_headerurl', 'custom_loginlogo_url');
function custom_loginlogo_url($url)
{
    return site_url();
}


/*
* Callback function to filter the MCE settings
*/
// Callback function to filter the MCE settings
function app_mce_before_init_insert_formats($init_array)
{
    // Define the style_formats array
    $style_formats = [

        [
            'title' => 'Dark Button',
            'selector' => 'a',
            'classes' => 'btn btn-dark ls-1 text-uppercase '
        ],
        [
            'title' => 'Blue Button',
            'selector' => 'a',
            'classes' => 'btn btn-primary '
        ],

        [
            'title' => 'White Button',
            'selector' => 'a',
            'classes' => 'btn btn-white '
        ],
        [
            'title' => 'Blue Link',
            'selector' => 'a',
            'classes' => 'text-primary ff-pp '
        ],

        [
            'title' => 'Black Background PDF',
            'selector' => 'ul',
            'classes' => 'button-utilities__list button-utilities__list--pdfblack'
        ],

        [
            'title' => 'White Background PDF',
            'selector' => 'ul', // Element to add class to
            'classes' => 'button-utilities__list button-utilities__list--pdfwhite'
        ]

    ];
    $init_array['style_formats'] = json_encode($style_formats);
    return $init_array;

}

// Attach callback to 'tiny_mce_before_init'
add_filter('tiny_mce_before_init', 'app_mce_before_init_insert_formats');

