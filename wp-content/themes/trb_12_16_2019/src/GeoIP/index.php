<?php

include("src/geoipcity.inc");
require_once 'src/geoipregionvars.php';

$gi = geoip_open("data/GeoLiteCity.dat",GEOIP_STANDARD);


/*echo geoip_country_code_by_addr($gi, "24.24.24.24") . "\t" .
     geoip_country_name_by_addr($gi, "24.24.24.24") . "\n";
echo geoip_country_code_by_addr($gi, "80.24.24.24") . "\t" .
     geoip_country_name_by_addr($gi, "80.24.24.24") . "\n";


echo   _get_region($gi, "80.24.24.24") . "\n";*/



function get_client_ip(){
// /print_r($_SERVER);
    if(isset($_SERVER['HTTP_REMOTE_IP'])){
        $ip = $_SERVER['HTTP_REMOTE_IP'];
    }else if (isset($_SERVER['HTTP_X_REMOTE_IP'])) {
        $ip = $_SERVER['HTTP_X_REMOTE_IP'];
    }else if (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else if(isset($_SERVER['HTTP_X_FORWARDED'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED'];
    }
    else if(isset($_SERVER['HTTP_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_FORWARDED_FOR'];
    }
    else if(isset($_SERVER['HTTP_FORWARDED'])) {
        $ip = $_SERVER['HTTP_FORWARDED'];
    }
    else{
        $ip = $_SERVER['REMOTE_ADDR'];
    }


 //check for multiple ip address
 if($ipIndex = strpos($ip, ',')){
  $ip = substr($ip,0, $ipIndex);
 }
 return $ip;
}

$ip  = get_client_ip();
echo "IP : ".$ip;

echo "<br>";
$result =  GeoIP_record_by_addr($gi, $ip);
echo "Infos : <br>";
print_r($result);

echo "<br>";
echo "Region : ".$GEOIP_REGION_NAME[$result->country_code][$result->region];

geoip_close($gi);

