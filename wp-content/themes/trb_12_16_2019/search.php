<?php
/**
 * The template for displaying search results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

    <!-- Search page simple text block {CMS resuable block} -->
<div class="container position-static">
<?php if (have_posts()) : ?>

    <section class="block block-cms theme-bkg-default-prim box-stretch box-gap-sm text-center">
        <!-- Block div -->
        <div class="block__div wow slideInUp" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
            <h2 class="ucase">Search Result for</h2>
            <h3 class="nogap"><?php printf(__('%s', 'app'), get_search_query()); ?></h3>
        </div><!-- /.Block div -->
    </section><!-- /.Search page simple text block {CMS resuable block} -->


    <!-- Separator / Divider {Please create a shortcode for this} -->
    <!-- <div class="line-separator line-separator-default line-separator-full"></div> -->
    <!-- /.Separator / Divider -->

    <section class="block block--text gap-p-eq text-center bg-faded is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                            <div class="block__b">
                                  <div class="row">

            <?php

            // Start the loop.
            while (have_posts()) : the_post(); ?>
               <?php /* <article class="search-result__post  wow slideInUp" data-wow-duration="1s" data-wow-delay="0.15s"
                         data-wow-offset="20">
                    <div class="gutter-15 block__grid search-result__grid">

                        <div class="col-sm-3 col-xs-12 block-col">
                            <div class="block-col-inner">
                                <figure class="search-result__thumb elem-animate-wrap">
                                    <?php if (has_post_thumbnail()): ?>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_post_thumbnail('large', ['class' => 'img-responsive img-full elem-animate elem-animate--scale']);

                                        else :
                                           // echo "<img class='img-responsive img-full elem-animate elem-animate--scale' src='" . get_template_directory_uri() . "/assets/images/no-preview.png' />";
                                        endif; ?>
                                    </a>
                                </figure>
                            </div>
                        </div>

                        <div class="col-sm-9 col-xs-12 block-col text-center">
                            <div class="block-col-inner">
                                <div class="search-result__div">
                                    <div class="search-result__meta">
                                        <time
                                            datetime="<?php echo get_the_date('Y-m-d'); ?>"><?php echo get_the_date('j F  Y'); ?></time>
                                        -
                                        <span class="search-result__category">Actualite</span> -
                                        <span
                                            class="search-result__author"><?php echo get_the_author_meta('first_name'); ?></span>
                                    </div>
                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                </div>
                                <?php if (!empty(get_the_excerpt())) : ?>
                                    <div class="search-result__content hidden-xs">
                                        <?php the_excerpt(); ?>
                                    </div>
                                <?php endif; ?>

                                    <a href="<?php the_permalink(); ?>"
                                       class="btn btn-primary"><strong>En savoir plus</strong></a>

                            </div>
                        </div>

                    </div>
                </article> <?php */ ?>



                 
                              
                                    <div class="col-sm-4 mb-3 mb-sm-0">
                                        <div class="card shadowed wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">

                                            <figure class="card__pic mb-0">
                                                  <?php if (has_post_thumbnail()): ?>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_post_thumbnail('medium', ['class' => 'imimg img-full img-fluid rounded upper']);

                                        else :
                                           // echo "<img class='img-responsive img-full elem-animate elem-animate--scale' src='" . get_template_directory_uri() . "/assets/images/no-preview.png' />";
                                       ?>
                                    </a>
                                         <?php  endif;  ?>      
                                                </figure>
                                            <!-- Add bg-white to make post body white -->
                                            <div class="card__b border-primary small-line-height bg-white">
                                                <h3 class="text-primary text-uppercase"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                                                <p> <?php the_excerpt(); ?></p>

                                                <a href="<?php the_permalink(); ?>" class="btn btn-outline-dark">Learn More</a>

                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                              
                           

            <?php endwhile; ?>
        </div>
        <div class="text-center search-pagination-wrap gap-top-lg">
            <?php
            // Previous/next page navigation.
            /*
            the_posts_pagination( array(
                'type' => 'list',
                'prev_text'          => __( 'Previous', 'app' ),
                'next_text'          => __( 'Next', 'app' ),
                'screen_reader_text' => '&nbsp;',
                //'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'app' ) . ' </span>',
            ) );
            */

            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $actionUrl = get_home_url() . '/';
            if ($paged == 1) {
                $nextLink = get_next_posts_link('charger la suite', '');
                $nextLink = str_replace('<a', '<a class="button button--dismiss-nobg button--md nogap"', $nextLink);
                echo $nextLink;
            } else {
                \App\custom_pagination("", "", $paged, $actionUrl);
            }
            ?>
       </div><!-- /.Block body ends -->

         </div>
                        </section>

<?php else: ?>

    <section class="block block--text gap-p-eq bg-white is-extended wow fadeInDown text-center" data-wow-duration="1s"
             data-wow-delay="0.15s" data-wow-offset="20">
        <div class="block__b">
            <div class="row">
                <div class="col-sm-12 mb-3 mb-sm-0">
                    <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <!-- Add bg-white to make post body white -->
                        <div class="card__b p-0 pl-sm-2">
                            <h2 class="search-result__message ucase">Aucun résultat trouvé!!</h2>
                            <h3 class="nogap"><?php printf(__('%s', 'app'), get_search_query()); ?></h3>
                            <div class="form-wrap text-container text-container--md gap-top-lg wow slideInUp"
                                 data-wow-duration="1s"
                                 data-wow-delay="0.15s" data-wow-offset="20">
                                <?php // Search form from functions.php // ?>
                                <?php echo get_search_form(); ?>

                            </div><!-- /.Form wrap -->

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>



<?php endif; ?>

</div>
