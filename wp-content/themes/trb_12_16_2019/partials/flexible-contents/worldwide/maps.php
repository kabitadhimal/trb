<?php $continents = []; ?>
<section class="global-presence ">
    <div class="container">
        <figure class="world-map hidden-lg-up"><img src="<?php echo get_template_directory_uri().'/assets/contents/maps-transparent.png'?>" alt="maps-transparent"/> </figure>
        <ul class="tabs clearfix">
            <?php $terms = get_terms(
                array(
                    'taxonomy' => 'map_cat',
                    'hide_empty' => false,
                    'orderby' => 'term_order',
                    'order' => 'ASC',

                )
            );
            if (!empty($terms)):
                foreach ($terms as $ind => $term):
                    ?>
                    <li onclick="" class="js-select-tab js-hide-popup tab-link  <?php echo $ind == 0 ? 'current' : '' ?>"
                        data-tab="tab-map-<?php echo $term->term_id ?>" data-term_id="<?php echo $term->term_id ?>"><span><?php echo $term->name ?></span>
                    </li>
                <?php endforeach;
            endif; ?>

        </ul>
        <div id="static-popup" class="js-static-popup popup-map js-hide-popup"></div>

        <div class="loader">
        <img id="loading-image" src="<?php echo get_template_directory_uri().'/assets/images/ajax-loader.gif'?>" alt="ajax-loader" style="display:none;" />
        </div>

        <div id="js-map-wrapper" class="position-relative">

        </div>

    </div>
</section>

