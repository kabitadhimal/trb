<?php
$text = get_sub_field('text');
$button_text = get_sub_field('button_text');
$button_url = get_sub_field('button_url');
?>
<section class="block block--quote gap-p-eq bg-primary text-white is-extended text-center wow fadeInDown"
         data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="text-container--sm">
        <header class="block__h">
            <?php if (!empty($text)): ?>
                <h2 class="mb-4 text-white"><?php echo $text ?></h2>
            <?php endif; ?>
            <a href="<?php echo $button_url?>" class="btn btn-white"><?php echo $button_text?></a>
        </header>
    </div>
</section>
