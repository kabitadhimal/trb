
<section class="block block--text gap-p-eq bg-faded is-extended wow fadeInDown" data-wow-duration="1s"
         data-wow-delay="0.15s" data-wow-offset="20">
    <header class="block__h mb-4 mb-lg-5">
        <h4 class="mb-0"><?php echo $title ?></h4>
    </header>
    <div class="block__b">
        <div class="row">
            <div class="col-sm-6">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s"
                     data-wow-offset="20">

                    <div class="card__b p-0 pr-sm-2">
                        <h3 class="text-primary">Lorem Ipusm Sit Amet</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            incididunt <br/> ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                            aute
                            irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                            nulla
                            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                            officia
                            deserunt mollit anim id est laborum.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s"
                     data-wow-offset="20">

                    <div class="card__b p-0 pl-sm-2">
                        <h3 class="text-primary">&nbsp;</h3>
                        <p>Lorem ipsum dolorenderit in voluptate velit esse cillum dolore eu fugiat nulla
                            pariatur.
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                            deserunt
                            mollit anim id est lccusantium doloremque laudantium, totam rem aperiam, eaque
                            ipsa quae
                            ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                            explicabo. Nemo
                            enim ipsam</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="block block--text gap-p-eq bg-faded is-extended wow fadeInDown" data-wow-duration="1s"
         data-wow-delay="0.15s" data-wow-offset="20">
    <header class="block__h mb-3">
        <h2 class="mb-0">Lorem Ipsum Dolor</h2>
    </header>
    <div class="block__b">
        <div class="row">
            <div class="col-sm-4">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s"
                     data-wow-offset="20">
                    <!-- Add bg-white to make post body white -->
                    <div class="card__b p-0 pr-sm-2">
                        <h3 class="text-primary">Lorem Ipusm Dolor</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            incididunt <br/> ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                            aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                            fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                            culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s"
                     data-wow-offset="20">
                    <!-- Add bg-white to make post body white -->
                    <div class="card__b p-0 pl-sm-2">
                        <h3 class="text-primary">&nbsp;</h3>
                        <p>Lorem ipsum dolorenderit in voluptate velit esse cillum dolore eu fugiat nulla
                            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                            officia deserunt mollit anim id est lccusantium doloremque laudantium, totam rem
                            aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae
                            vitae dicta sunt explicabo. Nemo enim ipsam</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s"
                     data-wow-offset="20">
                    <!-- Add bg-white to make post body white -->
                    <div class="card__b p-0 pl-sm-2">
                        <h3 class="text-primary">&nbsp;</h3>
                        <p>Lorem ipsum dolorenderit in voluptate velit esse cillum dolore eu fugiat nulla
                            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                            officia deserunt mollit anim id est lccusantium doloremque laudantium, totam rem
                            aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae
                            vitae dicta sunt explicabo. Nemo enim ipsam</p>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.Block body ends -->
</section>


<section class="block block--demo gap-p-eq bg-faded is-extended wow fadeIn" data-wow-duration="1s"
         data-wow-delay="0.15s" data-wow-offset="20">
    <header class="block__h text-center">
        <h2 class="mb-3 mb-sm-5">Titre H2 de la page</h2>
    </header>
    <div class="block__b">
        <!-- Action button utilities -->
        <div class="button-utilities">
            <ul class="row button-utilities__list" role="list">
                <li class="col-sm-6 mb-3 mb-sm-4">
                    <a href="" class="button-utilities__link"><i
                            class="button-picto button-picto--pdfBlack"></i><span><strong>Download Your PDF File</strong></span></a>
                </li>
                <li class="col-sm-6 mb-3 mb-sm-4">
                    <a href="" class="button-utilities__link"><i
                            class="button-picto button-picto--pdfBlack"></i><span><strong>Download Your PDF File</strong></span></a>
                </li>
                <li class="col-sm-6 mb-3 mb-sm-4">
                    <a href="" class="button-utilities__link"><i
                            class="button-picto button-picto--pdfBlack"></i><span><strong>Download Your PDF File</strong></span></a>
                </li>
                <li class="col-sm-6 mb-3 mb-sm-4">
                    <a href="" class="button-utilities__link"><i
                            class="button-picto button-picto--pdfBlack"></i><span><strong>Download Your PDF File</strong></span></a>
                </li>
                <li class="col-sm-6 mb-3 mb-sm-4">
                    <a href="" class="button-utilities__link"><i
                            class="button-picto button-picto--pdfBlack"></i><span><strong>Download Your PDF File</strong></span></a>
                </li>
                <li class="col-sm-6 mb-3 mb-sm-4">
                    <a href="" class="button-utilities__link"><i
                            class="button-picto button-picto--pdfBlack"></i><span><strong>Download Your PDF File</strong></span></a>
                </li>
                <li class="col-sm-6">
                    <a href="" class="button-utilities__link"><i
                            class="button-picto button-picto--pdfBlack"></i><span><strong>Download Your PDF File</strong></span></a>
                </li>
            </ul>
        </div><!-- /.Action button utilities ends -->
    </div>
</section>


<section class="block block--demo gap-p-eq bg-white is-extended last wow fadeIn" data-wow-duration="1s"
         data-wow-delay="0.15s" data-wow-offset="20">
    <div class="block__b">
        <!-- CMS page reusable utilities row -->
        <div class="row">
            <div class="col-sm-6 mb-3 mb-sm-0">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s"
                     data-wow-offset="20">
                    <div class="demo-holder mb-3">
                        <!-- Custom html5 audio player -->
                        <div class="audio-wrap js-audio-wrap">
                            <audio controls="controls" preload="auto" class="js-audio__node">
                                <!-- <source src="audio/minions-clip.ogg" type="audio/ogg" /> -->
                                <source src="media/audio/minions-clip.mp3" type="audio/mpeg"/>
                            </audio>
                        </div><!-- /.#Custom html5 audio player -->
                    </div>

                    <div class="demo-holder">
                        <!-- Action button utilities -->
                        <div class="button-utilities">
                            <ul class="button-utilities__list d-flex flex-wrap" role="list">
                                <li>
                                    <a href="" class="button-utilities__link"><i
                                            class="button-picto button-picto--pdfBlack"></i><span><strong>Download PDF</strong></span></a>
                                </li>
                                <li>
                                    <a href="" class="button-utilities__link"><i
                                            class="button-picto button-picto--pdfWhite"></i></a>
                                    <a href="" class="button-utilities__link"><i
                                            class="button-picto button-picto--downArrow"></i></a>
                                </li>
                            </ul>
                        </div><!-- Action button utilities -->
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s"
                     data-wow-offset="20">
                    <div class="demo-holder mb-3">
                        <!-- Social button utilities -->
                        <div class="social-utilities">
                            <ul class="lists lists--social d-flex flex-wrap align-items-center" role="list">
                                <li>
                                    <a href=""><i class="icon icon-facebook icon-faded mr-0"></i></a>
                                </li>
                                <li>
                                    <a href=""><i class="icon icon-twitter icon-faded mr-0"></i></a>
                                </li>
                                <li>
                                    <a href=""><i class="icon icon-instagram icon-faded mr-0"></i></a>
                                </li>
                                <li>
                                    <a href=""><i class="icon icon-youtube icon-faded mr-0"></i></a>
                                </li>
                                <li>
                                    <a href=""><i class="icon icon-pinterest icon-faded mr-0"></i></a>
                                </li>
                            </ul>
                        </div><!-- Social button utilities -->
                    </div>

                    <div class="demo-holder">
                        <!-- Form field utilities -->
                        <div class="form-utilities">
                            <ul class="lists lists--utilities d-flex flex-wrap justify-content-between"
                                role="list">
                                <li>
                                    <div class="h4 mb-sm-3">Checkboxes</div>
                                    <div class="demo-row">
                                        <div class="form-group form-group--custom">
                                            <input type="checkbox" value="" name=""
                                                   class="form-control d-none custom-form-field custom-form-field"
                                                   id="checkbox-1">
                                            <label for="checkbox-1">Unchekced Item</label>
                                        </div>
                                        <div class="form-group form-group--custom mb-0">
                                            <input type="checkbox" value="" name=""
                                                   class="form-control d-none custom-form-field"
                                                   id="checkbox-2" checked>
                                            <label for="checkbox-2">Chekced</label>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="h4 mb-sm-3">Radio buttons</div>
                                    <div class="demo-row">
                                        <div class="form-group form-group--custom">
                                            <input type="radio" value="" name="radio"
                                                   class="form-control d-none custom-form-field"
                                                   id="radio-1">
                                            <label for="radio-1">Unchekced Item</label>
                                        </div>
                                        <div class="form-group form-group--custom mb-0">
                                            <input type="radio" value="" name="radio"
                                                   class="form-control d-none custom-form-field"
                                                   id="radio-2" checked>
                                            <label for="radio-2">Chekced</label>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div><!-- Form field utilities -->
                    </div>
                </div>
            </div>
        </div><!-- /.CMS page reusable utilities row -->
    </div><!-- /.Block body ends -->
</section>














<div class="js-accordion__panel">
    <div class="js-accordion__heading">
        <h4 class="js-accordion__title text-center"><a href="" class="js-accordion__toggler"
                                                       data-toggle-target="js-accordion__panel-collapse">Lorem
                ipsum dolor</a></h4>
    </div>
    <div class="js-accordion__panel-collapse">
        <div class="js-accordion__panel-inner">

            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson
                ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food
                truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a
                bird on it squid sie-origin coffee nulla assumenda shoreditch et. Nihil anim
                keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea
                proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them
                accusamus labore sustainable VHS. <br><br> Anim pariatur cliche reprehenderit, enim
                eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non
                cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid sie-origin coffee
                nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes
                anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                probably haven't heard of them accusamus labore sustainable VHS.</p>

            <a href="" class="js-accordion__toggler up-arrow"
               data-toggle-target="js-accordion__panel-collapse"></a>
        </div>
    </div>
</div>
<div class="js-accordion__panel active">
    <div class="js-accordion__heading">
        <h4 class="js-accordion__title text-center"><a href=""
                                                       class="js-accordion__toggler expanded"
                                                       data-toggle-target="js-accordion__panel-collapse">Lorem
                ipsum dolor</a></h4>
    </div>
    <div class="js-accordion__panel-collapse">
        <div class="js-accordion__panel-inner">
            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson
                ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food
                truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a
                bird on it squid sie-origin coffee nulla assumenda shoreditch et. Nihil anim</p>

            <div class="block__b text-center">

                <!-- Block content -->
                <div class="row">
                    <div class="col-sm-4 mb-3 mb-sm-0">
                        <div class="card shadowed wow fadeInDown" data-wow-duration="1s"
                             data-wow-delay="0.15s" data-wow-offset="20">
                            <figure class="card__pic mb-0"><img alt="Post Thumbnail"
                                                                class="img img-full img-fluid rounded upper"
                                                                src="<?php echo get_template_directory_uri() ?>/assets/contents/placeholder-img-538x357.jpg"/>
                            </figure>
                            <!-- Add bg-white to make post body white -->
                            <div class="card__b border-primary small-line-height bg-white">
                                <h3 class="text-uppercase text-primary">Titre h3 lorem</h3>

                                <p>Lorem ipsum dolor sit amet, consectetur</p>

                                <a href="" class="btn btn-outline-dark ls-1 text-uppercase">Learn
                                    More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 mb-3 mb-sm-0">
                        <div class="card shadowed wow fadeInDown" data-wow-duration="1s"
                             data-wow-delay="0.15s" data-wow-offset="20">
                            <figure class="card__pic mb-0"><img alt="Post Thumbnail"
                                                                class="img img-full img-fluid rounded upper"
                                                                src="<?php echo get_template_directory_uri() ?>/assets/contents/placeholder-img-538x357.jpg"/>
                            </figure>
                            <!-- Add bg-white to make post body white -->
                            <div class="card__b border-primary small-line-height bg-white">
                                <h3 class="text-uppercase text-primary">Titre h3 lorem</h3>

                                <p>Lorem ipsum dolor sit amet, consectetur</p>

                                <a href="" class="btn btn-outline-dark ls-1 text-uppercase">Learn
                                    More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 mb-3 mb-sm-0">
                        <div class="card shadowed wow fadeInDown" data-wow-duration="1s"
                             data-wow-delay="0.15s" data-wow-offset="20">
                            <figure class="card__pic mb-0"><img alt="Post Thumbnail"
                                                                class="img img-full img-fluid rounded upper"
                                                                src="<?php echo get_template_directory_uri() ?>/assets/contents/placeholder-img-538x357.jpg"/>
                            </figure>
                            <!-- Add bg-white to make post body white -->
                            <div class="card__b border-primary small-line-height bg-white">
                                <h3 class="text-uppercase text-primary">Titre h3 lorem</h3>

                                <p>Lorem ipsum dolor sit amet, consectetur</p>

                                <a href="" class="btn btn-outline-dark ls-1 text-uppercase">Learn
                                    More</a>
                            </div>
                        </div>
                    </div>

                </div>

                <a href="" class="js-accordion__toggler up-arrow"
                   data-toggle-target="js-accordion__panel-collapse"></a>
            </div><!-- ./Block grid -->


        </div>
    </div>
</div>