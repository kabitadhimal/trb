<?php $blocks = get_sub_field('blocks'); ?>
<style type="text/css">
    #c-image {
        position: relative;
        overflow: hidden;
    }

    #c-image:before {
        content: "";
        display: block;
        padding-bottom: 64.28571428571429%;
    }

    #c-image img {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        opacity:0;
        /*transition: opacity ease-in-out 1.5s;*/
    }

    #c-image img.opaque {
        opacity:1;
    }
</style>
<section class="block block--text home-intro gap-p-eq bg-white is-extended wow fadeInDown" data-wow-duration="1s"
         data-wow-delay="0.15s" data-wow-offset="20">
    <div class="block__b">
        <div class="row align-items-center">
            <div class="col-sm-8 mb-3 mb-sm-0">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <figure class="card__pic mb-0" id="c-image">
                        <?php
                            if (!empty($blocks)):
                                $counter = 0;
                                $output = '';
                                foreach ($blocks as $ind=>$block):
                                    $img = \App\getImageManager()->resize(\App\getImageDirectoryPath($block['image']['url']), \App\IMAGE_SIZE_HOME_WHAT_WE_ARE_SECTION_IMAGE);
                                    $output .= "<a href='". $block['link'] ."'>";
                                    $output .= "<img alt='". $block['title'] ."' class='img img-full img-fluid rounded ". ($counter === 0 ? 'opaque' : '') ."' src='". $img ."' />";
                                    $output .= "</a>";
                                    $counter++;
                                endforeach;
                                echo $output;
                            endif;
                        ?>
                    </figure>
                </div>
            </div>
            <div class="col-sm-4 flex-sm-first">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <div class="card__b pl-0 pr-sm-3 py-0" id="c-imgchange">
                        <?php
                        //  App\debug($blocks);
                        if (!empty($blocks)):
                            foreach ($blocks as $block):
                                $img = \App\getImageManager()->resize(\App\getImageDirectoryPath($block['image']['url']), \App\IMAGE_SIZE_HOME_WHAT_WE_ARE_SECTION_IMAGE);
                                ?>
                        <a href="<?php echo $block['link']?>"  data-img="<?php echo $img?>">
                                <h3 class="text-primary text-uppercase"><?php echo $block['title']?></h3>
                                <p class="mb-2 mb-sm-6"><?php echo $block['text']?> </p>
                            </a>
                            <?php endforeach;
                        endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    $(document).ready(function () {
        $('#c-imgchange a').mouseenter(
            function (e) {
                e.preventDefault();
                //alert($(this).data('img'));
                $this = $(this);
                var img = $this.data('img');
                var link = $this.attr('href');


                $('#c-imgchange').find('a:first h3').removeClass('horizontal-line').addClass('text-primary');
                $this.siblings().find('h3').removeClass('horizontal-line');
                $this.find('h3').removeClass('text-primary').addClass('horizontal-line');


                $('#c-image').find('img').removeClass("opaque");

                var newImage = $(this).index()

                $("#c-image a:eq("+ newImage +") img").addClass("opaque");



               // var img = $(this).data('img');
                $this.parents('.row').find('#c-image img').attr('src', img);
               // $this.parents('.row').find('#c-image').css({
                   // background: 'url('+ img +')'
                //});
                $this.parents('.row').find('#c-image a').attr('href', link);

            }
        ).mouseleave(function() {
            $this = $(this);
            //$this.find('h3').removeClass('horizontal-line').addClass('text-primary');
            // $('#c-image').find('img').removeClass("opaque");
        });
    });
</script>