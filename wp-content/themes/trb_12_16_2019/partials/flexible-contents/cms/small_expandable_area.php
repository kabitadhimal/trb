<?php $expandable_area = get_sub_field('expandable_area'); ?>
<section class="block block-accordion block-accordion-sm box-stretch wow slideInUp gap-p-btm" data-wow-duration="1s"
         data-wow-delay="0.15s" data-wow-offset="20">
    <div class="js-accordion js-accordion--sm" data-scroll-active="true">
        <?php if (!empty($expandable_area)):
            foreach ($expandable_area as $item):?>
                <div class="js-accordion__panel">
                    <div class="js-accordion__heading">
                        <h4 class="js-accordion__title">
                            <a href="" class="js-accordion__toggler"
                               data-toggle-target="js-accordion__panel-collapse"><?php echo $item['title'] ?></a></h4>
                    </div>
                    <div class="js-accordion__panel-collapse" style="display: none;">
                        <div class="js-accordion__panel-inner">
                            <?php echo $item['description'] ?>
                            <?php if ($item['add_table']): ?>
                                <table class="cms-table cms-table--striped cms-table-responsive">
                                    <?php if (!empty($item['table']['header'])): ?>
                                        <thead>
                                        <tr>
                                            <?php foreach ($item['table']['header'] as $header): ?>
                                                <th><h4><?php echo $header['c'] ?></h4></th>
                                            <?php endforeach; ?>
                                        </tr>
                                        </thead>
                                    <?php endif; ?>
                                    <?php if (!empty($item['table']['body'])): ?>
                                        <tbody>
                                        <?php foreach ($item['table']['body'] as $row): ?>
                                            <tr>
                                                <?php if (!empty($row) && is_array($row)):
                                                    foreach ($row as $col):?>
                                                        <td data-th-title="<?php echo $col['c'] ?>"><?php echo $col['c'] ?></td>
                                                    <?php endforeach;
                                                endif; ?>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    <?php endif; ?>
                                </table>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach;
        endif; ?>
    </div>
</section>