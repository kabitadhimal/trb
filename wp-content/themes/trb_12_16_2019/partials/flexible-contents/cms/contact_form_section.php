<?php
$title = get_sub_field('title');
$text = get_sub_field('text');
$contact_form  = get_sub_field('contact_form');
?>
<!-- Reusable simple text block -->
<section class="block block--text gap-p-eq bg-white is-extended text-center wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="text-container--xs">
        <header class="block__h">
            <h2 class="mb-0"><?php echo $title?></h2>
            <?php echo $text?>
        </header>

        <div class="block__b">
            <div class="form mt-4 mt-sm-5 px-0 px-md-12 px-lg-5">
                <div class="cms-form">
                   <?php echo do_shortcode($contact_form);?>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.Reusable simple text block ends -->