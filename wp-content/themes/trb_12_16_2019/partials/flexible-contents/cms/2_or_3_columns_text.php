<?php

$main_title = get_sub_field('main_title');

$select_column = get_sub_field('select_column');
$first_column_title = get_sub_field('1st_column_title');
$first_column_description = get_sub_field('1st_column_description');

$second_column_title = get_sub_field('2nd_column_title');
$second_column_description = get_sub_field('2nd_column_description');

$select_background_color = get_sub_field('select_background_color');
?>
<!-- Reusable simple text block -->
<section class="block block--text gap-p-eq bg-<?php echo $select_background_color?> <?php echo $select_background_color=='primary'?'text-white':''?>  is-extended wow fadeInDown" data-wow-duration="1s"
         data-wow-delay="0.15s" data-wow-offset="20">

    <?php if (!empty($main_title)): ?>
        <header class="block__h mb-3">
            <h2 class="mb-0"><?php echo $main_title; ?></h2>
        </header>
    <?php endif; ?>

    <div class="block__b">
        <div class="row">
            <div class="col-sm-<?php echo $select_column == '2' ? '6' : '4' ?> mb-3 mb-sm-0">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <!-- Add bg-white to make post body white -->
                    <div class="card__b p-0 pl-sm-2">
                        <?php if (!empty($first_column_title)): ?>
                            <h3 class="text-primary" ><?php echo $first_column_title ?></h3>
                        <?php endif; ?>
                        <?php echo $first_column_description;?>
                    </div>
                </div>
            </div>
            <div class="col-sm-<?php echo $select_column == '2' ? '6' : '4' ?> mb-3 mb-sm-0">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <!-- Add bg-white to make post body white -->
                    <div class="card__b p-0 pl-sm-2">
                        <?php if (!empty($second_column_title)): ?>
                            <h3 class="text-primary" ><?php echo $second_column_title ?></h3>
                        <?php endif; ?>
                        <?php echo $second_column_description;?>
                    </div>
                </div>
            </div>

            <?php if (!empty($select_column) && $select_column == '3'):
                $third_column_title = get_sub_field('3rd_column_title');
                $third_column_description = get_sub_field('3rd_column_description');
                ?>
                <div class="col-sm-4">
                    <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <!-- Add bg-white to make post body white -->
                        <div class="card__b p-0 pl-sm-2">
                            <?php if (!empty($third_column_title)): ?>
                                <h3 class="text-primary" ><?php echo $third_column_title ?></h3>
                            <?php endif; ?>
                           <?php echo $third_column_description;?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div><!-- /.Block body ends -->
</section><!-- /.Reusable simple text block ends -->
