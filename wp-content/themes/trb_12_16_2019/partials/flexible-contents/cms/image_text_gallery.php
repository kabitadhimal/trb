<?php
$title = get_sub_field('title');
$description = get_sub_field('description');
$invert_blocks = get_sub_field('invert_blocks');
$section_equal_padding_size = get_sub_field('section_equal_padding_size');
$section_padding = get_sub_field('section_padding');
$gallery = get_sub_field('gallery');


?>
<?php if ($invert_blocks):?>
    <!-- Reusable simple text block -->
    <section class="block block--text <?php echo $section_equal_padding_size ?> <?php echo $section_padding ?> gap-p-eq pt-0 bg-faded is-extended wow fadeInDown" data-wow-duration="1s"
             data-wow-delay="0.15s" data-wow-offset="20">

        <div class="block__b">
            <div class="row align-items-center">
                <div class="col-sm-6 mb-3 mb-sm-0">
                    <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <div class="swiper-container slider slider--posts js-slider-hero">
                            <div class="swiper-wrapper slider__wrapper">
                                <?php if (!empty($gallery)):
                                    foreach ($gallery as $ga):
                                        $alt= $ga['alt'];
                                        if(empty($alt)){
                                            $alt= strip_tags($title);
                                        }
                                        ?>
                                        <div class="swiper-slide slider__slide">
                                            <div class="slider__b">
                                                <figure class="slider__pic mb-0 text-center"><img
                                                        alt="<?=$alt?>" data-no-lazy="1"
                                                        class="img img-full rounded"
                                                        src="<?php echo $ga['url'] ?>"/>
                                                </figure>
                                            </div>
                                        </div>
                                    <?php endforeach;
                                endif; ?>
                            </div>
                            <!-- If we need pagination -->
                            <div class="swiper-pagination d-none"></div>
                            <!-- Slider controls -->
                            <div class="swiper-button swiper-button-next"><i
                                    class="icon icon-right-chev icon-faded mr-0"></i></div>
                            <div class="swiper-button swiper-button-prev"><i
                                    class="icon icon-left-chev icon-faded mr-0"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <div class="card__b pl-sm-3 pr-0 py-0">
                            <?php if (!empty($title)): ?>
                                <h2><?php echo $title; ?></h2>
                            <?php endif; ?>
                            <?php echo $description ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.Block body ends -->
    </section><!-- /.Reusable simple text block ends -->

<?php else:?>

    <section class="block block--text <?php echo $section_equal_padding_size ?> <?php echo $section_padding ?> gap-p-eq bg-faded is-extended wow fadeInDown" data-wow-duration="1s"
             data-wow-delay="0.15s" data-wow-offset="20">

        <div class="block__b">
            <div class="row align-items-center">
                <div class="col-sm-6 mb-3 mb-sm-0">
                    <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <div class="swiper-container slider slider--posts js-slider-hero">
                            <div class="swiper-wrapper slider__wrapper">
                                <?php if (!empty($gallery)):
                                    foreach ($gallery as $ga):
                                        $alt= $ga['alt'];
                                        if(empty($alt)){
                                            $alt= strip_tags($title);
                                        }
                                        ?>
                                        <div class="swiper-slide slider__slide">
                                            <div class="slider__b">
                                                <figure class="slider__pic mb-0 text-center"><img
                                                        alt="<?=$alt?>" data-no-lazy="1"
                                                        class="img img-full rounded"
                                                        src="<?php echo $ga['url'] ?>"/>
                                                </figure>
                                            </div>
                                        </div>
                                    <?php endforeach;
                                endif; ?>
                            </div>
                            <!-- If we need pagination -->
                            <div class="swiper-pagination d-none"></div>
                            <!-- Slider controls -->
                            <div class="swiper-button swiper-button-next"><i
                                    class="icon icon-right-chev icon-faded mr-0"></i></div>
                            <div class="swiper-button swiper-button-prev"><i
                                    class="icon icon-left-chev icon-faded mr-0"></i></div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 flex-sm-first">
                    <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <div class="card__b pl-0 pr-sm-3 py-0">
                            <?php if (!empty($title)): ?>
                                <h2><?php echo $title; ?></h2>
                            <?php endif; ?>
                            <?php echo $description; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.Block body ends -->
    </section><!-- /.Reusable simple text block ends -->
<?php endif;