<?php
$background_color = get_sub_field('background_color');
$title = get_sub_field('title');
$image = get_sub_field('background_image');
$img="";
if (!empty($image)) {
    if ($image['mime_type'] == 'image/svg+xml') {
        $img = $image['url'];
    } else {
        $img = $image['url'];
        //$img = \App\getImageManager()->resize(\App\getImageDirectoryPath($image['url']), \App\IMAGE_SIZE_CMS_BACKGROUND_IMAGE);
    }
}
?>
<section class="promo promo--bkg small-banner-height text-right gap-p-sm has-cover is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" style="background-image:url(<?php echo $img?>);">
    <div class="promo__c is-floated center">
        <div class="row justify-content-end align-items-center">
            <div class="col-sm-10">
                <div class="card text-white wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s" data-wow-offset="20">
                    <?php if(!empty($title)):?>
                    <div class="fw-r mb-0"><h1><?php echo $title ?></h1></div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div><!-- /.Prom content ends -->
</section>

