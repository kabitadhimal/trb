<?php
$select_background_color = get_sub_field('background_color');
$title = get_sub_field('title');
$text = get_sub_field('text');
//app\debug($table);

?>
<!-- Reusable simple text block -->
<section style="padding-top: 0%; padding-bottom: 0%;" class="block block--quote gap-p-eq bg-<?php echo $select_background_color?> <?php echo $select_background_color=='primary'?'text-white':''?> is-extended text-center wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="text-container--sm">
        <header class="block__h">
            <?php if(!empty($title)):?>
            <h2 class="mb-0 text-white"><?php echo $title?></h2>
            <?php endif;?>
            <?php echo $text?>
        </header>
    </div>
</section><!-- /.Reusable simple text block ends -->