<?php
$select_background_color = get_sub_field('select_background_color');

$select_column = get_sub_field('select_column');


$first_column_image = get_sub_field('1st_column_image');
$first_column_title = get_sub_field('1st_column_title');
$first_column_description = get_sub_field('1st_column_description');
//App\debug($first_column_image );

$firstImageAlt = !empty($first_column_image['alt'])?$first_column_image['alt']:strip_tags($first_column_title);

$second_column_image = get_sub_field('2nd_column_image');
$second_column_title = get_sub_field('2nd_column_title');
$second_column_description = get_sub_field('2nd_column_description');
$secondImageAlt = !empty($second_column_image['alt'])?$second_column_image['alt']:strip_tags($second_column_title);
//app\debug($select_column);
?>

<!-- Reusable simple text block -->
<section class="block block--text gap-p-eq bg-<?php echo $select_background_color?> <?php echo $select_background_color=='primary'?'text-white':''?> is-extended wow fadeInDown" data-wow-duration="1s"
         data-wow-delay="0.15s" data-wow-offset="20">
<!--    <header class="block__h text-center mb-4 mb-lg-5">-->
<!--        <h2 class="mb-0">Mise en page 2 colonnes</h2>-->
<!--        <p>Lorem ipsum dolor sit amet it consectetur dolor sit amet oirutz eiutodh ciid</p>-->
<!--    </header>-->
    <div class="block__b">
        <div class="row">

            <div class="col-sm-<?php echo $select_column == '3' ? '4' : '6' ?> mb-3 mb-sm-0">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <?php
                    if (!empty($first_column_image['url'])):
                        if ($select_column == '3'):
                            $img = \App\getImageManager()->resize(\App\getImageDirectoryPath($first_column_image['url']), \App\IMAGE_SIZE_CMS_THREE_COL_IMAGE);
                        else :
                            $img = \App\getImageManager()->resize(\App\getImageDirectoryPath($first_column_image['url']), \App\IMAGE_SIZE_CMS_TWO_COL_IMAGE);
                        endif;
			//echo $img;
                        ?>
                        <figure class="card__pic mb-0"><img alt="<?=$firstImageAlt?>" class="img img-full img-fluid rounded"
                                                            src="<?php echo $first_column_image['url'] ?>"/>
                        </figure>
                    <?php endif; ?>
                    <!-- Add bg-white to make post body white -->
                    <div class="card__b bg-white">
                        <?php if (!empty($first_column_title)): ?>
                            <h3 class="text-primary"><?php echo $first_column_title ?></h3>
                        <?php endif; ?>
                        <?php echo $first_column_description ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-<?php echo $select_column == '3' ? '4' : '6' ?>">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <?php
                    if (!empty($second_column_image['url'])):
                        if ($select_column == '3'):
                            $img = \App\getImageManager()->resize(\App\getImageDirectoryPath($second_column_image['url']), \App\IMAGE_SIZE_CMS_THREE_COL_IMAGE);
                        else :
                            $img = \App\getImageManager()->resize(\App\getImageDirectoryPath($second_column_image['url']), \App\IMAGE_SIZE_CMS_TWO_COL_IMAGE);
                        endif;
                        ?>
                        <figure class="card__pic mb-0">
                            <img alt="<?=$secondImageAlt?>" class="img img-full img-fluid rounded"
                                 src="<?php echo $second_column_image['url']?>"/>
                        </figure>
                    <?php endif; ?>
                    <!-- Add bg-white to make post body white -->
                    <div class="card__b bg-white">
                        <?php if (!empty($second_column_title)): ?>
                            <h3 class="text-primary"><?php echo $second_column_title ?></h3>
                        <?php endif; ?>
                        <?php echo $second_column_description ?>
                    </div>
                </div>
            </div>
            <?php if ($select_column == '3'):
                $third_column_image = get_sub_field('3rd_column_image');
                $third_column_title = get_sub_field('3rd_column_title');
                $third_column_description = get_sub_field('3rd_column_description');
                $img = \App\getImageManager()->resize(\App\getImageDirectoryPath($third_column_image['url']), \App\IMAGE_SIZE_CMS_THREE_COL_IMAGE);
                $thirdImageAlt = !empty($third_column_image['alt'])?$third_column_image['alt']:strip_tags($third_column_title);

                ?>
                <div class="col-sm-4">
                    <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <figure class="card__pic mb-0"><img alt="<?=$thirdImageAlt?>" class="img img-full img-fluid rounded"
                                                            src="<?php echo $third_column_image['url'] ?>"/>
                        </figure>
                        <!-- Add bg-white to make post body white -->
                        <div class="card__b bg-white">
                            <?php if (!empty($third_column_title)): ?>
                                <h3 class="text-primary"><?php echo $third_column_title ?></h3>
                            <?php endif; ?>
                            <?php echo $third_column_description ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div><!-- /.Block body ends -->
</section>


