<?php
$option = get_sub_field('options');
if($option === '1'):
    $image = get_sub_field('background_image');?>
    <section class="video-banner is-extended-full wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
        <?php $videoId = get_sub_field('youtube_video_id'); ?>
        <div class="overflow-hidden">
            <div class="js-player-wrap player-wrap hidden-xs-down" id="player-overlay">
                <div class="js-player-overlay has-cover player-overlay" style="background-image:url(<?php echo $image['url']; ?>);"></div>
                <div id="player"></div>
            </div>
        </div>
        <script>
            video = <?php echo "'".$videoId."'"; ?>;
        </script>
    </section>
<?php elseif($option === '2'):
    $isExtended = get_sub_field('full_extended_video');
    $extended = ($isExtended[0] === '1')? 'is-extended-full': 'is-extended';?>
    <section class="video-banner <?= $extended; ?> wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
        <?php
        $loop_video = get_sub_field('loop_video');
        $video = get_sub_field('video');
        $loop = ($loop_video['0'] == 1)?'loop':''; ?>
        <div class="overflow-hidden">
            <video width="100%" src="<?php echo $video['url']; ?>" autoplay muted <?php echo $loop; ?>></video>
        </div>
    </section>
<?php endif; ?>
