<!-- Home page hero slider -->
<section class="block block--hero position-relative is-extended-full wow fadeInDown" data-wow-duration="1s"
         data-wow-delay="0.15s" data-wow-offset="20">
    <div class="block__b">
        <div class="swiper-container slider slider--hero slider--hero-style2 js-slider-hero">
            <div class="swiper-wrapper slider__wrapper">
                <?php $images = get_sub_field('images');
               // app\debug($images);
                if (!empty($images)):
                    foreach ($images as $image):
                        if(empty( $image['alt'])) {
                            $alt ="slider-image";
                        }
                        ?>
                        <div class="swiper-slide slider__slide">
                            <div class="slider__b">
                                <figure class="slider__pic mb-0 text-center"><img alt="<?=$alt?>?>"
                                                                                  src="<?php echo $image['url'];?>"/>
                                </figure>
                            </div>
                        </div>
                    <?php endforeach;
                endif; ?>
            </div>
            <!-- If we need pagination -->
            <div class="swiper-pagination hidden-xs-down"></div>
            <!-- Slider controls -->
            <div class="swiper-button swiper-button-next"><i class="icon icon-right-chev icon-faded mr-0"></i></div>
            <div class="swiper-button swiper-button-prev"><i class="icon icon-left-chev icon-faded mr-0"></i></div>
        </div>
    </div><!-- /.Block body ends -->
</section><!-- /.Home page hero slider ends -->