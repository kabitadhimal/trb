<?php
$title = get_sub_field('title');
$description = get_sub_field('description');
$invert_blocks = get_sub_field('invert_blocks');
$section_equal_padding_size = get_sub_field('section_equal_padding_size');
$section_padding = get_sub_field('section_padding');
$alt = $image['alt'];
if(empty($alt)){
    $alt = strip_tags($title);
}
?>
<?php if ($invert_blocks):
    $image = get_sub_field('image');
    ?>
    <!-- Reusable simple text block -->
    <section
        class="block block--text <?php echo $section_equal_padding_size ?> <?php echo $section_padding ?> bg-faded is-extended wow fadeInDown"
        data-wow-duration="1s"
        data-wow-delay="0.15s" data-wow-offset="20">

        <div class="block__b">
            <div class="row align-items-center">
                <div class="col-sm-6 mb-3 mb-sm-0">

                    <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <?php if (!empty($image['url'])): ?>
                        <figure class="card__pic mb-0"><img alt="<?=$alt?>" class="img img-full img-fluid rounded"
                                                            src="<?=$image['url']?>"/>
                            <?php endif;
                            ?>
                        </figure>
                    </div>

                </div>
                <div class="col-sm-6">
                    <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <div class="card__b pl-sm-3 pl-0 pr-0 py-0">
                            <?php if (!empty($title)): ?>
                                <h2><?php echo $title; ?></h2>
                            <?php endif; ?>
                            <?php echo $description ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.Block body ends -->
    </section><!-- /.Reusable simple text block ends -->

<?php else:
    $image = get_sub_field('image');
    $image_url = "";
    $alt = $image['alt'];
    if(empty($alt)){
        $alt = strip_tags($title);
    }
    ?>

    <section
        class="block block--text <?php echo $section_equal_padding_size ?> <?php echo $section_padding ?> bg-faded is-extended wow fadeInDown"
        data-wow-duration="1s"
        data-wow-delay="0.15s" data-wow-offset="20">

        <div class="block__b">
            <div class="row align-items-center">
                <div class="col-sm-6 mb-3 mb-sm-0">
                    <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <?php if (!empty($image['url'])): ?>
                            <figure class="card__pic mb-0"><img alt="<?=$alt?>"
                                                                class="img img-full img-fluid rounded"
                                                                src="<?=$image['url']?>"/>
                            </figure>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-sm-6 flex-sm-first">
                    <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <div class="card__b pl-0 pr-sm-3 pr-0 py-0">
                            <?php if (!empty($title)): ?>
                                <h2><?php echo $title; ?></h2>
                            <?php endif; ?>
                            <?php echo $description; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.Block body ends -->
    </section><!-- /.Reusable simple text block ends -->
<?php endif;