<div class="container">
    <section class="block block--text gap-p-eq bg-white is-extended wow fadeInDown" data-wow-duration="1s"
             data-wow-delay="0.15s" data-wow-offset="20">
        <div class="block__b">
            <div class="row">

                <div class="col-sm-12 mb-3 mb-sm-0">
                    <div class="card text-container--sm wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <?php $img = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));?>
                        <figure class="card__pic mb-0"><img alt="Post Thumbnail" class="img img-fluid rounded" src="<?php echo $img; ?>" />
                        </figure>
                        <?php /*<img alt="Post Thumbnail" class="img img-full img-fluid rounded" src="<?php echo $img; ?>" /> */ ?>
                        <div class="card__bx pt-3 pb-3 bg-white">
                                <h1 class="text-primary"> <?php the_title() ?></h1>
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>

            </div>
        </div><!-- /.Block body ends -->
    </section>
</div>