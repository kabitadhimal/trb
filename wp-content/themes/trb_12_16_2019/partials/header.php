 <!-- Site header -->
    <header id="masthead" class="topnavbar topnavbar--is-sticky wow fadeInDown" data-wow-duration="0.5s"
            data-wow-delay="0.11s" data-wow-offset="20" role="banner">
        <div class="collapsable-panel-group js-collapse">
            <div class="collapsable-panel bg-primary">
                <div class="container">
                    <div class="form search-form">



                        <form method="get" id="searchform" action="<?php bloginfo('url'); ?>" />
                            <div class="form-group mb-0 position-relative">
                                <input class="form-control form-control--lg border-fat border-white" type="text"
                                       placeholder="Rechercher sur le site" name="s" id="s" value="<?php the_search_query(); ?>"/>
                                <button type="submit" class="form__btn is-floated"><i
                                        class="icon icon-light icon-md icon-search mr-0"></i></button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.Site container ends -->
            </div><!-- /.Collapsable panel ends -->
        </div><!-- /.Collapsable panel group ends -->


        <div class="upper-header bg-primary hidden-sm-down">
            <div class="container">

                <ul class="justify-content-end d-flex">
                    <li class="dropdown"><a class="dropdown-toggle" href="#" id="dropdownMenu2" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">Global website
                            <span class="select2-selection__arrow"></span></a>

                        <span class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <?php $globalWebsites = get_field('global_websites', 'options');
                            if (!empty($globalWebsites)):
                                foreach ($globalWebsites as $website):
                                    ?>

                                    <a class="dropdown-item popup-with-zoom-anim" href="#small-dialog" data-link="<?php echo $website['url']['url']?>"><?php echo $website['url']['title']?></a>

                                <?php endforeach;
                            endif; ?>


                              </span>

                    </li>

                  <?php /*  <li class="dropdown"><a class="dropdown-toggle" href="#" id="dropdownMenu2" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">En<span
                                class="select2-selection__arrow"></span></a>

                        <span class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                <a href="" class="dropdown-item">Fr</a>
                                <a href="" class="dropdown-item">En</a>
                              </span>

                    </li> */ ?>

                    <li><a href="<?php echo site_url().'/contact-us' ?>">Contact Us</a></li>

                  <?php /*  <li><a href="http://www.doccheck.com/com/" target="_blank"><i class="icon icon-lock"></i>Healthcare
                            professional</a></li> */?>
                </ul>

            </div>
        </div>


        <div class="topnavbar__c">
            <div class="container">
                <div class="d-flex align-items-center topnavbar-row">
                    <div class="topnavbar-row__i topnavbar-row__i--l">
                        <!--<h1 class="m-0"> yvf modif-->
                            <a href="<?php echo site_url() ?>" class="brand">
                                <?php  $logo = get_field('logo','options');

                                ?>
                                <figure class="brand__pic m-0 text-center"><img alt="<?php echo $logo['alt'] ?>"
                                                                                class="site-logo"
                                                                                src="<?php echo $logo['url'] ?>">
                                   <?php /* <figcaption class="brand-tagline text-uppercase text-white h6 d-none">La vie côté
                                        shopping
                                    </figcaption> <?php */?>
                                </figure>
                            </a>
                       <!-- </h1> yvf modif-->
                    </div>
                    <div class="topnavbar-row__i topnavbar-row__i--r">
                        <?php require get_template_directory() . '/partials/menu.php'; ?>
                    </div>
                </div>
            </div><!-- /.Main container ends -->
        </div><!-- /.Site header content ends -->
    </header><!-- /.Site header ends -->













