<footer class="site-footer wow fadeInDown gap-p-eq">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <?php echo get_field('first_column', 'option'); ?>
            </div>

            <div class="col-sm-3 col-md-2">
                <?php echo get_field('second_column', 'option'); ?>

            </div>

            <div class="col-sm-3 col-md-2">
                <?php echo get_field('third_column', 'option'); ?>
            </div>

            <div class="col-sm-2">
                <?php echo get_field('fourth_column', 'option'); ?>
            </div>

            <div class="col-md-3">

                <div class="footer-widget text-center text-sm-left">
                    <h4 class="footer-widget__t">Newsletter</h4>
                    <div class="para text-small mb-3 text-white">Subscribe to our newsletter and we will inform you
                        about new product.
                    </div>
                    <div class="form">
                        <span class="message"></span>
                        <?php echo do_shortcode('[mc4wp_form id="225"]');?>
                        <form class="newsletter-form js-newsletter-form">
                            <div class="form-group mb-0 position-relative">

                               <?php /* <input type="text" name="email" id="email" class="form-control newsletter-form__control"
                                       placeholder="Your email address"/>
                                <button role="button" class="newsletter-form__btn js-newsletter-submit"><i
                                        class="icon icon-newsletter text-white"></i></button> */ ?>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="social-utilities mt-4">
                    <ul class="lists lists--social d-flex flex-wrap align-items-center" role="list">
                        <?php $social_links = get_field('social_links', 'option');
                        //App\debug($social_links);
                        ?>
                        <?php if (!empty($social_links['facebook_link'])): ?>
                            <li>
                                <a href="<?php echo $social_links['facebook_link']?>"
                                   target="_blank"><i class="icon icon-facebook icon-faded mr-0"></i></a>
                            </li>
                        <?php endif; ?>

                        <?php if (!empty($social_links['twitter_link'])): ?>
                        <li>
                            <a href="<?php echo $social_links['twitter_link']?>" target="_blank"><i
                                    class="icon icon-twitter icon-faded mr-0"></i></a>
                        </li>
                        <?php endif; ?>
                        <?php if (!empty($social_links['youtube_link'])): ?>
                        <li>
                            <a href="<?php echo $social_links['youtube_link']?>"
                               target="_blank"><i class="icon icon-youtube icon-faded mr-0"></i></a>
                        </li>
                        <?php endif; ?>
                        <?php if (!empty($social_links['linkedin_url'])): ?>
                        <li>
                            <a href="<?php echo $social_links['linkedin_url']?>" target="_blank"><i
                                    class="icon icon-linkedin icon-faded mr-0"></i></a>
                        </li>
                        <?php endif; ?>
                    </ul>
                </div><!-- Social button utilities -->

            </div>


        </div>

        <div class="below-footer">
            <div class="row">
                <div class="col-sm-4">© <?php echo date('Y') ?> TRB Chemedica International SA</div>
                <div class="col-sm-8 flex d-flex ">
                    <ul class="flex d-flex list-first">
                       <!-- yvf modification <li><i class="icon icon-lo text-center"></i>healthcare professional</li> -->
                    </ul>

                    <ul class="flex d-flex justify-content-end">
                        <?php

                        $menu = wp_get_nav_menu_items('Footer sitemap');
                        if (!empty($menu)):
                            foreach ($menu as $m):
                                ?>
                                <li><a href="<?php echo $m->url ?>"><?php echo $m->title ?></a></li>
                            <?php endforeach;
                        endif; ?>

                    </ul>
                </div>
            </div>
        </div>

    </div>
</footer>

</div>
<!-- Back to top -->
<div class="back-top-wrap">
    <a href="" id="js-back-top-trigger" class="js-back-top-trigger back-top-trigger"><i
            class="chevron chevron--up center"></i></a>
</div><!-- /.Back to top ends -->

<div id="small-dialog" class="zoom-anim-dialog mfp-hide">
    <p> You are about to leave trbchemedica.com. This link will take you to an external website. Medical practices and regulations can be different from one country to another. </p><p>  As a result, the medical information provided in the site which you are going to visit may not be appropriate for product use in your country.</p>

    <a href="#" class="btn btn-primary js-gotonextlink" data-gotolink="" target="_blank">CONTINUE </a>



<!--    You are about to leave trbchemedica.com website.-->


</div>

<!-- Youtube video library -->
<script>
    // https://developers.google.com/youtube/iframe_api_reference
    // 2. This code loads the IFrame Player API code asynchronously.
    if(typeof video != 'undefined' && video){
        if (window.screen.availWidth > 767) {

            var tag = document.createElement('script');

            tag.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        }

        // 3. This function creates an <iframe> (and YouTube player)
        //    after the API code downloads.
        var player;
        var playerOverlay;
        // var videoClearInterval;

        function onYouTubeIframeAPIReady() {
            playerOverlay = jQuery('.js-player-overlay');
            player = new YT.Player('player', {
                height: '100%',
                width: '100%',
                videoId: video, //'YT_VIDEO_ID',
                playerVars: {
                    autoplay: 1,
                    modestbranding: 1,
                    rel: 0,
                    showinfo: 0,
                    controls: 0,
                    disablekb: 1,
                    enablejsapi: 0,
                    vq: 'hd1080',
                },
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                }
            });
        }

        var nYoutubeInterval = -1;

        function getCurrentTime(){
            player.getCurrentTime();
        }

        // 4.
        function onPlayerReady(event) {
            event.target.playVideo();

            nYoutubeInterval = setInterval(function(){

                var time = player.getCurrentTime();
                //console.log(time)
                if(time ==0) {
                    //  console.log('play');
                    player.mute();
                    player.playVideo();
                }
                if(time > 0.3){
                    clearInterval(nYoutubeInterval);
                    playerOverlay.fadeOut();
                    //player.mute();
                }

            }, 50);
        }

        // 5. The API calls this function when the player's state changes.
        //    The function indicates that when playing a video (state=1),
        //    the player should play for six seconds and then stop.
        function onPlayerStateChange(event) {
            if (event.data == 1){
                playerOverlay.fadeOut();
                player.mute();
            }

            if (event.data == YT.PlayerState.ENDED) {
                player.seekTo(0);
            }
        }
    }

</script>



