<main id="main" class="main" role="main">
    <div class="content">
        <div class="container position-static">


            <?php
            if (have_rows('contents')):
                while (have_rows('contents')) : the_row();
                    $layout = get_row_layout();
                    // echo $layout;
                    $tpl = get_template_directory() . '/partials/flexible-contents/cms/' . $layout . '.php';
                    if (file_exists($tpl)) {
                        include $tpl;
                    }
                endwhile;
            else:

                while (have_posts()) : the_post();

                    ?>
                    <section class="block block--text gap-p-eq bg-white is-extended wow fadeInDown"
                             data-wow-duration="1s"
                             data-wow-delay="0.15s" data-wow-offset="20">

                        <?php if (get_the_title()): ?>
                            <header class="block__h mb-3">
                                <h1 class="mb-0"><?php echo get_the_title();
                                    ?></h1>
                            </header>
                        <?php endif; ?>

                        <?php if (get_the_content()): ?>

                            <div class="block__b">
                                <div class="row">
                                    <div class="col-sm-12 mb-3 mb-sm-0"><?php echo get_the_content() ?></div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </section>
                    <?php
                endwhile;

            endif;
            ?>

        </div><!-- /.Site container ends -->
    </div><!-- /.Site content ends -->
</main><!-- /.Site main content wrapper ends -->

<script>
    jQuery(document).ready(function($){
        if($("#main").find("h1").length == 0){
            console.log('H1 tag append');
            $()
        }
    });

</script>


