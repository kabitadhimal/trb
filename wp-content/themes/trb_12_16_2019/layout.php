<?php //include \App\get_main_template(); die();
use App\MetaInfo;
ob_start();
include \App\get_main_template();
$templateContent = ob_get_clean();
?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?> >
<?php get_template_part('partials/head'); ?>
<body <?php body_class(); ?> >
<div class="site-outer">

    <!--<div class="alert alert-danger ie-older-browser-message">-->
    <!--    <div class="ie-message-text">-->
    <!--        <p>You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your-->
    <!--                browser</a> to improve your experience.</p>-->
    <!--    </div>-->
    <!--</div>-->
    <?php
    do_action('get_header');
    get_template_part('partials/header');
    ?>
    <?php // include \App\get_main_template(); ?>
    <?php echo $templateContent ?>
    <?php
    do_action('get_footer');
    get_template_part('partials/footer');
    ?>
    <?php wp_footer(); ?>
</body>
</html>
