<?php

require __DIR__.'/../../../libs/vendor/autoload.php';

define('TEMP_DIR_URI', get_template_directory_uri());
define('TEMP_DIR', get_template_directory());
define('SITEURL', site_url());
define('HOMEURL', home_url());
/*Email footer link*/
//define('LINKEDIN_URL', 'https://www.linkedin.com/');

//define('NEWSLETTER_URL', HOMEURL.'/contact/');
//define('DEMAND_URL', HOMEURL.'/contact/');
define('CURRENT_YEAR', date('Y'));
define('ADMIN_URL', admin_url());



use App\Psr4AutoloaderClass;

if (!class_exists('App\Psr4AutoloaderClass')) {
    require __DIR__ . '/src/autoloader.php';
    Psr4AutoloaderClass::getInstance()->addNamespace('Procab', __DIR__ . '/src/Procab');
    Psr4AutoloaderClass::getInstance()->addNamespace('App', __DIR__ . '/src/App');
}

$include_files = [
    //'src/rosset-customizer.php',
    'src/helpers.php',
    'src/function-map-helpers.php',
    'src/App/MetaInfo.php',
    'src/setup.php',
    'src/filters.php',
    'src/admin.php',
    'src/ajax.php',
    'src/hooks.php',
    
    'src/shortcodes/shortcode-functions.php',
    'src/shortcodes/shortcodes.php',
    'src/Procab/Wp/Wrapper.php'
];

array_walk($include_files, function ($file) {
    if (!locate_template($file, true, true)) {
        trigger_error(sprintf('Couldnot find %s', $file), E_USER_ERROR);
    }
});



unset($include_files);







