<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
//$image_url =  get_template_directory_uri().'/assets/img/not-found.jpg';
////$img1 = \App\getImageManager()->resize(\App\getImageDirectoryPath($image_url), \App\IMAGE_SIZE_HOME_BACKGROUND_lARGE);
?>
<style type="text/css" media="all">
    .background-image__7 {
        background-image: url("<?php echo $image_url ?>");
    }
</style>
<main id="main" class="main" role="main">
    <div class="content">
        <section class="block block--text gap-p-eq bg-white is-extended wow fadeInDown" data-wow-duration="1s"
                 data-wow-delay="0.15s" data-wow-offset="20">
            <div class="block__b">
                <div class="row">
                    <div class="col-sm-12 mb-3 mb-sm-0">
                        <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s"
                             data-wow-offset="20">

                            <div class="card__b p-0 pl-sm-2 text-center">
                                <p>La page demandée n'a pas pu être trouvée.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>
</main>
