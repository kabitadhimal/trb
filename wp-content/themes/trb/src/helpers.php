<?php

namespace App;
//require_once('../../wp-load.php');
// //https://github.com/bradvin/social-share-urls
use Intervention\Image\ImageManager;
use Procab\Media\Manager;
use Procab\Wp\Wrapper;

//use App\MetaInfo;


function isRequestAjax()
{
    return Wrapper::$isRequestAjax;
}

/*
 * Get google Map Link
 */


// //https://github.com/bradvin/social-share-urls
function get_tiny_url($url)
{
    static $cache;
    if ($cache && isset($cache[$url])) {
        //var_dump('got from cache');
        return $cache[$url];
    } else {
        $cache = [];
    }
    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, 'http://tinyurl.com/api-create.php?url=' . $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $data = curl_exec($ch);
    curl_close($ch);

    $cache[$url] = $data;


    return $data;
}


/**
 * @param string $numpages
 * @param string $pagerange
 * @param string $paged
 * http://callmenick.com/post/custom-wordpress-loop-with-pagination
 */
function custom_pagination($numpages = '', $pagerange = '', $paged = '', $base = '')
{

    if (empty($pagerange)) {
        $pagerange = 2;
    }

    /**
     * This first part of our function is a fallback
     * for custom pagination inside a regular loop that
     * uses the global $paged and global $wp_query variables.
     *
     * It's good because we can now override default pagination
     * in our theme, and use this function in default quries
     * and custom queries.
     */
    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }
    if ($numpages == '') {
        global $wp_query;
        $numpages = $wp_query->max_num_pages;
        if (!$numpages) {
            $numpages = 1;
        }
    }

    $base = ($base) ?: get_pagenum_link(1);

    /**
     * We construct the pagination arguments to enter into our paginate_links
     * function.
     */
    $pagination_args = array(
        //'base'            => get_pagenum_link(1) . '%_%',
        'base' => $base . '%_%',
        'format' => 'page/%#%',
        'total' => $numpages,
        'current' => $paged,
        'show_all' => False,
        'end_size' => 1,
        'mid_size' => $pagerange,
        'prev_next' => True,
        'prev_text' => __('&laquo;'),
        'next_text' => __('&raquo;'),
        'type' => 'plain',
        'add_args' => false,
        'add_fragment' => '',
    );

    $paginate_links = paginate_links($pagination_args);

    $paginate_links = str_replace('<a', '<li><a', $paginate_links);
    $paginate_links = str_replace('/a>', '/a></li>', $paginate_links);
    $paginate_links = str_replace("<span class='page-numbers current'", '<li class="active"><span', $paginate_links);
    $paginate_links = str_replace('<span class="page-numbers dots"', '<li class="disabled"><span', $paginate_links);
    $paginate_links = str_replace('</span>', '</span></li>', $paginate_links);


    if ($paginate_links) {
        echo "<nav class='custom-pagination'><ul class='pagination pagination--light'>";
        echo $paginate_links;
        echo "</ul></nav>";
    }

}


function get_main_template()
{
    return Wrapper::$mainTemplate;
}

const IMAGE_SIZE_CMS_BACKGROUND_IMAGE = 'image-size-cms-background-image';
const IMAGE_SIZE_CMS_THREE_COL_IMAGE = 'image-size-cms-three-image';
const IMAGE_SIZE_CMS_TWO_COL_IMAGE = 'image-size-cms-two-image';
const IMAGE_SIZE_CMS_VIDEO_IMAGE = 'image-size-cms-video-image';
const IMAGE_SIZE_CMS_THREE_TEXT_BLOCKS_IMAGE = 'image-size-cms-three-text-blocks-image';
const IMAGE_SIZE_CMS_QUOTE_IMAGE = 'image-size-cms-quote-image';

const IMAGE_SIZE_TEMPLATE_PAGE_BANNER_IMAGE = 'image-size-template-pages-banner-image';

const IMAGE_SIZE_HOME_BANNER_IMAGE = 'image-size-home-banner-image';
const IMAGE_SIZE_HOME_WHAT_WE_ARE_SECTION_IMAGE = 'image-size-home-we-are-section-image';
const IMAGE_SIZE_HOME_MAP_IMAGE = 'image-size-home-map-image';
const IMAGE_SIZE_HOME_NEWS_LISTING_IMAGE = 'image-size-home-news-listing-image';

const IMAGE_SIZE_NEWS_LISTING_IMAGE = 'image-size-news-listing-image';
const IMAGE_SIZE_MENU_IMAGE = 'image-size-menu-image';


/**
 * @return Manager
 */
function getImageManager()
{
    static $imageManager;
    if (!$imageManager) {
        $manager = new ImageManager(['driver' => 'gd']);
        $config = [
            'sizes' => [
                IMAGE_SIZE_CMS_BACKGROUND_IMAGE => [1920, 620, 'fit'],
                IMAGE_SIZE_CMS_THREE_COL_IMAGE => [370, 246, 'fit'],
                IMAGE_SIZE_CMS_TWO_COL_IMAGE => [570, 372, 'fit'],
                IMAGE_SIZE_CMS_VIDEO_IMAGE => [1920, 775, 'fit'],
                IMAGE_SIZE_CMS_THREE_TEXT_BLOCKS_IMAGE => [770, 502, 'fit'],
                IMAGE_SIZE_CMS_QUOTE_IMAGE => [369, 326, 'fit'],
                IMAGE_SIZE_HOME_BANNER_IMAGE => [1920, 580, 'fit'],
                IMAGE_SIZE_HOME_WHAT_WE_ARE_SECTION_IMAGE => [770, 495, 'fit'],
                IMAGE_SIZE_HOME_MAP_IMAGE => [1020, 492, 'fit'],
                IMAGE_SIZE_TEMPLATE_PAGE_BANNER_IMAGE => [1920, 288, 'fit'],
                IMAGE_SIZE_NEWS_LISTING_IMAGE => [370, 266, 'fit'],
                IMAGE_SIZE_MENU_IMAGE => [370, 246, 'fit'],
                IMAGE_SIZE_HOME_NEWS_LISTING_IMAGE => [354, 235, 'fit'],

            ],
            'save_folder' => ABSPATH . '/cache/images',
            'save_url' => site_url() . '/cache/images'
        ];
        $imageManager = new Manager($manager, $config);
    }
    return $imageManager;
}

/**
 * get the directory path of the image
 * @param $imageUri
 * @return string
 * todo check if the image belongs to current website
 */
function getImageDirectoryPath($imageUri)
{
    static $uploadDir;
    if (!$uploadDir) $uploadDir = wp_get_upload_dir();
    $imageUri = str_replace($uploadDir['baseurl'], '', $imageUri);
    return $uploadDir['basedir'] . $imageUri;
}

function is_content_empty($str)
{
    return trim(str_replace('&nbsp;', '', strip_tags($str))) == '';
}

function is_post_content_empty(\WP_Post $post)
{
    return is_content_empty($post->post_content);
}


function getMetaInfoObject()
{
    static $metaInfos;
    if (!$metaInfos) {
        $currentPageLink = "$_SERVER[REQUEST_SCHEME]://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        //  $tinyUrl = get_tiny_url($currentPageLink);
        $metaInfo = new MetaInfo(get_bloginfo('name'),
            get_template_directory_uri() . '/assets/images/logo-site.png',
            $currentPageLink,
            get_bloginfo('description')
        );
        $metaInfos = $metaInfo->getMetaInfo();
    }

    return $metaInfos;
}


/* ---Rosset--- */


function createPaginationLink($item_per_page, $current_page, $total_records, $total_pages)
{
    $current_page = empty($current_page) ? 1 : $current_page;
    $pagination = '';

    if ($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages) { //verify total pages and current page number

        $right_links = $current_page + 3; //show number of pagination box
        $previous = $current_page - 1; //previous link
        $next = $current_page + 1; //next link
        $first_link = true; //boolean var to decide our first link


        if ($current_page > 1) {
            $previous_link = ($previous == 0) ? 1 : $previous;

            $pagination .= '<li class="page-item"><a href="' . get_site_url() . '/newsroom/page/' . $previous_link . '" class="page-link"  aria-label="Previous">
                   <span aria-hidden="true" class="icon icon-right-chev mr-0 icon-left"></span>
                                            <span class="sr-only">Previous</span></a></li>';

            $first_link = false; //set first link to false
        }


        for ($i = 1; $i < $right_links; $i++) { //create right-hand side links
            if ($i <= $total_pages) {
                if ($current_page == $i) {


                    $pagination .= " <li class='page-item current'>" . $i . "</li>";

                } else {
                    $pagination .= '<li class="page-item"><a class="page-link" href="' . get_site_url() . '/newsroom/page/' . $i . '">' . $i . '</a></li>';
                }
            }
        }
        if ($current_page < $total_pages) {
            $next_link = ($i > $total_pages) ? $total_pages : $i;

            $pagination .= ' <li class="page-item"><a href="' . get_site_url() . '/newsroom/page/' . $next . '" class="page-link text-center">
                  <span aria-hidden="true" class="icon icon-right-chev mr-0"></span>
                                            <span class="sr-only">Next</span>
                </a></li>';


        }
//        $pagination .= "</li>";
    }

    $pagi = '<ul class="pagination justify-content-center">';
    $pagi .= $pagination;
    $pagi .= '</ul>';
    return $pagi; //return pagination links
}


function debug($var = '', $die = true)
{
    $array = debug_backtrace();
    echo '<br/>Debugging from ' . $array[0]['file'] . ' line: ' . $array[0]['line'];

    echo '<pre>';
    print_r($var);
    echo '</pre>';

    if ($die !== FALSE) {
        die();
    }
}

function get_month_by_language($code = 'fr', $month)
{
    $lan_arr = array(
        'fr' => array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'décembre'),
        'en' => array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'),
        'de' => array('Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember')
    );
    return $lan_arr[$code][$month - 1];
}


function pageTitle($echo)
{
    $title = "";

    global $paged;
    if (function_exists('is_tag') && is_tag()) {
        $title .= single_tag_title(__("Tag Archive for &quot;", 'circle'), false);
        $title .= '&quot; - ';
    } elseif (is_archive()) {
        $title .= wp_title('', true);
        //$title .= __(' Archive - ' , 'circle');
        $title .= __(' - ', 'circle');

    }

    $title .= get_bloginfo('name', 'display');

    // Add the site description for the home/front page.
    $site_description = get_bloginfo('description', 'display');
    if ($site_description && (is_home() || is_front_page())) {
        //$title = "$title  $site_description";
        $title = __("Geneva real estate, sell & rent", "m3-theme");
    } elseif (is_search()) {
        $title .= __('Search for &quot;', 'circle') . esc_html(get_search_query()) . '&quot; - ';
    } elseif (!(is_404()) && (is_single()) || (is_page())) {
        $title = wp_title('', true);
        //$title .= wp_title('', true);
        //$title .= ' - ';
    } elseif (is_404()) {
        $title .= __('Not Found - ', 'circle');
    } else {
        $title .= get_bloginfo('name');
    }
    if ($paged > 1) {
        $title .= ' - page ' . $paged;
    }

    if (!$echo) return $title;
    echo $title;
}


function email_exists_check($email, $tablename)
{
    global $wpdb;
    $result = $wpdb->get_row("SELECT * FROM $tablename WHERE subscriber_email='" . $email . "'");
    return $wpdb->num_rows > 0 ? $result : false;


}


function generate_token($table, $length = 32)
{
    /* generate random string */
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $token = '';
    for ($i = 0; $i < $length; $i++) {
        $token .= $characters[rand(0, $charactersLength - 1)];
    }

    global $wpdb;

    $sql = "SELECT count(*) FROM " . $table . " as a WHERE a.token='" . $token . "'";
    $count = $wpdb->get_var($sql);

    if ($count > 0) {
        generate_user_guid($length);
    }

    return $token;
}


function center_current_element($arr, $current_element, $number)
{
    $sim_prop_without_current = array_diff($arr, array($current_element));
    $repeated_arr = array_merge($sim_prop_without_current, array_merge($arr, $sim_prop_without_current));
    $position = array_search($current_element, $repeated_arr);

    $final_sorted_arr_left = [];
    $final_sorted_arr_right = [];


    if ($repeated_arr > $number) {
        $loop = ceil($number / 2);
        for ($i = 1; $i <= $loop; $i++) {
            if (isset($repeated_arr[$position + $i]))
                $final_sorted_arr_right[] = $repeated_arr[$position + $i];
            if (isset($repeated_arr[$position - $i]))
                $final_sorted_arr_left[] = $repeated_arr[$position - $i];
        }

        $final_sorted_arr = array_merge($final_sorted_arr_right, array_reverse($final_sorted_arr_left));
    } else {
        $final_sorted_arr = $repeated_arr;
    }


    return array_unique($final_sorted_arr);
}

function get_limited_string_with_word($text, $length)
{
    if (strlen($text) > $length) {
        $text = substr($text, 0, strpos($text, ' ', $length));
    }

    return $text;
}


function check_video_type($url)
{

    if (strpos($url, 'youtube') > 0) :
        return 'youtube';
    elseif (strpos($url, 'vimeo') > 0) :
        return 'vimeo';
    else :
        return 'unknown';
    endif;
}

/*
  Extract youtube video id from given url
 */
function extract_yt_video_id($url)
{
    parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);
    return $my_array_of_vars['v'];
}

/**
 * Extract vimeo video id from given url
 */
function extract_vimeo_video_id($url)
{
    return (int)substr(parse_url($url, PHP_URL_PATH), 1);
}

/*Get active on menu*/
function check_active_menu($url)
{
    $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if ($actual_link == $url) {
        return 'current-menu-item';
    }
    return '';
}

function __procab_menu_hierarchy_creater($location)
{
    $menu_tree = array();
    $menu_locations = get_nav_menu_locations();
    // var_dump($menu_locations);
    $menu = wp_get_nav_menu_object($menu_locations[$location]);
    //var_dump($menu);
    $menu_items = wp_get_nav_menu_items($menu->term_id);
    $new_menu_array = array();
    if (!empty($menu_items)):
        foreach ((array)$menu_items as $key => $menu_item) {
            $new_menu_array[$menu_item->menu_item_parent][] = $menu_item;
        }
        $new_menu_array1 = array();
        foreach ((array)$menu_items as $key => $menu_item) {
            if (isset($new_menu_array[$menu_item->ID])) {
                $menu_item->sub = $new_menu_array[$menu_item->ID];
                if ($menu_item->menu_item_parent == 0) {
                    $new_menu_array1[] = $menu_item;
                }
            }
        }

        $menu_tree = array_splice($new_menu_array[0], 0, 100, $new_menu_array1);
    endif;

    return $menu_tree;
}

function image_alt_by_url($image_url)
{
    global $wpdb;

    if (empty($image_url)) {
        return false;
    }

    $query_arr = $wpdb->get_col($wpdb->prepare("SELECT ID FROM {$wpdb->posts} WHERE guid='%s';", strtolower($image_url)));
    $image_id = (!empty($query_arr)) ? $query_arr[0] : 0;

    return get_post_meta($image_id, '_wp_attachment_image_alt', true);
}

function remove_http($url) {
   $disallowed = array('http://', 'https://');
   foreach($disallowed as $d) {
      if(strpos($url, $d) === 0) {
         return str_replace($d, '', $url);
      }
   }
   return $url;
}


