<?php

/*
**
* Set the content width based on the theme's design and stylesheet.
 *
 * @since SEIC 1.0
 */
if (! function_exists('procab_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     *
     * @sinceprocab 1.0
     */
    function procab_setup()
    {

        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based onprocab, use a find and replace
* to change 'procab' to the name of your theme in all the template files
*/
        load_theme_textdomain('app');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(158, 240, true);

              // Registers a single custom Navigation Menu in the custom menu editor
        //  register_nav_menu('primary', __('Primary Menu', 'procab'));

        register_nav_menus( array(
            'primary_menu' => 'Main Menu',
            'secondary_menu' => 'Footer Menu',
            'dropdown_menu' => 'Dropdown Menu',
//            'navigation_buttons' => 'Navigation Button'
        ) );



        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption'
        ));

        /*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
        add_theme_support('post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
            'gallery',
            'status',
            'audio',
            'chat'
        ));
    }

endif; //procab_setup
add_action('after_setup_theme', 'procab_setup');


/**
 * Register widget area.
 *
 * @since procab 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function procab_widgets_init()
{
    register_sidebar(array(
        'name' => __('Widget Area', 'app'),
        'id' => 'sidebar-1',
        'description' => __('Add widgets here to appear in your sidebar.', 'app'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>'
    ));
}
add_action('widgets_init', 'procab_widgets_init');









