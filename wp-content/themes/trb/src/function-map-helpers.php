<?php 

function wpdocs_selectively_enqueue_admin_script( $hook ) {
	 
    wp_enqueue_script( 'my_custom_script', get_template_directory_uri().'/assets/js/custom_admin.js', array('jquery'), '1.0' );
}
add_action( 'admin_enqueue_scripts', 'wpdocs_selectively_enqueue_admin_script' );
class MySettingsPage
{
   
    private $options;
   
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_map_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_map_page()
    {
      
        add_options_page(
            'Settings Admin', 
            'Get homepage map coordinates', 
            'manage_options', 
            'my-setting-admin', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
     
        ?>
        <script src="<?php echo get_template_directory_uri().'/assets/js/custom_admin.js'?>"></script>

        <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/assets/js/smoke/smoke.css'?>" />
        <script src="<?php echo get_template_directory_uri().'/assets/js/smoke/smoke.min.js'?>"></script>
        <div class="wrap">
            <h2>Click on the image to get position of map</h2> 
            <div id="image-map" style="position:relative">
            	<img src="<?php echo get_template_directory_uri().'/assets/images/location-maps-transparent.png'?>">
            </div>          
           
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'my_option_group', // Option group
            'my_option_name' // Option name
            
        );

        add_settings_section(
            'setting_section_id', // ID
            'Get Map Co-Ordinats', // Title
            array( $this, 'print_section_info' ), // Callback
            'my-setting-admin' // Page
        );         

            
    }


    public function print_section_info()
    {
        print 'Get the coordinate of location to your homepage map';
    } 

    
}


if( is_admin() )
    $my_settings_page = new MySettingsPage();
?>