<?php

function rosset_theme_customizer( $wp_customize ) {
    // Fun code will go here

     /* Header Part */
     $wp_customize->add_section( 'rosset_logo_section' , array(
        'title'       => __( 'Rosset Logo', 'rosset' ),
        'priority'    => 30,
        'description' => 'Upload a logo to replace the default site name and description in the header',
    ) );


    $wp_customize->add_setting( 'rosset_logo' );
   
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'rosset_logo', array(
        'label'    => __( 'Rosset Logo', 'rosset' ),
        'section'  => 'rosset_logo_section',
        'settings' => 'rosset_logo',
    ) ) );

    /* Footer part */
    
    $wp_customize->add_section( 'rosset_footer_section' , array(
        'title'       => __( 'Rosset Footer', 'rosset' ),
        'priority'    => 30,
        'description' => 'Please change Footer setting here',
    ) );


     /* UPSI Logo  */
    $wp_customize->add_setting( 'upsi_logo');

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'upsi_logo', array(
        'label'    => __( 'UPSI Logo', 'rosset' ),
        'section'  => 'rosset_footer_section',
        'settings' => 'upsi_logo',
    ) ) );



    /* UPSI Logo Link */
    $wp_customize->add_setting( 'upsi_logo_link', array(
      'capability' => 'edit_theme_options',
      'sanitize_callback' => 'themeslug_sanitize_url',
    ) );

    $wp_customize->add_control( 'upsi_logo_link', array(
          'type' => 'url',
          'section' => 'rosset_footer_section', 
          'label' => __( 'UPSI Logo' ),
          'description' => __( 'Link to UPSI site' ),
          'input_attrs' => array(
                                 'placeholder' => __( 'http://www.google.com' ),
                                ),
    ) );


    /* SGS Logo  */
    $wp_customize->add_setting( 'sgs_logo');

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'sgs_logo', array(
        'label'    => __( 'SGS Logo', 'rosset' ),
        'section'  => 'rosset_footer_section',
        'settings' => 'sgs_logo',
    ) ) );


    /* SGS Logo Link */
    $wp_customize->add_setting( 'sgs_logo_link', array(
      'capability' => 'edit_theme_options',
      'sanitize_callback' => 'themeslug_sanitize_url',
    ) );

    $wp_customize->add_control( 'sgs_logo_link', array(
          'type' => 'url',
          'section' => 'rosset_footer_section', 
          'label' => __( 'SGS Logo' ),
          'description' => __( 'Link to SGS site' ),
          'input_attrs' => array(
                                 'placeholder' => __( 'http://www.google.com' ),
                                ),
    ) );


 /* Left Text Block */
    $wp_customize->add_setting( 'left_text_block', array(
      'capability' => 'edit_theme_options',
      'default' => 'Lorem Ipsum Dolor',
      'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'left_text_block', array(
      'type' => 'textarea',
      'section' => 'rosset_footer_section',
      'label' =>  __( 'Left Text Block', 'rosset' ),
      'description' =>  __( 'Left Text Block', 'rosset' ),
    ) );


     /* Mid Text Block */
     $wp_customize->add_setting( 'mid_text_block', array(
      'capability' => 'edit_theme_options',
      'default' => 'Lorem Ipsum Dolor Sit',
      'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'mid_text_block', array(
      'type' => 'textarea',
      'section' => 'rosset_footer_section',
      'label' => __( 'Mid Text Block', 'rosset' ),
      'description' => __( 'Mid Text Block', 'rosset' ),
    ) );


    /* Right Text Block */
    $wp_customize->add_setting( 'right_text_block', array(
      'capability' => 'edit_theme_options',
      'default' => 'Lorem Ipsum Dolor Sit amet',
      'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'right_text_block', array(
      'type' => 'textarea',
      'section' => 'rosset_footer_section',
      'label' => __( 'Right Text Block', 'rosset' ),
      'description' => __( 'Right Text Block', 'rosset' ),
    ) );


}

add_action( 'customize_register', 'rosset_theme_customizer' );

function themeslug_sanitize_url( $url ) {
  return esc_url_raw( $url );
}