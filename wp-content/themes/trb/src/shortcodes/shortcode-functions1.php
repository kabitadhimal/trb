<?php

function custom_download_button($atts,$content=null) {
    extract(shortcode_atts(array(
        'src' =>''
    ), $atts));


        if(!empty($src)):

            $downloadContent = ' <ul class="button-utilities__list flex flex--wrap" role="list">
                        <li>
                          <a href="'.$src.'" class="button-utilities__link"><i class="button-picto button-picto--downArrow"></i></a>
                        </li>
                      </ul>';


        endif;

        //assets/media/audio/minions-clip.mp3


    return  $downloadContent;
}

function register_shortcodes(){
    //arrow
    add_shortcode('download-button', 'custom_download_button');
    add_shortcode('bg-white-pdf', 'whitepdf_button');
}
add_action( 'init', 'register_shortcodes');


function whitepdf_button($atts,$content=null) {

    extract(shortcode_atts(array(
        'src' =>''
    ), $atts));


    $pdfContent = '';
    if(!empty($src)):

        $pdfContent = ' <ul class="button-utilities__list flex flex--wrap" role="list">
                        <li>
                          <a href="'.$src.'" class="button-utilities__link"><i class="button-picto button-picto--pdfWhite"></i></a>
                        </li>
                      </ul>';


    endif;

    //assets/media/audio/minions-clip.mp3
    return  $pdfContent;

}






