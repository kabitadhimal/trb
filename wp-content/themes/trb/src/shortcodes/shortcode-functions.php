<?php


function ngl_br() {
    return '<br />';
}


function white_download_button($args, $content = ''){

     $pdfContent = ' <ul class="button-utilities__list flex flex--wrap" role="list">
                        <li>
                          <a href="'.$content.'" class="button-utilities__link"><i class="button-picto button-picto--pdfWhite"></i></a>
                        </li>
                      </ul>';

                      return $pdfContent;

  
}




function black_download_button($args, $content = ''){

   extract(shortcode_atts(array(
        'url' => "#"       
        ), $args));

     $pdfContent = ' <ul class="button-utilities__list d-flex flex--wrap" role="list">
                        <li>
                          <a href="'.$url.'" class="button-utilities__link"><i class="button-picto button-picto--pdfBlack"></i><span><strong>'.$content.'</strong></span></a>
                        </li>
                      </ul>';

                      return $pdfContent;

  
}

 
function ngl_anchor($args, $content = '') {
    extract(shortcode_atts(array(
        'url' => "#"       
        ), $args));

    $tar = 'target="_blank"';
    $html='<a href="'.$url.'" class="ipro-link ipro-link--underline ipro-link__gap--large ipro-link--darkGoldenrod"' . $tar . '>'.do_shortcode($content).'</a>';
    return $html;
}



function download_pdf($args,$content = '') {
 
  $downloadContent = ' <ul class="button-utilities__list flex flex--wrap" role="list">
                        <li>
                          <a href="'.$content.'" class="button-utilities__link"><i class="button-picto button-picto--downArrow"></i></a>
                        </li>
                      </ul>';
                      return $downloadContent;
}

function trb_audio($args, $content = ''){
     $html ='';

   extract(shortcode_atts(array(
        'url' => ""       
        ), $args));
  $html .='<div class="ipro-audio__wrap js-audio-wrap">
                                            <audio controls="controls" preload="auto" class="ipro-audio__elem js-audio">
                                                <source src="'.$url.'" type="audio/mpeg" />
                                            </audio>
                                        </div>';

  return $html;
}




function register_shortcodes() {
   
    
    add_shortcode('white-download-pdf', 'white_download_button');
    add_shortcode('black-download-pdf', 'black_download_button');
  
    add_shortcode('audio-player', 'trb_audio');
  
    add_shortcode('download-pdf', 'download_pdf');
    
}

add_action('init', 'register_shortcodes');
?>