(function () {
    tinymce.create('tinymce.plugins.wp_procab', {
        init: function (ed, url) {

           
             ed.addButton('white-download-pdf', {
                title: 'White Download PDF',
                cmd: 'white-download-pdf',
                text: 'White Download PDF'
            });
             ed.addButton('download-pdf', {
                title: 'Download PDF',
                cmd: 'download-pdf',
                text: 'Download PDF'
            });

              ed.addButton('black-download-pdf', {
                title: 'Black Download PDF',
                cmd: 'black-download-pdf',
                text: 'Black Download PDF'
            });
           
          

          

             ed.addCommand('white-download-pdf', function () {

                ed.execCommand('mceInsertContent', 0, '[white-download-pdf]--PDF URL--[/white-download-pdf]');
            }); 
             ed.addCommand('download-pdf', function () {

                ed.execCommand('mceInsertContent', 0, '[download-pdf]--PDF URL--[/download-pdf]');
            }); 

             ed.addCommand('black-download-pdf', function () {

                ed.execCommand('mceInsertContent', 0, '[black-download-pdf url="--PDF URL--"]--Text--[/black-download-pdf]');
            }); 

            
             
           

        },
        createControl: function (n, cm) {
            return null;
        },
        getInfo: function () {
            return {
                longname: 'Wp Procab Buttons'
            };
        }
    });

    tinymce.PluginManager.add('wp_procab', tinymce.plugins.wp_procab);
})();