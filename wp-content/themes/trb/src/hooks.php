<?php


function rosset_after_setup_theme()
{
    register_nav_menu('header_menu', __('Header Menu', 'rosset'));
    register_nav_menu('footer_sitemap', __('Footer Sitemap', 'rosset'));
}

add_action('after_setup_theme', 'rosset_after_setup_theme');


/**
 * Enqueue scripts and styles.
 *
 * @since procab 1.0
 */
function procab_scripts()
{

    wp_enqueue_style('style', get_template_directory_uri() . '/assets/css/style.css', array(), '2.0');

    wp_deregister_script('jquery');
    wp_enqueue_script('modernizr', get_template_directory_uri() . '/assets/js/vendor/modernizr.min.js', array(), '1.0', true);
    //wp_enqueue_script('smoothscroll', get_template_directory_uri() . '/assets/js/vendor/smoothscroll.min.js', array(), '1.0', true);
    wp_enqueue_script('rellax', get_template_directory_uri() . '/assets/js/vendor/rellax.min.js', array(), '1.0', true);
    wp_enqueue_script('swiper', get_template_directory_uri() . '/assets/js/vendor/swiper.min.js', array(), '1.0', true);
    wp_enqueue_script('wow', get_template_directory_uri() . '/assets/js/vendor/wow.min.js', array(), '1.0', true);


    //  wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/js/vendor/jquery-3.2.1.min.js', array(), '1.0', true);


    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/js/vendor/bootstrap.min.js', array(), '1.0', true);
    wp_enqueue_script('jquery.easing', get_template_directory_uri() . '/assets/js/vendor/jquery.easing-1.4.1.min.js', array(), '1.0', true);
    wp_enqueue_script('nicescroll', get_template_directory_uri() . '/assets/js/vendor/jquery.nicescroll.js', array(), '1.0', true);
    wp_enqueue_script('select2', get_template_directory_uri() . '/assets/js/vendor/select2.full.min.js', array(), '1.0', true);

    wp_enqueue_script('lightgallery', get_template_directory_uri() . '/assets/js/vendor/lightgallery-all.min.js', array(), '1.0', true);
    wp_enqueue_script('mediaelement', get_template_directory_uri() . '/assets/js/vendor/mediaelement.min.js', array(), '1.0', true);


    wp_enqueue_script('sliders', get_template_directory_uri() . '/assets/js/custom/jquery-sliders.js', array(), '1.0', true);
    wp_enqueue_script('custom-select', get_template_directory_uri() . '/assets/js/custom/jquery-custom-select.js', array(), '1.0', true);

    wp_enqueue_script('custom-tab', get_template_directory_uri() . '/assets/js/custom/jquery-custom-tab.js', array(), '1.0', true);
    wp_enqueue_script('custom-collapse', get_template_directory_uri() . '/assets/js/custom/jquery-custom-collapse.js', array(), '1.0', true);
    wp_enqueue_script('simple-accordion', get_template_directory_uri() . '/assets/js/custom/jquery.simple.accordion.js', array(), '1.0', true);
    wp_enqueue_script('collapsible', get_template_directory_uri() . '/assets/js/custom/jquery.simple.collapsible.js', array(), '1.0', true);
    wp_enqueue_script('script', get_template_directory_uri() . '/assets/js/custom/script.js', array(), '1.0', true);

    wp_enqueue_script('menu', get_template_directory_uri() . '/assets/js/custom/jquery-menu.js', array(), '1.0', true);


    //POPUP
    wp_enqueue_script('magnific-js', get_template_directory_uri() . '/assets/js/magnific/jquery.magnific-popup.min.js', array(), '1.0', true);
//   wp_enqueue_style('magnific-style', get_template_directory_uri() . '/assets/js/magnific/magnific-popup.css', array(), '1.2', true);
//    wp_enqueue_style('popup-style', get_template_directory_uri() . '/assets/css/popup.css', array(), '1.0', true);

    if (is_page_template('page-templates/template-worldwide.php')):
        wp_enqueue_script('map-popup', get_template_directory_uri() . '/assets/js/custom/js-map-popup.js', array(), '1.0', true);
    endif;


}

add_action('wp_enqueue_scripts', 'procab_scripts');


/*Body class*/

add_filter('body_class', 'trb_body_class');

function trb_body_class($classes)
{


    $classes[] = 'page';

//
//    if (is_front_page()) {
//        $classes[] = 'front';
//    } else {
//        $classes[] = 'not-front';
//    }
//    if (is_page_template('page-templates/template-property-listing.php')) {
//        $classes[] = 'not-front';
//        $classes[] = 'page-properties';
//        $classes[] = 'page-properties-rent';
//
//        $classes[] = 'properties-scroll-active';
//
//    }

    //if(!is_single())
//    if (!is_singular())
//        $classes[] = 'processed__menu-sticky';
//
//    if (is_singular())
//        $classes[] = 'page-node page-node- page-node-2560 node-type-news';
//
//    //if(is_single())
//    if (is_singular("property"))
//        $classes[] = 'node-type-property';
//
//    /*remove in single page*/
//    //$classes[] = 'processed__menu-sticky';
//
//
//    $classes[] = 'processed__menu';
//    $classes[] = 'processed__slider';
//    $classes[] = 'processed__datalayer';

//html front not-logged-in one-sidebar sidebar-first page-node page-node- page-node-7 node-type-homepage i18n-fr


    return $classes;
}




