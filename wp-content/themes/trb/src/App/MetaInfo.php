<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 8/2/2017
 * Time: 11:21 AM
 */
 namespace App;

 class MetaInfo {

     protected $title;
     protected $image;
     protected $link;
     protected $description;

     protected $defTitle;
     protected $defImage;
     protected $defLink;
     protected $defDescription;

     public function __construct($title='',$image='',$link='',$description='')
     {
        $this->defTitle = $title;
        $this->defImage = $image;
        $this->defLink = $link;
         $this->defDescription = $description;
     }

     /*
      * //  <meta name="twitter:site" content="@nytimes">
  // <meta name="twitter:creator" content="@SarahMaslinNir">
      */
     public function getMetaInfo()
     {
         $tinyUrl = \APP\get_tiny_url($this->getLink());
         $str = <<<EOF
        <meta property="og:title" content="{$this->getTitle()}"/>
        <meta property="og:image" content="{$this->getImage()}"/>
        <meta property="og:description" content="{$this->getDescription()}"/>
        <meta property="og:url" content="{$tinyUrl}" />
	
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:description" content="{$this->getDescription()}"/>
        <meta name="twitter:title" content="{$this->getTitle()}"/>
        <meta name="twitter:image" content="{$this->getImage()}" />
EOF;
         //<meta property="fb:app_id" content="901306723330852" />
         return $str;
     }



     /**
      * @param mixed $title
      */
     public function setTitle($title)
     {
         if(!$this->title) $this->title = $title;
         return $this;
     }

      /**
      * @param mixed $description
      */
     public function setDescription($description)
     {
         if($description){
             $description = strip_tags($description);
             //$description = substr($description, 0, 110).'...';
             $this->description = trim($description);
         }

         return $this;
     }

     /**
      * @param mixed $image
      */
     public function setImage($image)
     {
         if(!$this->image) $this->image = $image;
         return $this;
     }
 /**
      * @param mixed $link
      */
     public function setLink($link)
     {
        if(!$this->link) $this->link = $link;
         return $this;
     }

     /**
      * @return mixed
      */
     public function getTitle()
     {
         return ($this->title) ? : $this->defTitle;

     }

     /**
      * @return mixed
      */
     public function getDescription()
     {
         return ($this->description) ? : $this->defDescription;
     }

     /**
      * @return mixed
      */
     public function getImage()
     {
         return ($this->image) ? : $this->defImage;
     }

      /**
      * @return mixed
      */
     public function getLink()
     {
         return ($this->link) ? : $this->defLink;
     }
 }