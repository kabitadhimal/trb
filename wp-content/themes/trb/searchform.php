<div class="text-container--xs">
<div class="block__b">
	<div class="form mt-4 mt-sm-3 px-0 px-md-12 px-lg-5">
			<form action="<?php echo home_url( '/' ); ?>" method="get" class="cms-form">
				<div class="form-group">
				    <label for="search">Search in <?php echo home_url( '/' ); ?></label>
				    <input type="text" name="s" id="search" class="form-control" value="<?php the_search_query(); ?>" />
				 
			</div>
		

			<div class="form-group mt-4 mt-sm-2 mbt-0">
				 <input type="submit" class="btn btn-primary" alt="Search" value="Search" /></div>
				 	</form>
	</div>
</div>
</div>