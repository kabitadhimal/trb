var staticMap = new function(){
    this._popup = document.querySelector('.js-static-popup');
    this.updateByDomId = function(markerDom){
        var yPos = markerDom.style.top;
        var xPos = markerDom.style.left;

        this._popup.style.top = yPos;
        this._popup.style.left = xPos;

        var contentId = markerDom.getAttribute('data-content-id');
        //alert(contentId);
        if(!contentId) {
            this.hide();
            return;
        }
        var content = document.getElementById(contentId);
       // console.log(content);

        markerDom.parentNode.insertBefore(this._popup, markerDom);

        this._popup.innerHTML = content.innerHTML;
        //console.log(content.innerHTML);

        this.show();
    };

    this.show = function(){
        // alert('hello');
        this._popup.classList.remove('d-none');
    };

    this.hide = function(){
        this._popup.classList.add('d-none');
    };

    this.hide();
};

document.addEventListener('click', function(e) {
    if (e.target.className.indexOf('js-marker') !== -1) {
        e.preventDefault();
        staticMap.updateByDomId(e.target);
    }

    if (e.target.className.indexOf('js-proxy-marker') !== -1) {
        e.preventDefault();
        var markerId = e.target.getAttribute('data-marker-id');
        if(markerId){
            var marker = document.getElementById(markerId);
            staticMap.updateByDomId(marker);
        }
    }

     if (e.target.className.indexOf('js-marker') === -1 && e.target.className.indexOf('js-proxy-marker') === -1) {
        staticMap.hide();
     }
});


// document.querySelectorAll('.js-hide-popup , .js-hide-popupx').forEach(function(ele){
//     ele.addEventListener('click', function(e){
//         staticMap.hide();
//     })
// });


Array.prototype.forEach.call(document.querySelectorAll('.js-hide-popup , .js-hide-popupx'), function(ele){
    ele.addEventListener('click', function(e){
        staticMap.hide();
    })
}); 

