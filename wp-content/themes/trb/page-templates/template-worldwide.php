<?php
/*
Template Name: Worldwide Page
*/

?>

<main id="main" class="main" role="main">
                <div class="content">
                    <div class="container position-static">
                        <!-- Reusable promotion block :: #type_1 -->



                        <?php

                        if (have_rows('worldwide_content')):
                            while (have_rows('worldwide_content')) : the_row();
                                $layout = get_row_layout();
                                $tpl = get_stylesheet_directory() . '/partials/flexible-contents/worldwide/' . $layout . '.php';
                                if (file_exists($tpl)) {
                                    include $tpl;
                                }

                            endwhile;
                        endif;
                        ?>

                    </div><!-- /.Site container ends -->
                </div><!-- /.Site content ends -->
            </main><!-- /.Site main content wrapper ends -->