<?php
/*
Template Name: Newsroom Page
*/

$image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
//$img = \App\getImageManager()->resize(\App\getImageDirectoryPath($image_url), \App\IMAGE_SIZE_TEMPLATE_PAGE_BANNER_IMAGE);
$posts_per_page = '6';
?>

<main id="main" class="main" role="main">
    <div class="content">
        <div class="container position-static">

            <!-- Reusable promotion block :: #type_1 -->
            <section class="promo promo--bkg-sm text-right gap-p-sm has-cover is-extended wow fadeInDown"
                     data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20"
                     style="background-image:url(<?php echo $image_url ?>);">
                <div class="is-floatedx center">
                    <div class="row justify-content-end align-items-center">
                        <div class="col-sm-7">
                            <div class="card text-white wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s"
                                 data-wow-offset="20">
                                <div class="fw-r mb-0"><h1><?php the_title(); ?></h1></div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.Prom content ends -->
            </section><!-- /.Reusable promotion block ends -->


            <section class="l-breadcrumb">
                <nav class="is-extended wow fadeInDown" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo site_url() ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Library</li>
                    </ol>
                </nav>
            </section>


            <section class="block block--filter gap-p-sm bg-info is-extended pb-0 clearfix wow fadeInDown"
                     data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                <h5>Filtrer par</h5>

                <?php

                $args = array(
                    'child_of' => 0,
                    'parent' => '',
                    'orderby' => 'name',
                    'order' => 'ASC',
                    'hide_empty' => 0,
                    'exclude' => 1,
                    'hierarchical' => 1,
                    'pad_counts' => false
                );

                $categories = get_categories($args);
                // App\debug($categories);

                ?>
                <div class="block--filter__menu">
                    <ul>
                        <li class="<?php echo isset($_GET['cat']) ? '' : 'current'; ?>"><a
                                href="<?php echo site_url() . '/newsroom' ?>">All</a></li>
                        <?php if (!empty($categories)):
                            foreach ($categories as $category):?>
                                <li class="<?php echo(isset($_GET['cat']) && $_GET['cat'] != '' && $_GET['cat'] == $category->slug ? 'current' : '') ?>">
                                    <a
                                        href="<?php echo site_url() . '/newsroom/?cat=' . $category->slug ?>"><?php echo $category->name ?></a>
                                </li>
                            <?php endforeach;
                        endif; ?>
                    </ul>
                </div>
            </section>

            <!-- Reusable simple text block -->
            <section class="block block--text gap-p-eq bg-info text-center is-extended wow fadeInDown l--filter"
                     data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                <div class="block__b">
                    <div class="row">

                        <?php
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $args = array(
                            'paged' => $paged,
                            'orderby' => 'DESC',
                            'posts_per_page' => $posts_per_page,
                            'post_status' => 'publish',

                            'suppress_filters' => 0
                        );
                        if (!empty($_GET['cat'])):
                            $args = ['category_name' => $_GET['cat']];
                        endif;
                        $query = new WP_Query($args);
                        $total = $query->found_posts;
                        $max_num_pages = $query->max_num_pages;

                        if ($query->have_posts()):
                            while ($query->have_posts()):$query->the_post();
                                $image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
                                $alt = App\image_alt_by_url($image_url);
                               // $img = \App\getImageManager()->resize(\App\getImageDirectoryPath($image_url), \App\IMAGE_SIZE_NEWS_LISTING_IMAGE);
                                ?>
                                <div class="col-sm-4 mb-3 mb-sm-0">
                                    <div class="card position-relative shadowed wow fadeInDown" data-wow-duration="1s"
                                         data-wow-delay="0.15s" data-wow-offset="20">
                                        <figure class="card__pic mb-0">
                                            <?php if (!empty($image_url)): ?>
                                                <img alt="<?php echo $alt?>"
                                                     class="img img-full img-fluid rounded upper"
                                                     src="<?php echo $image_url ?>"/>
                                            <?php endif ?>
                                        </figure>
                                        <!-- Add bg-white to make post body white -->
                                        <div class="card__b border-top small-line-height bg-white">
                                            <h3 class="text-primary text-uppercase"><?php the_title(); ?></h3>
                                            <a href="<?php echo get_the_permalink() ?>" class="btn btn-outline-dark">Learn
                                                More</a>

                                        </div>

                                        <a href="<?php echo get_the_permalink() ?>" class="full-link"></a>

                                    </div>
                                </div>

                            <?php endwhile; wp_reset_postdata();
                        else:?>
                            <div class="col-sm-12 mb-3 mb-sm-0">
                                <div class="card shadowed theme-primary wow fadeInDown" data-wow-duration="1s"
                                     data-wow-delay="0.15s" data-wow-offset="20">
                                    <p>Records not found.</p>
                                </div>
                            </div>

                        <?php endif; ?>


                    </div>
                </div><!-- /.Block body ends -->
            </section><!-- /.Reusable simple text block ends -->

            <section
                class="block l--filter__pager gap-p-eq text-center bg-info is-extended text-center pt-0 wow fadeInDown"
                data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">

                <nav aria-label="Page navigation example">
                    <?php echo App\createPaginationLink($posts_per_page, $paged, $total, $max_num_pages); ?>
                </nav>
            </section>

        </div><!-- /.Site container ends -->
    </div><!-- /.Site content ends -->
</main><!-- /.Site main content wrapper ends -->


