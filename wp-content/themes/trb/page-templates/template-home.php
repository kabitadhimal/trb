<?php
/*
Template Name: Homepage
*/
?>
<main id="main" class="main" role="main">
            <div class="content">

        <?php

        if (have_rows('homepage_contents')):
            while (have_rows('homepage_contents')) : the_row();
                $layout = get_row_layout();

                if ($layout == 'banner_section'|| $layout == 'slider_section' ) {


                    if ($layout == 'banner_section' || $layout == 'slider_section') {
                        $tpl = get_stylesheet_directory() . '/partials/flexible-contents/home/' . $layout . '.php';
                        if (file_exists($tpl)) {
                            include $tpl;
                        }
                        break;
                    }
                }

            endwhile;
        endif;
        ?>


        <div class="container position-static">
            <?php
            if (have_rows('homepage_contents')):
                while (have_rows('homepage_contents')) : the_row();
                    $layout = get_row_layout();
                    $tpl = get_stylesheet_directory() . '/partials/flexible-contents/home/' . $layout . '.php';
                    if (file_exists($tpl)) {
                        include $tpl;
                    }

                endwhile;
            endif;
            ?>
        </div><!-- /.Site container ends -->

    </div><!-- /.Site content ends -->
</main>
<?php

//    if (have_rows('home_contents')):
//        while (have_rows('home_contents')) : the_row();
//            $layout = get_row_layout();
//            //echo $layout;
//            $tpl = get_stylesheet_directory() . '/partials/flexible-contents/home/' . $layout . '.php';
//            if (file_exists($tpl)) {
//                include $tpl;
//            }
//        endwhile;
//    endif;
?>




