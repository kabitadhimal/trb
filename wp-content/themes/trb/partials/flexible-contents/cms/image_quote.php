<?php
$image = get_sub_field('image');
$quote = get_sub_field('quote');

$image_url  = App\getImageManager()->resize(App\getImageDirectoryPath($image['url']), \App\IMAGE_SIZE_CMS_QUOTE_IMAGE);
?>


<section class="block block--text gap-p-sm bg-primary text-white is-extended text-center wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="block__b">

        <div class="row align-items-center">
            <div class="col-sm-4 mb-4 mb-sm-0">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <figure class="card__pic zoom-effect-holder mb-0">
                        <img alt="<?php echo $image['alt']?>" class="img img-full fluid-img zoom-effect" src="<?php echo $image['url']?>" />
                    </figure>
                </div>
            </div>

            <div class="col-sm-8">
                <div class="card pl-sm-3 wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <blockquote class="text-justify mb-0" cite="">
                        <?php echo $quote?>
                    </blockquote>
                </div>
            </div>
        </div>

    </div><!-- /.Blcok body ends -->
</section><!-- /.Reusable simple text block ends -->