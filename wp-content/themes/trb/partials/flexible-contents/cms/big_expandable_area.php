<?php
$expandable_area = get_sub_field('expandable_area');

?>


<section class="block block-accordion bg-white block-accordion-lg is-extended wow slideInUp"
         data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="js-accordion js-accordion--lg" data-scroll-active="true">
        <?php if (!empty($expandable_area)):
            foreach ($expandable_area as $area): ?>
                <div class="js-accordion__panel">
                    <?php if (!empty($area['items'])):
                        foreach ($area['items'] as $item):
                            ?>


                            <div class="js-accordion__heading">
                                <?php if ($item['acf_fc_layout'] == 'accordion_title'): ?>
                                <h4 class="js-accordion__title text-center"><a href="" class="js-accordion__toggler"
                                                                               data-toggle-target="js-accordion__panel-collapse"><?php echo $item['title'] ?></a>
                                    <?php endif;
                                    ?>
                                </h4>
                            </div>
                            <div class="js-accordion__panel-collapse">
                                <div class="js-accordion__panel-inner">
                                    <?php if ($item['acf_fc_layout'] == 'description'):
                                        echo $item['text'];
                                    endif; ?>

                                    <?php if ($item['acf_fc_layout'] == 'blocks'):
                                        $select_column = $item['select_column'];

                                        $first_column_image = $item['1st_column_image'];
                                        $first_column_title = $item['1st_column_title'];
                                        $first_column_description = $item['1st_column_description'];


                                        $second_column_image = $item['2nd_column_image'];
                                        $second_column_title = $item['2nd_column_title'];
                                        $second_column_description = $item['2nd_column_description'];

                                        ?>
                                        <div class="block__b text-center">

                                            <!-- Block content -->
                                            <div class="row">

                                                <div
                                                    class="col-sm-<?php echo $select_column == '3' ? '4' : '6' ?> mb-3 mb-sm-0">
                                                    <div class="card wow fadeInDown" data-wow-duration="1s"
                                                         data-wow-delay="0.15s" data-wow-offset="20">
                                                        <?php
                                                        if (!empty($second_column_image['url'])):
                                                            if ($select_column == '3'):
                                                                $img = \App\getImageManager()->resize(\App\getImageDirectoryPath($first_column_image['url']), \App\IMAGE_SIZE_CMS_THREE_COL_IMAGE);
                                                            else :
                                                                $img = \App\getImageManager()->resize(\App\getImageDirectoryPath($first_column_image['url']), \App\IMAGE_SIZE_CMS_TWO_COL_IMAGE);
                                                            endif;
                                                            ?>
                                                            <figure class="card__pic mb-0"><img alt="<?php echo $first_column_image['alt']?>"
                                                                                                class="img img-full img-fluid rounded"
                                                                                                src="<?php echo $img ?>"/>
                                                            </figure>
                                                        <?php endif; ?>
                                                        <!-- Add bg-white to make post body white -->
                                                        <div class="card__b bg-white">
                                                            <?php if (!empty($first_column_title)): ?>
                                                                <h3 class="text-primary"> <?php echo $first_column_title ?></h3>
                                                            <?php endif; ?>
                                                            <?php echo $first_column_description ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-<?php echo $select_column == '3' ? '4' : '6' ?>">
                                                    <div class="card wow fadeInDown" data-wow-duration="1s"
                                                         data-wow-delay="0.15s" data-wow-offset="20">
                                                        <?php
                                                        if (!empty($second_column_image['url'])):
                                                            if ($select_column == '3'):
                                                                $img = \App\getImageManager()->resize(\App\getImageDirectoryPath($second_column_image['url']), \App\IMAGE_SIZE_CMS_THREE_COL_IMAGE);
                                                            else :
                                                                $img = \App\getImageManager()->resize(\App\getImageDirectoryPath($second_column_image['url']), \App\IMAGE_SIZE_CMS_TWO_COL_IMAGE);
                                                            endif;
                                                            ?>
                                                            <figure class="card__pic mb-0">
                                                                <img alt="<?php echo $second_column_image['alt']?>"
                                                                     class="img img-full img-fluid rounded"
                                                                     src="<?php echo $img ?>"/>
                                                            </figure>
                                                        <?php endif; ?>
                                                        <!-- Add bg-white to make post body white -->
                                                        <div class="card__b bg-white">
                                                            <?php if (!empty($second_column_title)): ?>
                                                                <h3 class="text-primary"> <?php echo $second_column_title ?></h3>
                                                            <?php endif; ?>
                                                            <?php echo $second_column_description ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php if ($select_column == '3'):
                                                    $third_column_image = $item['3rd_column_image'];
                                                    $third_column_title = $item['3rd_column_title'];
                                                    $third_column_description = $item['3rd_column_description'];

                                                    $img = \App\getImageManager()->resize(\App\getImageDirectoryPath($third_column_image['url']), \App\IMAGE_SIZE_CMS_THREE_COL_IMAGE);
                                                    ?>
                                                    <div class="col-sm-4">
                                                        <div class="card wow fadeInDown" data-wow-duration="1s"
                                                             data-wow-delay="0.15s" data-wow-offset="20">
                                                            <figure class="card__pic mb-0"><img alt="<?php echo $third_column_image['alt']?>"
                                                                                                class="img img-full img-fluid rounded"
                                                                                                src="<?php echo $img ?>"/>
                                                            </figure>
                                                            <!-- Add bg-white to make post body white -->
                                                            <div class="card__b bg-white">
                                                                <?php if (!empty($third_column_title)): ?>
                                                                    <h3 class="text-primary"><?php echo $third_column_title ?></h3>
                                                                <?php endif; ?>
                                                                <?php echo $third_column_description ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>

                                            <a href="" class="js-accordion__toggler up-arrow"
                                               data-toggle-target="js-accordion__panel-collapse"></a>
                                        </div><!-- ./Block grid -->
                                    <?php endif; ?>



                                </div>
                            </div>
                            <?php
                        endforeach;
                    endif; ?>
                </div>
                <?php
            endforeach;
        endif;
        //app\debug($expandable_area);
        ?>


    </div>
</section>



