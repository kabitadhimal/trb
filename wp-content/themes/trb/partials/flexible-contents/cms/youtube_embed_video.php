<?php

$video_url = get_sub_field('link');
$video_src = (App\check_video_type($video_url) === 'youtube') ? 'https://www.youtube.com/embed/'.App\extract_yt_video_id($video_url).'?rel=0&showinfo=0' : 'https://player.vimeo.com/video/'.App\extract_vimeo_video_id($video_url).'';



if (!empty($video_src)):?>

    <!-- Reusable simple text block -->
    <section class="block block--text gap-p-eq bg-faded is-extended text-center wow fadeInDown" data-wow-duration="1s"
             data-wow-delay="0.15s" data-wow-offset="20">
        <div class="body__b">
            <div class="video-holder">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item"
                            src="<?php echo $video_src?>" frameborder="0"
                            allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section><!-- /.Reusable simple text block ends -->

<?php endif; ?>


