<?php
$video_link = get_sub_field('video_url');
$video_title = get_sub_field('title');
$video_src = (App\check_video_type($video_link) === 'youtube') ? 'https://www.youtube.com/embed/'.App\extract_yt_video_id($video_link).'?rel=0&showinfo=0' : 'https://player.vimeo.com/video/'.App\extract_vimeo_video_id($video_url).'';

$background_image = get_sub_field('background_image');

//$img = \App\getImageManager()->resize(\App\getImageDirectoryPath($background_image['url']), \App\IMAGE_SIZE_CMS_VIDEO_IMAGE);

?>



<!-- Reusable simple text block -->
<section class="promo promo--text position-relative is-extended-full wow fadeInDown" data-wow-duration="1s"
         data-wow-delay="0.15s" data-wow-offset="20">
    <figure class="promo__pic mb-0"><img alt="<?php echo $background_image['alt']?>" class="img img-full img-fluid"
                                         src="<?php echo $background_image['url']?>"/>
    </figure>
    <div class="promo__c is-floated center zindex-2 text-center">
        <?php if($video_title):?>
            <div class="h2 text-white"><?php echo $video_title;?></div>
        <?php endif;?>
        <div class="lightbox">
            <a href="<?php echo $video_src ?>"
               class="lightbox__play js-video-play"><i class="icon icon-play-round mr-0"></i></a>
        </div>
    </div>
</section><!-- /.Reusable simple text block ends -->