<section class="block cms-banner is-extended-full wow fadeInDown gap-p-btm" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="swiper-container slider slider--hero slider--hero-style2 js-slider-hero">
        <div class="swiper-wrapper slider__wrapper">
            <?php while(have_rows('sliders')): the_row(); ?>
            <div class="swiper-slide slider__slide">
                <div class="slider__b">
                    <?php $image = get_sub_field('image');
                    $link = get_sub_field('link'); ?>
                    <figure class="slider__pic mb-0 text-center"><img alt="<?php echo strip_tags($image['title']); ?>" src="<?php echo $image['url']; ?>" class="promo__img" /></figure>
                    <div class="content">
                        <div class="banner-content">
                            <?php the_sub_field('text'); ?>
                            <?php if($link): ?>
                            <a href = "<?php echo $link['url']; ?>" class="btn btn-white"><?php echo $link['title']; ?></a>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
        </div>

        <!-- Slider controls -->
        <div class="swiper-button swiper-button-next"><i class="icon icon-right-chev mr-0"></i></div>
        <div class="swiper-button swiper-button-prev"><i class="icon icon-left-chev mr-0"></i></div>
    </div>
</section><!-- /.Home page hero slider ends -->