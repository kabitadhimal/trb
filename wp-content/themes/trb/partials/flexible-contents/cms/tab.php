<?php
$tabs = get_sub_field('tabs');
//App\debug($tabs);
?>

<section class="block block--tab gap-p-eq bg-white is-extended wow fadeInDown" data-wow-duration="1s"
         data-wow-delay="0.15s" data-wow-offset="20">
    <div class="block__b">

        <!-- Tab module -->
        <div class="tab">
            <!-- Tab nav -->
            <nav class="tab-nav hidden-xs-down" role="navigation">
                <!-- Tab nav menu -->
                <ul class="tab-list" role="list">
                    <?php if (!empty($tabs)):
                        foreach ($tabs as $ind => $tab):
                            ?>
                            <li class="tab-list__i <?php echo $ind == 0 ? 'active' : '' ?>">
                                <a href="" class="tab-list__l js-tab-toggler"
                                   data-tab-target="tab-pane<?php echo $ind ?>"><?php echo $tab['tab_title'] ?></a>
                            </li>
                        <?php endforeach;
                    endif; ?>

                </ul><!-- /.Tab nav menu -->
            </nav><!-- /.Tab nav -->

            <!-- Tab content -->
            <div class="tab__content">
                <div class="accordion js-accordion" data-scroll-active="true">
                    <?php if (!empty($tabs)):
                        foreach ($tabs as $ind => $tab):
                            ?>
                            <div class="tab__pane <?php echo $ind==0?'active-pane animate':''?> tab-pane<?php echo $ind ?>">
                                <div class="accordion-panel active">
                                    <div class="accordion-panel__h hidden-sm-up">
                                        <h4 class="accordion-panel__t mb-0"><a href=""
                                                                               class="accordion-panel__l js-accordion__toggler expanded"
                                                                               data-toggle-target="js-accordion-target-panel<?php echo $ind ?>"><?php echo $tab['tab_title'] ?></a>
                                        </h4>
                                    </div>
                                    <div class="accordion-panel__collapse js-accordion-target-panel<?php echo $ind ?>">
                                        <div class="accordion-panel__b">
                                            <div class="row">

                                                <div
                                                    class="col-sm-<?php echo $tab['tab_content'] == 1 ? '12' : '6' ?> mb-3 mb-sm-0">
                                                    <div class="card">
                                                        <?php echo $tab['first_column'] ?>
                                                    </div>
                                                </div>

                                                <?php if ($tab['tab_content'] == 2): ?>
                                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                                        <div class="card">
                                                            <?php echo $tab['second_column'] ?>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;
                    endif; ?>

                </div>
            </div><!-- /.Tab content -->
        </div><!-- /.Tab module ends -->

    </div><!-- /.Blcok body ends -->
</section><!-- /.Reusable tab text block ends -->