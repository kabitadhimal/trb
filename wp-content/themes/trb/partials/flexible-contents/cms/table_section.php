<?php $table = get_sub_field('table');
//app\debug($table);

?>

<section class="block block-cms-table is-extended wow fadeIn gap-p-eq" data-wow-duration="1s" data-wow-delay="0.15s"
         data-wow-offset="20">
    <div class="cms-table-wrap wow slideInUp" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
        <table class="cms-table cms-table--striped cms-table-responsive">
            <?php if (!empty($table['header'])): ?>
                <thead>
                <tr>
                    <?php foreach ($table['header'] as $header): ?>
                        <th><h4><?php echo $header['c'] ?></h4></th>
                    <?php endforeach; ?>

                </tr>
                </thead>
            <?php endif; ?>
            <?php if (!empty($table['body'])): ?>
                <tbody>
                <?php foreach ($table['body'] as $row): ?>
                    <tr>
                        <?php if (!empty($row) && is_array($row)):
                            foreach ($row as $ind=>$col):?>
                                <td data-th-title="<?php echo $table['header'][$ind]['c']?>"><?php echo $col['c']?></td>
                            <?php endforeach;
                        endif; ?>
                    </tr>
                <?php endforeach;
                ?>

                </tbody>
            <?php endif; ?>
        </table>
    </div>
</section>