<?php

$background_color = get_sub_field('background_color');
$title = get_sub_field('title');
$description = get_sub_field('description');
$button_background_color = get_sub_field('button_background_color');
$button_text = get_sub_field('button_text');
$button_link = get_sub_field('button_link');


?>
<!-- Reusable simple text block -->
<section class="block block--text gap-p-eq bg-<?php echo $background_color?> <?php echo $background_color=='primary'?'text-white':''?> is-extended text-center wow fadeInDown" data-wow-duration="1s"
         data-wow-delay="0.15s" data-wow-offset="20">
    <div class="text-container--sm">
        <header class="block__h">
            <?php if (!empty($title)): ?>
                <h2><?php echo $title ?></h2>
            <?php endif; ?>
        </header>
        <div class="block__b">
            <?php echo $description ?>
            <div class="btn-group mt-3">
                <?php $color = $button_background_color == 'blue' ? 'primary' : 'dark'; ?>
                <?php if (!empty($button_text)): ?>
                    <a href="<?php echo $button_link?>" class="btn btn-<?php echo $color ?> ls-1 text-uppercase"><?php echo $button_text?></a>
                <?php endif; ?>

            </div>
        </div><!-- /.Block body ends -->
    </div>
</section><!-- /.Reusable simple text block ends -->