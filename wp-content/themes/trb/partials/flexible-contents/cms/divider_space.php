<?php
/**
 * Created by PhpStorm.
 * User: Dinesh
 * Date: 6/19/2018
 * Time: 4:04 PM
 */
$divider_type = get_sub_field('divider_type');
$type='';
if($divider_type=='small'){
    $type ='sm';
}elseif($divider_type=='medium'){
    $type ='md';
}elseif($divider_type=='large'){
    $type ='xl';
}elseif($divider_type=='extrasmall '){
    $type ='xs';
}
?>
<div class="divider-<?php echo $type?>"></div>


