<?php
$image = get_sub_field('image');
$img = '';

if (!empty($image)) {
    if ($image['mime_type'] == 'image/svg+xml') {
        $img = $image['url'];
    } else {
       // $img = \App\getImageManager()->resize(\App\getImageDirectoryPath($image['url']), \App\IMAGE_SIZE_TEMPLATE_PAGE_BANNER_IMAGE);
        $img = $image['url'];
    }

}

$title = get_sub_field('title');
?>
<section class="promo promo--bkg-sm text-right gap-p-sm has-cover is-extended wow fadeInDown"
         data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20"
         style="background-image:url(<?php echo $img ?>);">
    <div class="is-floated center">
        <div class="row justify-content-end align-items-center">
            <div class="col-sm-7">
                <div class="card text-white wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s"
                     data-wow-offset="20">
                    <div class="fw-r mb-0"><h1><?php echo $title ?></h1></div>
                </div>
            </div>
        </div>
    </div><!-- /.Prom content ends -->
</section><!-- /.Reusable promotion block ends -->