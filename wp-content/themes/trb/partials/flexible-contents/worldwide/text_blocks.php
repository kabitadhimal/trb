<?php

$left_block = get_sub_field('left_block');
$right_block = get_sub_field('right_block');



?>
<section class="ss block block--text gap-p-eq bg-faded is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <!-- <header class="block__h text-center mb-4 mb-lg-5">
        <h2 class="mb-0">Mise en page 2 colonnes</h2>
        <p>Lorem ipsum dolor sit amet it consectetur dolor sit amet oirutz eiutodh ciid</p>
    </header> -->
    <div class="block__b">
        <div class="row">
            <div class="col-sm-6 flex-sm-first">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <div class="card__b pl-lg-0">
                        <h2 class="text-uppercase"><?php echo $left_block['title']?></h2>
                        <?php echo $left_block['content']?>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 flex-sm-first bg-primary border-radius-5 text-white">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <div class="card__b">
                        <h2 class=" text-white text-uppercase"><?php echo $right_block['title']?></h2>
                        <?php echo $right_block['content']?>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.Block body ends -->
</section><!-- /.Reusable simple text block ends -->