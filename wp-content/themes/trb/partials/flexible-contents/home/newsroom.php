<?php
$title = get_sub_field('title');
$text = get_sub_field('text');

$select_news = get_sub_field('select_news');
//App\debug($select_news);

?>


<section class="block block--text gap-p-eq text-center bg-faded is-extended wow fadeInDown newsroom-block" data-wow-duration="1s newsroom-block"
         data-wow-delay="0.15s" data-wow-offset="20">

    <header class="block__h text-center mb-4 mb-lg-5">
        <h2 class="mb-2"><?php echo $title ?></h2>
        <p><?php echo $text ?></p>
    </header>


    <!-- Swiper -->
    <div class="swiper-container swiper-three-js-xs js-swiper-type-pager">
        <div class="swiper-wrapper">

            <?php
            $block = get_sub_field('last_block');
            $image_url = $block['image']['url'];
            $alt = $block['image']['alt'];
            $blockTitle = $block['title'];
            $linkUrl = $block['link']['url'];
            $linkTitle = $block['link']['title'];
            $target = !empty($block['link']['target'])?'target="_blank"':'';
            if(!empty($block)):
            ?>
            <div class="swiper-slide">
                <div class="card shadowed wow fadeInDown position-relative" data-wow-duration="1s"
                     data-wow-delay="0.15s" data-wow-offset="20">
                    <figure class="card__pic mb-0">
                        <img alt="<?php echo $alt?>" class="img img-full img-fluid rounded upper"
                             src="<?php echo $image_url?>"/></figure>
                    <div class="card__b border-top small-line-height bg-white">
                        <?php if(!empty($blockTitle)): ?>  <h3 class="text-primary text-uppercase"><?php echo $blockTitle;?></h3><?php endif; ?>

                        <?php if(!empty($linkUrl)):?> <a href="<?=$linkUrl?>" class="btn btn-primary"><?=$linkTitle?></a> <?php endif; ?>

                    </div>
                    <a href="<?=$linkUrl?>" <?=$target?> class="full-link"></a>
                </div>
            </div>
            <?php endif; ?>
            <?php if (!empty($select_news)):
                //$color = ['primary-2','green','grey'];
                //theme-<?php echo $color[$ind]
                foreach ($select_news as $ind=>$news):

                    ?>
                    <div class="swiper-slide">

                        <div class="card shadowed wow fadeInDown position-relative" data-wow-duration="1s"
                             data-wow-delay="0.15s" data-wow-offset="20">
                            <figure class="card__pic mb-0">
                                <?php  $image_url = wp_get_attachment_url(get_post_thumbnail_id($news->ID));
                                $alt = App\image_alt_by_url($image_url);
                                // $img = \App\getImageManager()->resize(\App\getImageDirectoryPath($image_url), \App\IMAGE_SIZE_HOME_NEWS_LISTING_IMAGE);
                                ?>
                                <img alt="<?php echo $alt?>" class="img img-full img-fluid rounded upper"
                                     src="<?php echo $image_url?>"/></figure>
                            <!-- Add bg-white to make post body white -->
                            <div class="card__b border-top small-line-height bg-white">
                                <h3 class="text-primary text-uppercase"><?php echo $news->post_title;?></h3>

                                <?php /*  <p><?php   echo wp_trim_words($news->post_content, 12, '');?></p>*/ ?>

                                <a href="<?php  echo get_permalink($news->ID); ?>" class="btn btn-primary">Learn More</a>

                            </div>
                            <a href="<?php  echo get_permalink($news->ID); ?>" class="full-link"></a>
                        </div>

                    </div>
                <?php endforeach;
            endif; ?>

        </div>
        <!-- Add Arrows -->
        <div class="swiper-arrow hidden-sm-up">
            <div class="swiper-button-prev icon icon-right-chev"></div>
            <div class="swiper-item-number js-swiper-pager"></div>
            <div class="swiper-button-next icon icon-right-chev"></div>
        </div>
    </div>

</section><!-- /.Reusable simple text block ends -->