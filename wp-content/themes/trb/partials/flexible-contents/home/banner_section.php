<?php
$title = get_sub_field('title');
$background_image = get_sub_field('banner_image');



//$img = \App\getImageManager()->resize(\App\getImageDirectoryPath($background_image['url']), \App\IMAGE_SIZE_HOME_BANNER_IMAGE);
?>

<script>
    $(document).ready(function(){
        var userAgent, ieReg, ie;
        userAgent = window.navigator.userAgent;
        ieReg = /msie|Trident.*rv[ :]*11\./gi;
        ie = ieReg.test(userAgent);

        if(ie) {
            $(".home-banner .slider__pic").each(function () {
                var $container = $(this),
                    imgUrl = $container.find("img").prop("src");
                if (imgUrl) {
                    $container.css("backgroundImage", 'url(' + imgUrl + ')').addClass("custom-object-fit");
                }
            });
        }
    });

</script>

<section
    class="block block--hero home-banner promo--bkg text-center position-relative is-extended-full">
    <div class="slider__b">
        <figure class="slider__pic mb-0 text-center">
            <img alt="<?php echo $background_image['alt']?>" src="<?php echo $background_image['url'];?>"/>
        </figure>

        <div class="content wow fadeInDown" data-wow-duration="1s"
             data-wow-delay="0.4s" data-wow-offset="20">
            <div class="card text-white">
                <div class="h1 fw-r mb-0 promo__c is-floated center"><?php echo $title;?></div>
            </div>
        </div><!-- /.Prom content ends -->
    </div>
</section><!-- /.Home page hero slider ends -->
