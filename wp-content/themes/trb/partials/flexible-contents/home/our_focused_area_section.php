<?php
$background_image  = get_sub_field('background_image');
$title  = get_sub_field('title');
$sub_title  = get_sub_field('sub_title');


$add_buttons  = get_sub_field('add_buttons');

//$img = \App\getImageManager()->resize(\App\getImageDirectoryPath($background_image['url']), \App\IMAGE_SIZE_CMS_BACKGROUND_IMAGE);

?>

<section class="block focus-banner promo--bkg small-banner-height text-center position-relative is-extended-full" style="background-image: url(<?php echo $background_image['url']?>) ">
    <div class="slider__b wow fadeIn" data-wow-duration="1s"
         data-wow-delay="0.25s" data-wow-offset="20">
        <div class="card text-white promo__c is-floated center">
            <h2 class="text-white"><?php echo $title?></h2>
            <div class="text-container--sm">
                <?php echo $sub_title ?>
            </div>
            <div class="btn-group mt-3">
                <?php if(!empty($add_buttons)):
                    foreach($add_buttons as $button):?>
                <a href="<?php echo $button['button_link']?>" class="btn btn-white"><?php echo $button['button_text']?></a>
            <?php endforeach;
        endif; ?>
               
            </div>
        </div>
    </div>
</section>
