<?php
$title = get_sub_field('title');
$text = get_sub_field('text');
$block = get_sub_field('block_content');
$image_url = $block['image']['url'];
$alt = $block['image']['alt'];
$blockTitle = $block['title'];
$linkUrl = $block['link']['url'];
$linkTitle = $block['link']['title'];
$target = !empty($block['link']['target'])?'target="_blank"':'';
?>
<section class="block block--text gap-p-eq text-center bg-faded is-extended wow fadeInDown newsroom-block" data-wow-duration="1s newsroom-block"
         data-wow-delay="0.15s" data-wow-offset="20">
    <?php if(!empty($title) || !empty($text)):?>
    <header class="block__h text-center mb-4 mb-lg-5">
        <h2 class="mb-2"><?php echo $title ?></h2>
        <p><?php echo $text ?></p>
    </header>
    <?php endif; ?>
    <!-- Swiper -->
    <div dir="rtl" class="row">
        <div class="col-sm-4">
            <?php if (!empty($block)): ?>

                        <div class="card shadowed position-relative">
                            <figure class="card__pic mb-0">
                                <img alt="<?php echo $alt?>" class="img img-full img-fluid rounded upper"
                                     src="<?php echo $image_url?>"/>
                            </figure>
                            <div class="card__b border-top small-line-height">
                                <div class="bg-white">
                                <?php if(!empty($blockTitle)): ?>  <h3 class="text-primary text-uppercase"><?php echo $blockTitle;?></h3><?php endif; ?>

                                <?php if(!empty($linkUrl)):?> <a href="<?=$linkUrl?>" class="btn btn-primary"><?=$linkTitle?></a> <?php endif; ?>
                                </div>

                            </div>
                            <a href="<?=$linkUrl?>" <?=$target?> class="full-link"></a>
                        </div>
                <?php
            endif; ?>
        </div>
    </div>

</section><!-- /.Reusable simple text block ends -->