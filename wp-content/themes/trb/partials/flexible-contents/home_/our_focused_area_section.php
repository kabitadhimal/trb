<?php
$background_image  = get_sub_field('background_image');
$title  = get_sub_field('title');
$sub_title  = get_sub_field('sub_title');

$left_button_text  = get_sub_field('left_button_text');
$left_button_link  = get_sub_field('left_button_link');

$middle_button_text  = get_sub_field('middle_button_text');
$middle_button_link  = get_sub_field('middle_button_link');


$right_button_text  = get_sub_field('right_button_text');
$right_button_link  = get_sub_field('right_button_link');
$img = \App\getImageManager()->resize(\App\getImageDirectoryPath($background_image['url']), \App\IMAGE_SIZE_CMS_BACKGROUND_IMAGE);

?>

<section class="block focus-banner promo--bkg small-banner-height text-center position-relative is-extended-full" style="background-image: url(<?php echo $img?>) ">
    <div class="slider__b wow fadeIn" data-wow-duration="1s"
         data-wow-delay="0.25s" data-wow-offset="20">
        <div class="card text-white promo__c is-floated center">
            <h2 class="text-white"><?php echo $title?></h2>
            <div class="text-container--sm">
                <?php echo $sub_title ?>
            </div>
            <div class="btn-group mt-3">
                <a href="<?php echo $left_button_link?>" class="btn btn-white"><?php echo $left_button_text?></a>
                <a href="<?php echo $middle_button_link?>" class="btn btn-white"><?php echo $middle_button_text?></a>
                <a href="<?php echo $right_button_link?>" class="btn btn-white"><?php echo $right_button_text?></a>
            </div>
        </div>
    </div>
</section>
