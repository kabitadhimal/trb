<?php
$blocks = get_sub_field('blocks');
//App\debug($blocks);

?>

<!-- Reusable simple text block for mobile -->
<section class="block block--text gap-p-eq text-center bg-faded is-extended wow fadeInDown" data-wow-duration="1s"
         data-wow-delay="0.15s" data-wow-offset="20">
    <!-- Swiper -->
    <div class="swiper-container swiper-three-js-xs js-swiper-type-pager">
        <div class="swiper-wrapper">

            <?php if (!empty($blocks)):
                foreach ($blocks as $block):?>
                    <div class="swiper-slide">
                        <div class="card shadowed theme-<?php echo $block['block_color']?> wow fadeInDown position-relative" data-wow-duration="1s"
                             data-wow-delay="0.15s" data-wow-offset="20">
                            <figure class="card__pic mb-0">
                                <?php
                                $img = \App\getImageManager()->resize(\App\getImageDirectoryPath($block['image']['url']), \App\IMAGE_SIZE_HOME_NEWS_LISTING_IMAGE);
                                if (!empty($img)):
                                ?>
                                <img alt="Post Thumbnail"
                                     class="img img-full img-fluid rounded upper"
                                     src="<?php echo $img ?>"/></figure>
                            <?php endif;
                            ?>
                            <!-- Add bg-white to make post body white -->
                            <div class="card__b border-top small-line-height bg-white">
                                <h3 class="text-uppercase"><?php echo $block['title'] ?></h3>
                                <p></p>
                                <p></p>
                                <a href="<?php echo $block['link'] ?>" class="btn btn-outline-dark">Learn More</a>

                            </div>

                            <a href="<?php echo $block['link'] ?>" class="full-link"></a>
                        </div>
                    </div>
                <?php endforeach;
            endif; ?>

        </div>
        <!-- Add Arrows -->
        <div class="swiper-arrow hidden-sm-up">
            <div class="swiper-button-prev icon icon-right-chev"></div>
            <div class="swiper-item-number js-swiper-pager"></div>
            <div class="swiper-button-next icon icon-right-chev"></div>
        </div>
        <!-- <div class=" hidden-xs-down swiper-pagination swiper-pagination-round"></div> -->
    </div>
</section><!-- /.Reusable simple text block ends -->

