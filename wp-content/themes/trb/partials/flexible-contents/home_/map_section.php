<?php
$title = get_sub_field('title');
$text_block = get_sub_field('text_block');
$button_text = get_sub_field('button_text');
$button_url = get_sub_field('button_url');

$map_image = get_sub_field('map_image');
$img = \App\getImageManager()->resize(\App\getImageDirectoryPath($map_image['url']), \App\IMAGE_SIZE_HOME_MAP_IMAGE);
?>

<section
    class="block block--text home-intro gap-p-eq bg-white is-extended wow fadeInDown text-right block--home-map position-relative"
    data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20"
    style="background:url(<?php echo $img?>) center  no-repeat; ">

    <div class="block__b">
        <div class="row align-items-center">

            <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s"
                 data-wow-offset="20">
                <div class="card__b">
                    <h3 class="text-primary text-uppercase"><?php echo $title?></h3>
                    <?php echo $text_block?>
                    <a href="<?php echo $button_url?>" class="btn btn-primary"><?php echo $button_text?></a>
                </div>
            </div>

        </div>
    </div>

    <a href="<?php echo $button_url?>" class="map-link"></a>
</section>
