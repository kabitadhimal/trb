<?php
$title = get_sub_field('title');
$background_image = get_sub_field('background_image');
$agencies = get_sub_field('agencies');
//App\debug($agencies);
$img = \App\getImageManager()->resize(\App\getImageDirectoryPath($background_image['url']), \App\IMAGE_SIZE_AGENCY_BACKGROUND_lARGE);
?>
<div class="l-agency"
     style="background: url('<?php echo $img ?>');background-size: cover;">
    <p class="title"><?php echo $title ?></p>
    <div class="wrapper">
        <div class="c-agency">
            <?php if (!empty($agencies)):
                foreach ($agencies as $agency):?>
                    <a href='<?php echo $agency['link']?>' class="col-3">
                        <div class="c-agency__header">
                            <img src="<?php echo $agency['image']['url']?>"
                                 width="531"
                                 height="115" alt="M3|Advisor" class="first">
                            <img src="<?php echo $agency['hover_image']['url']?>"
                                 width="531"
                                 height="115" alt="M3|Advisor" class="second">
                        </div>
                        <div class="c-agency__body">
                            <p><?php echo $agency['text']?></p>
                        </div>
                    </a>
                <?php endforeach;
            endif; ?>

        </div>
    </div>
</div>