<?php
$title = get_sub_field('title');
$text_block = get_sub_field('text_block');


?>
<section class="block block--quote gap-p-eq bg-primary text-white is-extended text-center wow fadeInDown"
         data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="text-container--sm">
        <header class="block__h">
            <h2 class="mb-2 text-white"><?php echo $title ?></h2>
            <?php echo $text_block ?>
        </header>
    </div>
</section>