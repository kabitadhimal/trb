<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="x-ua-compatible" content="ie=edge"/>

    <!-- <link rel="shortcut icon" href="<?php //echo get_template_directory_uri()?>/favicon.ico" type="image/vnd.microsoft.icon"/> -->
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript">

        window.ajaxURL = "<?php echo site_url('/'); ?>wp-admin/admin-ajax.php";
        site_url = "<?php echo site_url(); ?>";
        var templateDirUri = "<?php echo get_template_directory_uri()?>";
        window.jQuery || document.write("<script src='//code.jquery.com/jquery-1.10.2.min.js'>\x3C/script>")

    </script>

    <?php wp_head();   ?>
<!--    <link rel="alternate" href="--><?php //echo site_url(); ?><!--" hreflang="en" />-->

    <style>
        .grecaptcha-badge { visibility: hidden;}
    </style>

</head>