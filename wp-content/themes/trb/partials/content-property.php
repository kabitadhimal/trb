<?php

$images = json_decode(stripslashes_deep(get_field("images")));
$sender_id = get_field("sender_id");

if ($sender_id == "m3 REAL ESTATE")
    $sender_id = "app-m3";

$term_list = wp_get_post_terms(get_the_ID(), 'property_type', array("fields" => "all"));
$propertyType = $term_list[0]->slug;

//m3 REAL ESTATE
//realforce
//portia

?>

<style type="text/css" media="all">
    .background-image__7 {
        background-image: url("<?php echo site_url(); ?>/files/<?php echo $sender_id; ?>/property/<?php echo $propertyType; ?>/s_detail_header/<?php echo $images->picture_1_filename; ?>");
    }

</style>

<main id="main__content" class="main__content page" role="main">

    <a id="main-content"></a>
    <div class="tabs">
    </div>
    <div class="region region-content">
        <?php include get_stylesheet_directory() . '/partials/property-search.php' ?>
        <div id="block-system-main" class="block block-system">
            <div class="content">

                <!--popup if sold/rented -->
                <!--Découvrez d'autres biens correspondant à votre recherche-->

                <section id="content" class="content">
                    <article class="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">
                        <div class="wrapper">
                            <div class="product__fixed">
                                <div class="product__block">
                                    <div class="product__title">
                                        <p>
                                            <?php echo \APP\property_detail_location_title(get_field("object_zip"), get_field("object_city"), get_field("object_street"), get_field('object_state'), get_field('object_country')); ?>
                                        </p>
                                        <h1 class="h1">
                                            <span class="element-invisible"><?php _e("Rent"); ?> - </span>
                                            <?php //echo \APP\property_detail_title(get_field("object_category"), get_field("number_of_rooms"), get_field("floor"));
                                            the_title();?>
                                        </h1>
                                    </div>
                                    <div class="product__header-image">
                                        <img src="<?php echo site_url(); ?>/files/<?php echo $sender_id; ?>/property/<?php echo $propertyType; ?>/s_detail_header/<?php echo $images->picture_1_filename; ?>" alt="">
                                    </div>
                                    <div class="product__price">
                                        <div class="row">
                                            <!--                            <div class="product__content col-12" style="border-right: none">-->
                                            <div class="product__content col-8">

                                                <?php ?>
                                                <p class="price">
                                                    <?php

                                                    if ($propertyType == 'location')
                                                        $price = get_field('rent_net');
                                                    elseif ($propertyType == 'vente')
                                                        $price = get_field('selling_price');

                                                    ?>
                                                    <?php echo \APP\price_format(get_field("currency"), $price, get_field('price_unit')); ?>
                                                </p>

                                                <div class="product__reference">
                                                    <p>Ref. <strong><?php echo get_field("ref"); ?></strong></p>
                                                </div>
                                                <?php if ($propertyType == 'location' && get_field("rent_extra")): ?>
                                                    <p class="charges">(+ <?php echo get_field("currency"); ?> <?php echo get_field("rent_extra") + 0; ?>.- CHARGES)</p>
                                                <?php endif; ?>

                                            </div>
                                            <div class="product__fav col-4">
                                                <button class="button button__fav button__connection <?php echo $propertyType."Favs"; ?>" code="<?php echo get_field('ref'); ?>"></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product__contact hidden-xs">
                                        <p class="first"><?php _e("Contact","m3-theme"); ?></p>
                                        <?php $contact = json_decode(get_field("agent_details")); ?>
                                        <?php if (!empty($contact->visit_name)): ?><p class="icon"><i
                                                class="demo-icon icon-user"></i><?php echo $contact->visit_name; ?>
                                            </p><?php endif; ?>
                                        <?php if (!empty($contact->visit_phone)): ?><p class="icon"><i
                                                class="demo-icon icon-phone"></i><?php echo $contact->visit_phone; ?>
                                            </p><?php endif; ?>
                                        <?php if (!empty($contact->visit_remarks)): ?><p class="icon"><i
                                                class="demo-icon icon-phone"></i><?php echo $contact->visit_remarks; ?>
                                            </p><?php endif; ?>
                                        <nav class="application-form__links">
                                            <ul class="links">
                                                <?php
                                                $prop_type = get_field("object_category");

                                                $form_link = icl_get_home_url() . 'application-form/';
                                                $doc_file = "";
                                                if ($prop_type == "INDUS") {
                                                    $form_link .= "commercial";
                                                    $doc_file = 'commerce.pdf';
                                                } elseif ($prop_type == "HOUSE" || $prop_type == "APPT") {
                                                    $form_link .= "housing";
                                                    $doc_file = 'lodgement.pdf';
                                                } elseif ($prop_type == "PARK") {
                                                    $form_link .= "parkinggaragewarehouse";
                                                    $doc_file = 'parkinggaragewarehouse.pdf';
                                                } else {
                                                    $form_link = "";
                                                }

                                                ?>
                                                <?php if (!empty($form_link)): ?>
                                                    <li class="apply-form first"><a href="<?php echo $form_link; ?>/?ref=<?php echo get_field('ref'); ?>">
                                                            <?php _e("apply online","m3-theme"); ?></a></li>
                                                    <li class="apply-doc last"><a href="<?php echo site_url() . '/files/inscriptions/inscription-form-fillup.php?file=' . $doc_file . '&ref=' . get_field('ref'); ?>">
                                                            <?php _e("download form","m3-theme"); ?>
                                                           </a></li>
                                                <?php endif; ?>
                                            </ul>
                                        </nav>
                                        <button class="button button__contact js_contact_popup">
                                            <span><?php _e("Contact us","m3-theme"); ?></span>
                                        </button>
                                    </div>
                                    <div class="product__action hidden-xs">
                                        <div class="col-4">
                                            <a class="button button__print" href="javascript:window.print()"><i class="demo-icon icon-printer"></i></a>
                                        </div>
                                        <div class="col-4">
                                            <div class="m-dropdown button button__share">
                                                <ul class="m-dropdown__menu">
                                                    <li><i class="demo-icon icon-share"></i> </li>
                                                    <li>
                                                        <ul class="m-dropdown__submenu">
                                                            <li>
                                                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink(); ?>"
                                                                   title=""><i class="demo-icon icon-facebook2"></i></a>
                                                            </li>
                                                            <li>
                                                                <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo get_the_permalink(); ?>&amp;title=<?php echo get_the_title(); ?>"
                                                                   title=""><i class="demo-icon icon-linkedin2"></i></a>
                                                            </li>
                                                            <li>
                                                                <a href="https://plus.google.com/share?url=<?php echo get_the_permalink(); ?>"
                                                                   title=""><i class="demo-icon icon-googleplus2"></i></a>
                                                            </li>   
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <button class="button button__mail"><i class="demo-icon icon-mail"></i>
                                            </button>
                                            <div class="form__mail">
                                                <p><?php _e("Share this article","m3-theme"); ?></p>

                                                <?php

                                                if(ICL_LANGUAGE_CODE == "fr")
                                                    echo do_shortcode('[contact-form-7 id="6569" title="Property Detail Contact Form" html_class="form__mail--content"]');
                                                else if(ICL_LANGUAGE_CODE == "en")
                                                    echo do_shortcode('[contact-form-7 id="6768" title="Property Detail Contact Form EN" html_id="webform-client-form-25" html_class="webform-client-form webform-client-form-25 form__agency form__contact form"]');
                                                else if(ICL_LANGUAGE_CODE == "de")
                                                    echo do_shortcode('[contact-form-7 id="6769" title="Property Detail Contact Form DE" html_id="webform-client-form-25" html_class="webform-client-form webform-client-form-25 form__agency form__contact form"]');



                                                ?>
                                                <!--<form action="#" method="post"
                                                      class="form__mail--content">
                                                    <input required="required"
                                                           placeholder="Votre nom"
                                                           type="text"
                                                           id="" name="name" value="" size="60"
                                                           maxlength="128"
                                                           class="">
                                                    <input required="required"
                                                           placeholder="Adresse e-mail de votre ami"
                                                           type="text"
                                                           id=""
                                                           name="email" value="" size="60"
                                                           maxlength="128" class="">
                                                    <div class="form-actions">
                                                        <div class="button__submit">
                                                            <input class="button form-submit"
                                                                   type="submit" id=""
                                                                   name="sendMail"
                                                                   value="Envoyer">
                                                        </div>
                                                    </div>
                                                </form>-->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row product__information first">
                                <div class="bg-grey">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="row">
                                                <div class="col-3 first">
                                                    <p class="color-main">
                                                        <?php
                                                        if ($propertyType == "vente") {
                                                             _e("sale","m3-theme");
                                                        } else if ($propertyType == "location") {
                                                             _e("rent","m3-theme");
                                                        } ?>
                                                    </p>
                                                    <p>Ref. <?php echo get_field('ref_object') ?><?php echo get_field("ref_house"); ?><?php echo get_field("ref_property"); ?></p>
                                                </div>
                                                <?php if (get_field('object_category')): ?>
                                                    <div class="col-3">
                                                        <p><i class="demo-icon icon-house"></i></p>
                                                        <p><?php

                                                            $object_category = "";
                                                            if(get_field('object_category') == "APPT")
                                                                $object_category = __("Apartment", "m3-theme");
                                                            else if(get_field('object_category') == "HOUSE")
                                                                $object_category = __("House", "m3-theme");
                                                            else if(get_field('object_category') == "INDUS")
                                                                $object_category = __("Industrial Objects", "m3-theme");
                                                            else if(get_field('object_category') == "PARK")
                                                                $object_category = __("Parking/Warehouse", "m3-theme");
                                                            else if(get_field('object_category') == "OTHER")
                                                                $object_category = __("Other", "m3-theme");


                                                            echo $object_category;


                                                            ?></p>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if (get_field('number_of_rooms')): ?>
                                                    <div class="col-3">
                                                        <p><i class="demo-icon icon-room"></i></p>
                                                        <p> <?php echo get_field('number_of_rooms') ?> <?php _e("rooms","m3-theme"); ?></p>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if (get_field('surface_living')): ?>
                                                    <div class="col-3 last">
                                                        <p><i class="demo-icon icon-surface"></i></p>
                                                        <p><?php echo get_field("surface_living"); ?> m²</p>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-7">
                                    <div class="product__information border">
                                        <div class="product__header print-page-break">
                                            <h2 class="h2"><?php _e("Description","m3-theme"); ?></h2>
                                            <p><?php  echo html_entity_decode(get_the_content()); ?></p>

                                            <?php
                                                $features = json_decode(get_field("features"));

                                               // print_r($features);
                                                /*echo $features->prop_view;
                                                echo $features->prop_garage;
                                                echo get_field("new_building");
                                                echo $features->prop_fireplace;
                                                echo $features->prop_balcony;
                                                echo get_field("swimmingpool");
                                                echo $features->prop_elevator;
                                                echo get_field("wheelchair_accessible");
                                                echo get_field("minergie_certified");
                                                echo $features->prop_parking;
                                                echo get_field("animal_allowed");
                                                echo get_field("under_roof");*/

                                                ?>

                                        </div>

                                        <div class="product__body item-3">
                                            <ul>
                                                <?php if($features->prop_view == "Y"): ?>
                                                    <li class="icon"><i class="demo-icon icon-sun"></i> <?php _e("Nice view", "m3-theme"); ?></li>
                                                <?php endif;
                                                if($features->prop_garage == "Y"): ?>
                                                    <li class="icon"><i class="demo-icon icon-car"></i> <?php _e("Garage", "m3-theme"); ?></li>
                                                <?php endif;
                                                if(get_field("new_building") == "Y"): ?>
                                                    <li class="icon"><i class="demo-icon icon-star"></i> <?php _e("New building", "m3-theme"); ?></li>
                                                <?php endif;
                                                if($features->prop_fireplace == "Y"): ?>
                                                    <li class="icon"><i class="demo-icon icon-fire"></i> <?php _e("Fireplace", "m3-theme"); ?></li>
                                                <?php endif;
                                                if($features->prop_balcony == "Y"): ?>
                                                    <li class="icon"><i class="demo-icon icon-balcony"></i> <?php _e("Balcony", "m3-theme"); ?></li>
                                                <?php endif;
                                                if(get_field("swimmingpool") == "Y"): ?>
                                                    <li class="icon"><i class="demo-icon icon-swimmingpool"></i> <?php _e("Swiming pool", "m3-theme"); ?></li>
                                                <?php endif;
                                                if(get_field("prop_elevator") == "Y"): ?>
                                                    <li class="icon"><i class="demo-icon icon-elevator"></i> <?php _e("Elevator", "m3-theme"); ?></li>
                                                <?php endif;
                                                if(get_field("wheelchair_accessible") == "Y"): ?>
                                                    <li class="icon"><i class="demo-icon icon-handicap"></i> <?php _e("Handicap access", "m3-theme"); ?></li>
                                                <?php endif;
                                                if(get_field("minergie_certified") == "Y"): ?>
                                                    <li class="icon"><i class="demo-icon icon-leaf"></i> <?php _e("Minergie", "m3-theme"); ?></li>
                                                <?php endif;
                                                if($features->prop_parking == "Y"): ?>
                                                    <li class="icon"><i class="demo-icon icon-parking1"></i> <?php _e("Parking", "m3-theme"); ?></li>
                                                <?php endif;
                                                if(get_field("animal_allowed") == "Y"): ?>
                                                    <li class="icon"><i class="demo-icon icon-animal"></i> <?php _e("Animal allowed", "m3-theme"); ?></li>
                                                <?php endif;
                                                if(get_field("under_roof") == "Y"): ?>
                                                    <li class="icon"><i class="demo-icon icon-roof"></i> <?php _e("Unde roof", "m3-theme"); ?></li>
                                                <?php endif; ?>



                                            </ul>
                                        </div>
                                    </div>
                                    <?php $images = get_field('images');
                                    $images_arr = json_decode($images, true);
                                    ?>

                                    <div class="product__information border">
                                        <div class="slider__gallery slider__bien">
                                            <?php if (!empty($images_arr)): ?>
                                                <div class="slider__double first">
                                                    <?php foreach ($images_arr as $image):
                                                        if (!empty($image)):
                                                            $img1 = site_url() . '/files/' . $sender_id . '/property/'.$propertyType.'/s_detail_gallery_image/' . $image;
                                                            ?>
                                                            <div class="slider__item popup-gallery"><a href="<?php echo $img1; ?>">
                                                                    <picture>
                                                                        <img class="lazyloaded" src="<?php echo $img1 ?>" alt="" title="">
                                                                    </picture>
                                                                </a>
                                                            </div>
                                                        <?php endif;
                                                    endforeach; ?>
                                                </div>
                                            <?php

                                            endif;

                                            ?>
                                            <div class="slider__double second gallery">
                                                <?php foreach ($images_arr as $image):
                                                    if (!empty($image)):
                                                        $img2 = site_url() . '/files/' . $sender_id . '/property/'.$propertyType.'/s_detail_gallery_thumb/' . $image;

                                                        ?>
                                                        <div class="img">
                                                            <picture>
                                                                <img class="lazyloaded" src="<?php echo $img2 ?>" alt="" title="">
                                                            </picture>
                                                        </div>
                                                    <?php endif;
                                                endforeach; ?>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="product__information feature border print-page-break">
                                        <div class="row">
                                            <div class="col-5 product__feature">
                                                <h2 class="h2"><?php  _e("Characteristics","m3-theme"); ?></h2>
                                                <div class="product__body">
                                                    <?php if(get_field("floor")): ?>
                                                    <p class="text__feature">
                                                        <span class="first"><?php  _e("Floor of the property","m3-theme"); ?></span>
                                                        <span class="second"><?php echo get_field("floor"); ?>
                                                            <sup>e</sup></span>
                                                    </p>
                                                    <?php endif; ?>

                                                    <?php if(get_field("year_built")): ?>
                                                    <p class="text__feature">
                                                        <span class="first"><?php  _e("Year of construction","m3-theme"); ?></span>
                                                        <span class="second"><?php echo get_field("year_built"); ?></span>
                                                    </p>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div class="col-7 product__feature">
                                                <h2 class="h2"><?php _e("Visit information","m3-theme"); ?></h2>
                                                <div class="product__body">
                                                    <?php if(get_field("available_from") && get_field("sparefield_1")): ?>
                                                    <p class="text__feature"><span class="first"><?php  _e("Availability date","m3-theme"); ?></span>
                                                        <span class="second"><?php echo get_field("available_from"); ?><?php echo get_field("sparefield_1"); ?> </span>
                                                    </p>
                                                    <?php endif; ?>
                                                    <p class="text__feature">
                                                        <span class="first"><?php  _e("Contact","m3-theme"); ?></span>
                                                        <span class="second"><?php echo $contact->visit_name; ?><?php echo $contact->visit_remarks; ?></span>
                                                    </p>


                                                    <?php if(!empty($contact->visit_phone)) : ?>
                                                        <p class="text__feature">
                                                            <span class="first"><?php  _e("Phone number","m3-theme"); ?></span>
                                                            <span class="second"><?php echo $contact->visit_phone; ?></span>
                                                        </p>
                                                    <?php endif; ?>

                                                    <?php /*$agency_details = get_field("agency_details");
                                                    if(!empty($agency_details->agency_phone)) : */?><!--
                                                    <p class="text__feature">
                                                        <span class="first"><?php /* _e("Phone number","m3-theme"); */?></span>
                                                        <span class="second"><?php /*echo $agency_details->agency_phone; */?></span>
                                                    </p>
                                                     --><?php /*endif; */?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div id="map" class="map__property"></div>
                        <div id="map_printed" class="map__property map__printed"></div>

                        <?php  if(isset($_SESSION['similar_props']) && !empty($_SESSION['similar_props'])) : ?>
                        <div class="wrapper">
                            <div class="l-slider__offer">
                                <div class="wrapper">
                                    <div class="row">
                                        <div class="section__header">
                                            <div class="left">
                                                <h3 class="h2"><?php  _e("Headline","m3-theme"); ?></h3>
                                            </div>
                                            <div class="right">
                                                <?php
                                                $listing_page_slug = "biens-a-louer";
                                                if ($propertyType == "vente")
                                                    $listing_page_slug = "biens-a-vendre";

                                                ?>
                                                <a href="<?php echo icl_get_home_url(); ?>recherche/<?php echo $listing_page_slug; ?>" class="link"><?php  _e("All our properties","m3-theme"); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row offer__other">


                                    <?php


                                    if(isset($_SESSION['similar_props']) && !empty($_SESSION['similar_props'])) {

                                        $similar_properties =  \APP\center_current_element($_SESSION['similar_props'], get_the_ID(), 3);
                                        $similar_args = array(
                                            'post_type' => 'property',
                                            'posts_per_page' => '3',
                                            'post__in'            => $similar_properties,

                                        );
                                        $similar_props = new WP_Query($similar_args);

                                        // The Loop
                                        if ($similar_props->have_posts()) :
                                            while ($similar_props->have_posts()) :
                                                $similar_props->the_post();
                                                get_template_part( 'partials/content', 'property-block' );
                                            endwhile;
                                            wp_reset_postdata();
                                        endif;
                                    }

                                    ?>

                                </div>
                            </div>
                        </div>
                        <?php endif; ?>


                    </article>
                </section>
            </div>
        </div>
    </div>
</main>

<div class="pop-up pop-up__contact">
    <p class="close"></p>
    <div class="pop-up__content">
        <p class="close cross"><i class="demo-icon icon-cross"></i></p>
        <div class="wrap">
            <div class="col-5 bg-grey">
                <div class="pop-up__header">
                    <p class="title__pop-in"><i class="demo-icon icon-user"></i><?php  _e("Your contact","m3-theme"); ?></p>
                </div>
                <div class="pop-up__body">
                    <div class="block__information">
                        <p><i class="demo-icon icon-phone"></i><?php echo $contact->visit_phone; ?></p></div>
                    <div class="block__information-1">
                    </div>
                </div>
            </div>
            <div class="col-7">
                <div class="pop-up__header"></div>

                <?php
                if(ICL_LANGUAGE_CODE == "fr")
                    echo do_shortcode('[contact-form-7 id="6570" title="Detail Page Contact Popup" html_id="webform-client-form-25" html_class="webform-client-form webform-client-form-25 form__agency form__contact form"]');
                else if(ICL_LANGUAGE_CODE == "en")
                    echo do_shortcode('[contact-form-7 id="6762" title="Detail Page Contact En" html_id="webform-client-form-25" html_class="webform-client-form webform-client-form-25 form__agency form__contact form"]');
                else if(ICL_LANGUAGE_CODE == "de")
                    echo do_shortcode('[contact-form-7 id="6765" title="Detail Page Contact Popup De" html_id="webform-client-form-25" html_class="webform-client-form webform-client-form-25 form__agency form__contact form"]');

                ?>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {



        $('.js_contact_popup').on("click", function (e) {
            // e.preventDefault();
            var id = $(this).data('popup');
            $('.pop-up__contact').css('display', 'block');
            $('.pop-up__contact').css('opacity', '1');
            $('.pop-up__contact').css('z-index', '1050');
            $('body').addClass('pop-up-active');
            $('body').addClass('pop-up__contact');


            $('.pop-up__contact .close').on(
                'click', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    $('body').removeClass('pop-up-active pop-up__contact pop-up__connexion');
                    $('.pop-up__contact .close').off('click');
                    $('.pop-up__contact').hide();

                }
            );


        });


        function initMap(map_id) {
            var $map = $(map_id);

            $map.addClass('processed__map');

            lng = <?php echo (get_field("map_lng"))?get_field("map_lng"):"6.1432"; ?>;
            lat = <?php echo (get_field("map_lat"))?get_field("map_lat"):"46.2044"; ?>;



            var map = new google.maps.Map($map[0], {
                zoom: 16,
                center: {
                    lng: lng,
                    lat: lat
                },
                scrollwheel: false
            });

            var infowindow = new google.maps.InfoWindow({
                content: '<div class="ok">' + '<?php echo get_field('object_zip'); ?>' + ' '+'<?php echo get_field("object_city"); ?>'  + ' ' + '<?php echo get_field("object_state"); ?>' + ' ' + '<?php echo get_field('object_street'); ?>' + '</div>'
            });

            <?php if (get_field("map_lng") && get_field("map_lat")): ?>

                markerIcon = "<?php echo get_template_directory_uri(); ?>/assets/img/pin.png";
                var marker = new google.maps.Marker({
                    position: {
                        lng: lng,
                        lat: lat
                    },
                    icon: markerIcon,
                    map: map,
                    // icon: markerIcon
                });
                marker.addListener('click', function () {
                    infowindow.open(map, marker);
                });

            <?php endif; ?>
        }


            initMap('#map');
            initMap('#map_printed');

    })

    $(document).ready(function () {
        RossetStorage.updateFavClass("<?php echo $propertyType; ?>");
    })


   /* $(document).ready(function(){
        $(".form-radios-inline").html('  <label for="edit-submitted-contact-me-by"><?php _e("Contact me by","m3-theme"); ?></label>\n' +
            ' <div id="edit-submitted-contact-me-by" class="form-radios"><div class="form-item form-type-radio form-item-submitted-contact-me-by">\n' +
            ' <input type="radio" id="edit-submitted-contact-me-by-1" name="contact_me_by" value="tel" class="form-radio">  <label class="option" for="edit-submitted-contact-me-by-1">Tel </label>\n' +
            '\n' +
            '</div><div class="form-item form-type-radio form-item-submitted-contact-me-by">\n' +
            ' <input type="radio" id="edit-submitted-contact-me-by-2" name="contact_me_by" value="email" class="form-radio">  <label class="option" for="edit-submitted-contact-me-by-2"><?php _e("Email","m3-theme"); ?> </label>\n' +
            '\n' +
            '</div></div>');
    })*/


</script>