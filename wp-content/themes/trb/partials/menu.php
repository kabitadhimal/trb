<?php

/**
 * This function generates hierarchical menu tree.
 * @param type $location
 * @return type
 */
global $wp_query;
$post_id = $wp_query->post->ID;
$post_parent_id = wp_get_post_parent_id($post_id);


$menu = App\__procab_menu_hierarchy_creater('primary_menu');
//App\debug($menu);

//$post->ID == $m['object_id']
//if($_GET['test']==1){
//    App\debug($menu);die('test');
//}
//App\debug($menu);
?>

<nav class="navbar flex justify-content-end">
    <div class="navbar__header hidden-md-up">
        <button class="navbar__toggle js-navbar-toggle" data-target="js-flyout-menu">
            <span class="line-bar"></span>
            <span class="line-bar"></span>
            <span class="line-bar"></span>
            <span class="line-bar"></span>
        </button>
    </div>
    <div class="d-flex align-items-center justify-content-end">
        <div class="js-flyout-menu menu-holder">
            <ul class="menu d-flex justify-content-end" role="menu">
                <?php if (!empty($menu)):
                    foreach ($menu as $ind => $m):
                        ?>

                    <li class="menu__i <?php echo $m->object_id ?> <?php echo $post_parent_id ?> <?php echo ($post_id == $m->object_id) ? 'current-menu-item' : '' ?> <?php echo !empty($m->sub) ? 'has-submenu' : '' ?>">
                        <a href="<?php echo $m->url ?>"
                           class="js-menu-link <?php echo implode(' ', $m->classes); ?>"><?php echo $m->title ?></a>
                        <?php if (!empty($m->sub)): ?>
                        <div class="submenu-holder">
                            <div class="hidden-md-up menu__i-xs--current js-menu-i-xs-current"><i
                                    class="icon icon-left-chev"></i><?php echo $m->title ?>
                            </div>
                            <div class="container">
                                <div class="row justify-content-center">
                                    <?php foreach ($m->sub as $m1): ?>
                                        <div class="col-sm-4 mb-3 mb-sm-0">
                                            <div class="card">
                                                <figure class="card__pic mb-0">
                                                    <?php $image = get_field('image', $m1->ID);
                                                    $img = "";

                                                    if (!empty($image)) {
                                                        if ($image['mime_type'] == 'image/svg+xml') {
                                                            $img = $image['url'];
                                                        } else {
                                                            $img = \App\getImageManager()->resize(\App\getImageDirectoryPath($image['url']), \App\IMAGE_SIZE_MENU_IMAGE);
                                                            $img = $image['url'];
                                                        }

                                                    }
                                                    //$img = \App\getImageManager()->resize(\App\getImageDirectoryPath($image['url']), \App\IMAGE_SIZE_MENU_IMAGE);
                                                    if (!empty($img)): ?>
                                                        <a href="<?php echo $m1->url ?>">
                                                            <img alt="Post Thumbnail"
                                                                 class="img img-full img-fluid rounded upper"
                                                                 src="<?php echo $img ?>"/> </a>
                                                    <?php endif; ?>
                                                </figure>

                                                <div class="card__b border-primary small-line-height bg-white">
                                                    <h3 class="text-primary text-uppercase <?php echo ($post_id == $m1->object_id) ? 'js-current-menu-item' : '' ?> "><a
                                                            href="<?php echo $m1->url ?>"><?php echo $m1->title ?></a>
                                                    </h3>
                                                    <?php if (!empty($m1->sub)):
                                                        ?>
                                                        <ul class="submenu submenu-level1">
                                                            <?php foreach ($m1->sub as $m2): ?>
                                                                <li class="submenu__i"><a
                                                                        href="<?php echo $m2->url ?>"><?php echo $m2->title ?></a>
                                                                </li>
                                                            <?php endforeach; ?>

                                                        </ul>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>


                                </div>
                            </div>
                        </div>
                        </li>
                    <?php endif; ?>

                    <?php endforeach;
                endif; ?>


                <li class="menu__i has-submenu hidden-md-up">
                    <a href="" class="js-menu-link">Global website</a>


                    <div class="submenu-holder">
                        <div class="hidden-md-up menu__i-xs--current js-menu-i-xs-current"><i
                                class="icon icon-left-chev"></i>Global website
                        </div>

                        <?php $globalWebsites = get_field('global_websites', 'options');
                        if (!empty($globalWebsites)): ?>
                        <ul class="submenu submenu-level1">
                            <?php foreach ($globalWebsites as $website): ?>
                                <li class="submenu__i "><a href="#small-dialog" class="popup-with-zoom-anim"
                                                           data-link="<?php echo $website['url']['url'] ?>"><?php echo $website['url']['title'] ?></a>
                                </li>
                            <?php endforeach;
                            endif;
                            ?>
                        </ul>

                    </div>


                </li>

             <?php /*        <li class="menu__i has-submenu hidden-md-up">
                    <a href="" class="js-menu-link">Language</a>
                    <div class="submenu-holder">
                        <div class="hidden-md-up menu__i-xs--current js-menu-i-xs-current"><i
                                class="icon icon-left-chev"></i>Language
                        </div>

                        <ul class="submenu submenu-level1">
                            <li class="submenu__i"><a href="">fr</a></li>
                            <li class="submenu__i"><a href="">En</a></li>
                        </ul>
                    </div>
                </li> */ ?>


                <li class="menu__i hidden-md-up"><a href="http://www.doccheck.com/com/" target="_blank">Healthcare
                        Professional</a></li>

                <li class="menu__i hidden-md-up"><a href="<?php echo site_url().'/contact-us' ?>">Contact Us</a></li>

            </ul>
        </div>
        <div class="actions d-flex align-items-center">
            <ul class="actions-list d-flex actions__i">
                <li class="actions-list__i"><a href="" class="js-toggle-panel" data-target="collapsable-panel-group"><i
                            class="icon icon-md icon-search mr-0"></i></a></li>
            </ul>
        </div>
    </div>
</nav>
