var staticMap = new function(){
    this._popup = document.querySelector('.js-static-popup');
    console.log(this._popup + '  hi')
    this.updateByDomId = function(markerDom){
        var yPos = markerDom.style.top;
        var xPos = markerDom.style.left;

        this._popup.style.top = yPos;
        this._popup.style.left = xPos;

        var contentId = markerDom.getAttribute('data-content-id');
        if(!contentId) {
            this.hide();
            return;
        }
        var content = document.getElementById(contentId);
        this._popup.innerHTML = content.innerHTML;

        this.show();
    };

    this.show = function(){
        this._popup.classList.remove('d-none');
    };
    this.hide = function(){
        this._popup.classList.add('d-none');
    };

    this.hide();
};

document.querySelectorAll('.js-marker').forEach(function(ele){
    ele.addEventListener('click', function(e){
        e.preventDefault();
        staticMap.updateByDomId(ele);
    })
});

document.querySelectorAll('.js-marker-proxy').forEach(function(ele){
    ele.addEventListener('click', function(e){
        e.preventDefault();
        var markerId = ele.getAttribute('data-marker-id');
        if(markerId){
            var marker = document.getElementById(markerId);
            console.log(marker)
            staticMap.updateByDomId(marker);
        }

    })
});

document.querySelectorAll('.js-hide-popup').forEach(function(ele){
    ele.addEventListener('click', function(e){
        staticMap.hide();
    })
});


