/**
* @name: sticky-header.js
* @desc: contains sticky header script
*/

/*!
* Wrap all javascript snippets inside a javascript clousure
* to avoid potential code conflict with
* other libraries and codes ***
* ';' infront of the IIFE is for security purpose, i.e. to run our code in safe mode.
* Because when we try to compress/optimize our js code, compiler/optimizer try to concatenate our code with other js codes
* and it throws an error if it detects a statement without ending with ';' and breaks whole code!
* To fix this issue, it's always safe to add ';' infront of our IIFE which ensures compiler won't throw any error even if a statement doesn't ends at ';'
*/

// == This is an Immediately Invoked Function Expression (IIFE) or simply known as Self-Invoking Function
;(function(w, d, $, undefined) {
    /**
    * Don't write any code above this except comment because "use strict" directive won't have any effetc
    * Enable strict mode to prevent script to run in safe mode (modern way - ECMA2016)
    */
    "use strict";

    /**
    * Detect scroll direction and change the behavior of the header on page scroll
    */
    var scrollTimer = null;
    var scrollDealy = 5;
    var lastScrollTop = 0;
    var $body = $('body');
    // var hearHght = $('#masthead').outerHeight();

    $(w).scroll(function(event){
        if (scrollTimer != null) clearTimeout(scrollTimer);
        scrollTimer = setTimeout(function() {
            var $self = $(this);
            var scrollTop = $self.scrollTop();

            if (scrollTop > window.headerHeight * 1.5) {
                // Reset navbar :: Reference to script.js
                //window.resetNavbarOnScroll();

                if (scrollTop > lastScrollTop) {
                    if (docClickFlag === false) $body.addClass('page-scrolled-down').removeClass('page-scrolled-up');
                } else if(scrollTop == lastScrollTop) {
                    //do nothing 
                    //In IE this is an important condition because there seems to be some instances where the last scrollTop is equal to the new one
                } else {
                    if (docClickFlag === false) $body.addClass('page-scrolled-up').removeClass('page-scrolled-down');
                }
            }
            
            if (scrollTop <= window.headerHeight * 1.5) {
                if (docClickFlag === false) $body.removeClass('page-scrolled-up page-scrolled-down');
            }

            lastScrollTop = scrollTop;
        }, scrollDealy);
    });

    /**
     * Build a function to toggle submenu in a navbar
     */

    
    var currentWidth = $(window).width(); 
    $(document).ready(function () {
    if (currentWidth < 1024) {

        toggleSubmenu($('.has-submenu .js-menu-link'));

        function toggleSubmenu(_node) {
            if (_node.length === 0) return;
            _node.each(function() {
                var $self = $(this);
                $self.on('click', function(e) {
                    //$self.closest('.has-submenu').siblings().removeClass('is-shown');
                    if ($self.closest('.has-submenu').siblings('.is-shown').length > 0) {
                        $self.closest('#menu').addClass('has-shown-child');
                    } else {
                        $self.closest('#menu').removeClass('has-shown-child');
                    }
                    $self.closest('.has-submenu').siblings().removeClass('is-shown is-already-shown');
                    $self.closest('#menu').removeClass('is-already-shown');
                    $self.closest('.has-submenu').toggleClass('is-shown');
                    $self.closest('.has-submenu').find('.submenu-holder').fadeIn(300);
                    e.preventDefault();
                });
            });
        }

        /**
         * Hide mobile submenu
         */

        $('.js-menu-i-xs-current').each(function() {
            $(this).on('click', function(e) {
                $(this).closest('.has-submenu').removeClass('is-shown');
                e.preventDefault();
            });
        });

        if($(window).width() >= 1024){

            $(document).click(function(){
              $(".submenu-holder").hide();
              $(".menu__i").removeClass("is-shown");
            });

            $(".submenu-holder").click(function(e){
              e.stopPropagation(); 
            });
        }

    }

    });

})(window, document, jQuery, undefined);